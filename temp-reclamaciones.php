<?php /* Template Name: Template Libro de Reclamaciones */ 
  get_header('dark');
  $postID = $post->ID;
  $arg = array(
    'post_type' => array( 'proyecto_arteco' ),
    'order'     => 'ASC',
  );
  $proyect = new WP_Query( $arg );
?>
<div class="section">
  <div class="container">
    <?php get_template_part( 'template-parts/banner', 'top' ); ?>
  </div>
  <div class="container <?= ( $_GET['form'] != '' ? 'container--extra-small-2' : '' ); ?> ">
    <div>
      <h2 class="h2 text-center"><?= get_the_title(); ?></h2>
      <?php if( $_GET['form'] == ''): ?>
        <div class="text-center mb-5"> Elige un proyecto </div>
      <?php endif; ?>
      <div class="mt-5">
        <?php if( $_GET['form'] != ''): ?>
          <?php $form = $_GET['form']; ?>
          <form action="" method="get" class="complaints-book-form">
            <input type="hidden" name="action" value="ar_ajax_claim">
            <input type="hidden" name="page" value="<?= $post->ID; ?>">
            <input type="hidden" name="proyect" value="<?= $form; ?>">
            <div class="pt-5"> ¡Hola! Si tienes cualquier consulta o requieres una atención inmediata, te invitamos a usar nuestro canal de atención: central telefónica 00000000.  Gracias por ayudarnos a mejorar nuestro servicio. </div>
            <div class="pt-2">
              <strong> Proveedor: Arteco, <?= get_the_title( $form ); ?></strong>
            </div>
            <?php if ( have_rows( 'previo' , $form ) ) : ?>
              <?php while ( have_rows( 'previo' , $form ) ) : the_row(); ?>
                <div class="pt-1 br-none">Dirección: <?php the_sub_field( 'ubicacion' ); ?></div>
              <?php endwhile; ?>
            <?php endif; ?>
            <div class="pt-5">
              <div class="row">
                <div class="col"><strong> Fecha : </strong> <?= date('d/m/Y', current_time('timestamp')); ?></div>
                <div class="col"><div class="text-right"> </div> </div>
              </div>
            </div>
            <div class="pt-4">
              <h3 class="h3"> Identificación del reclamante </h3>
            </div>

            <div class="pt-4">
              <div class="row">
                <div class="col-lg-6">
                  <input type="text" placeholder="Nombre" name="claimant-first-name" class="form-control" required>
                </div>
                <div class="col-lg-6">
                  <input type="text" placeholder="Apellido" name="claimant-last-name" class="form-control" required>
                </div>
                <div class="col-lg-6">
                  <input type="number" placeholder="DNI" name="claimant-dni" class="form-control ar-input-number" onkeypress="return event.keyCode === 8 || event.charCode >= 48 && event.charCode <= 57" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength = "8" required>
                </div>
                <div class="col-lg-6">
                  <input type="number" placeholder="Teléfono" name="claimant-phone" class="form-control ar-input-number" onkeypress="return event.keyCode === 8 || event.charCode >= 48 && event.charCode <= 57" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength = "9" required>
                </div>
                <div class="col-lg-12">
                  <input type="email" placeholder="Correo electrónico" name="claimant-email" class="form-control" required>
                </div>
                <div class="col-lg-12">
                  <input type="text" placeholder="Domicilio" name="claimant-address" class="form-control" required>
                </div>
                <div class="col-lg-12">
                  <div class="custom-radio">
                    <label><input type="radio" name="claimant-adult" value="yes" checked><span>Soy mayor de edad</span></label>
                    <label><input type="radio" name="claimant-adult" value="no" ><span>No soy mayor de edad</span></label>
                  </div>
                </div>
              </div>
            </div>

            <div class="pt-5">
              <h3 class="h3 ln-h3">Identificación del representante <br> legal del reclamante </h3>
            </div>

            <div class="pt-4">
              <div class="row">
                <div class="col-lg-6">
                  <input type="text" placeholder="Nombre" name="legal-first-name" class="form-control" required>
                </div>
                <div class="col-lg-6">
                  <input type="text" placeholder="Apellido" name="legal-last-name" class="form-control" required>
                </div>
                <div class="col-lg-6">
                  <input type="number" placeholder="DNI" name="legal-dni" class="form-control ar-input-number" onkeypress="return event.keyCode === 8 || event.charCode >= 48 && event.charCode <= 57" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength = "8" required>
                </div>
                <div class="col-lg-6">
                  <input type="number" placeholder="Teléfono" name="legal-phone" class="form-control ar-input-number" onkeypress="return event.keyCode === 8 || event.charCode >= 48 && event.charCode <= 57" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength = "9" required>
                </div>
                <div class="col-lg-12">
                  <input type="email" placeholder="Correo electrónico" name="legal-email" class="form-control" required>
                </div>
                <div class="col-lg-12">
                  <input type="text" placeholder="Domicilio" name="legal-address" class="form-control" required>
                </div>
              </div>
            </div>

            <div class="pt-5">
              <h3 class="h3 ln-h3">Identificación del producto o <br> servicio contratado </h3>
            </div>

            <div class="pt-4">
              <div class="row">
                <div class="col-lg-12">
                  <input type="text" placeholder="Nombre del producto o servicio" name="legal-service" class="form-control" required>
                </div>
                <div class="col-lg-12">
                  <div class="relative d-flex mb-3">
                    <div class="form-check ar-check-input">
                      <input type="radio" class="form-check-input" id="claim-type-claim" name="claim-type" value="Reclamo" required>
                      <label class="form-check-label me-5" for="claim-type-claim">Tengo un reclamo</label>
                    </div>
                    <div class="form-check ar-check-input">
                      <input type="radio" class="form-check-input" id="claim-type-complain" name="claim-type" value="Queja" required>
                      <label class="form-check-label" for="claim-type-complain">Tengo una queja</label>
                    </div>
                  </div>
                </div>
                <div class="col-lg-12">
                  <textarea class="form-control mt-2" name="claim-messange" cols="30" rows="5" placeholder="Detalle de del reclamo o queja" required></textarea>
                </div>

                <div class="col-lg-12 mt-2">
                  <div class="g-recaptcha" data-sitekey="6LcoM7YaAAAAAI_Cv7zhXHfpopQ_GNAbzi7dG7Lg"></div>
                </div>
              </div>
            </div>

            <div class="pt-4">
              <div class="my-3">
                <small> * La formulación del reclamo no impide acudir a otras vías de solución de controversias ni es requisito previo para interponer una denuncia ante el INDECOPI. </small>
              </div>
              <div class="my-3">
                <small> * El proveedor deberá dar respuesta al reclamo en un plazo no mayor a treinta (30) días calendario, pudiendo ampliar el plazo hasta por treinta (30) días más, previa comunicación al consumidor.</small>
              </div>
            </div>
            
            <div class="pt-4 text-center">
              <button class="butn butn-1 ajax-claim-send" type="button" > Enviar reclamo </button>
            </div>
          </form>
          <script src="https://www.google.com/recaptcha/api.js" async defer></script>
        <?php else: ?>
          <div class="row">
            <?php 
              if ( $proyect->have_posts() ) :
                while ( $proyect->have_posts() ):
                  $proyect->the_post();
                  $cats  = get_the_terms( $post->ID, 'proyecto_categoria' );
                  $dists = get_the_terms( $post->ID, 'proyecto_distrito' );
                  if(!empty($cats)):
                    $cat = array_shift($cats);
                    $catproyect = $cat->name;
                    $catproyectslug = $cat->slug;
                  endif;
                  if(!empty($dists)):
                    $dist = array_shift($dists);
                    $distproyect = $dist->name;
                    $distproyectslug = $dist->slug;
                  endif;
                  ?>
                    <div class="col-lg-3">
                      <a href="<?= esc_url( add_query_arg( 'form', $post->ID, get_the_permalink( $postID ) ) ); ?>" class="custom-card custom-card--2 text-left my-4">
                        <div class="custom-card__img-wrapper">
                          <?php
                            if( has_post_thumbnail() ):
                              echo wp_get_attachment_image(get_post_thumbnail_id(), 'full', false, array('class'=> 'custom-card__img img-fluid'));
                            endif; 
                          ?>
                          <h3 class="custom-card__title h3 ln-h3"><?= $distproyect ?></h3>
                        </div>
                        <div>
                          <div class="px-3 py-3">
                            <div class="row align-items-center">
                              <div class="col-lg-12">
                                <h3 class="h4 custom-card__subtitle"><?= get_the_title(); ?></h3>
                                <?php if ( have_rows( 'previo' ) ) : ?>
                                  <?php while ( have_rows( 'previo' ) ) : the_row(); ?>
                                    <div class="mt-1 br-none"><?php the_sub_field( 'ubicacion' ); ?></div>
                                  <?php endwhile; ?>
                                <?php endif; ?>
                              </div>
                            </div>
                          </div>
                        </div>
                      </a>
                    </div>    
                  <?php
                endwhile;
              endif;
              wp_reset_postdata();
            ?>
          </div>
        <?php endif; ?>
      </div>
    </div>
  </div>
</div>
<style>
  .br-none br{
    display: none;
  }
</style>
<?php get_footer(); ?>
