<?php /* Template Name: Template Contacto*/ ?>

<?php get_header('dark'); ?>
<?php if (have_posts()) : while (have_posts()) : the_post();?>
<div class="section bg-color-11">
  <div class="container pt-4 container--small">
    <h2 class="h2 text-center">
     <?php the_title(); ?>
    </h2>
    <div class="text-center">
     <?php the_content(); ?>
    </div>
  </div>
  <div class="container pb-4 container--extra-small mt-4">
    <div class="row">
      <div class="col-lg-6">
        <?php if ( have_rows( 'cta_1' ) ) : ?>
	<?php while ( have_rows( 'cta_1' ) ) : the_row(); ?>
	    <img class="img-border-radius" src="<?php the_sub_field( 'imagen' ); ?>" alt="">
        <div class="text-center mt-4">
          <a class="butn butn-1" href="<?php the_sub_field( 'url' ); ?>">	<?php the_sub_field( 'texto' ); ?></a>
        </div>
      <?php endwhile; ?>
    <?php endif; ?>
      </div>
      <div class="col-lg-6">
        <?php if ( have_rows( 'cta_2' ) ) : ?>
	<?php while ( have_rows( 'cta_2' ) ) : the_row(); ?>
	    <img class="img-border-radius" src="<?php the_sub_field( 'imagen' ); ?>" alt="">
        <div class="text-center mt-4">
          <a class="butn butn-1" href="<?php the_sub_field( 'url' ); ?>">	<?php the_sub_field( 'texto' ); ?></a>
        </div>
      <?php endwhile; ?>
    <?php endif; ?>
      </div>
    </div>
  </div>
</div>
<?php endwhile; endif; ?>

<?php get_footer(); ?>
