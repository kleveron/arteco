<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Arteco
 */

get_header('bloginterna');
$term = get_queried_object();
$categoria_slug = $term->slug;
$categoria_titulo= $term->name;

query_posts( array(
    'category_name'  => $categoria_slug,
    'posts_per_page' => -1
) );


?>
<!-- Categorias -->

<div class=" section" >
 <div class="container">
   <?php   if (have_posts() ) { ?>
	 <div>
		 <h2 class="h2">
       <?php    printf( __( 'Categoria: %s', 'arteco' ), '<span>' . single_cat_title( '', false ) . '</span>' ); ?>
			 </h2>
	 </div>
	 <div class="text-center">
		 <div class="row proyectos__no-slick">
       <?php
       while ( have_posts() ) : the_post();
       $fecha = get_the_date( 'j \d\e F \d\e Y', $POST->ID );
       $featured_img_url = get_the_post_thumbnail_url($POST->ID, 'full');
       $categories = get_the_category();
       $autor = $POST->post_author;
       $autor_nombre =  get_the_author_meta('display_name',$autor);
       $autor_descripcion =  get_the_author_meta('description',$autor);
       $autor_foto = get_avatar_url($autor);

       ?>
        	<div class="col-lg-4">
            <a href="<?php the_permalink(); ?>" class="custom-card text-left my-5">
              <div class="custom-card__img-wrapper">
                  <img class="custom-card__img img-fluid blog" src="<?php echo $featured_img_url; ?>" alt="">
                <div class="custom-card__sticker"><?php if ( ! empty( $categories ) ) { echo esc_html( $categories[0]->name );  } ?></div>
              </div>
              <div class="color-3">
                <div class="color-3 pt-2 px-3">
                  <small>
                     <?php echo $fecha; ?>
                  </small>
                </div>
                <div class="custom-card__subtitle h4 ln-h4 px-3">
                   <?php echo the_title(); ?>
                </div>
                <hr class="bg-color-4">
                <div class="px-3">
                  <div class="d-flex color-3 pt-0 pb-3">
                    <div class="mr-2">
                      <?php
                        $author = get_the_author_meta('ID');
                        $picture = get_avatar_url($author, array('size' => 450));
                      ?>
                      <img class="autor_foto" src="<?= $picture; ?>" alt="">
                    </div>
                    <div class="pt-2">
                      <div class="mb-2">
                        <strong>
                           <?php echo $autor_nombre; ?>
                        </strong>
                      </div>
                      <div>
                        <?php echo $autor_descripcion; ?>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </a>
        	</div>
		 <?php
   endwhile;
   wp_reset_query();
      ?>

		 </div>
	 </div>

   <?php } else {
       echo '<div><h2 class="h2">No se encontraron artículos en esta categoría</h2></div>';
     } ?>
	 <div class="text-center pt-4">
		   <a href="<?php echo esc_url( home_url( '/noticias' ) ); ?>" class="butn butn-1"><?php _e('Ver todos los artículos','arteco'); ?></a>
	 </div>



 </div>
</div>


<?php

get_footer();
