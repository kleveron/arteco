<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package Arteco
 */

get_header('bloginterna');
?>

<?php get_template_part( 'template-parts/noticias', 'resultados' ); ?>

<?php

get_footer();
