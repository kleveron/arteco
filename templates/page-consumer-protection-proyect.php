<?php
  /**
   *  Template Name: Protection Proyect
   * @package WordPress
  */
  __('Protection Proyect', 'arteco' );
  get_header();
  $parent = $post->post_parent;
  $child = get_pages( array( 'child_of' => $parent ) );
?>

 <!-- Breadcrumb -->
  <div class="custom-breadcrumb">
    <nav aria-label="breadcrumb">
      <div class="container">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="<?= get_bloginfo('url') ?>">Home</a></li>
          <?php if($parent): ?>
            <li class="breadcrumb-item"><a href="<?= get_permalink( $parent ) ?>"><?= get_the_title( $parent ); ?></a></li>
            <li class="breadcrumb-item active" aria-current="page"><?= get_the_title(); ?></li>
        <?php else: ?>
          <li class="breadcrumb-item active" aria-current="page"><?= get_the_title(); ?></li>
          <?php endif; ?>
        </ol>
      </div>
    </nav>
  </div>

  <div class="banner banner-departamentos overflow-hidden">
    <?php get_template_part( 'template-parts/banner','top' ); ?>
    <div class="banner__images">
      <div>
        <div class="position-relative">
          <?php if( get_field('PGCP_banner') ): ?>
            <?= wp_get_attachment_image( get_field('PGCP_banner'), 'full', false, array('class'=> 'banner__img') ); ?>
          <?php else: ?>
            <img class="banner__img" src="<?= get_template_directory_uri(); ?>/src/img/departamentos-background.png" alt="banner-arteco">
          <?php endif; ?>
        </div>
      </div>
    </div>
    <div class="banner__title-wrapper">
      <div class="banner__title">
        <?php if( get_field('PGCP_title') ): ?>
          <?= get_field('PGCP_title' ); ?>
        <?php else: ?>
          Código de Protección al <span class="color-1">Consumidor</span>
        <?php endif; ?>
      </div>
    </div>
  </div>

  <div class="section">
    <div class="container">
      <div>
        <span> Elige el proyecto de tu preferencia </span>
        <?php if($parent && $child): ?>
          <div class="dropdown dropdown-arteco">
            <button class="btn dropdown-toggle" type="button" id="dropdownCustomer" data-toggle="dropdown" aria-expanded="false"> <?= get_the_title(); ?></button>
            <ul class="dropdown-menu" aria-labelledby="dropdownCustomer">
              <?php  foreach ( $child as $key => $cat ):?>
                <li>
                  <a class="dropdown-item <?= $post->ID == $cat->ID ? 'disabled' : ''; ?>" href="<?= get_the_permalink($cat->ID); ?>">
                    <div class="position-relative">
                      <div class="d-flex">
                        <?php if( get_field('PGCP_thumbnail', $cat->ID ) ): ?>
                          <div>
                            <?= wp_get_attachment_image( get_field('PGCP_thumbnail', $cat->ID ), 'full', false, array('class'=> 'img-fluid') ); ?>
                          </div>
                        <?php endif; ?>
                        <div class="pt-2 pl-3">
                          <strong><?= get_the_title($cat->ID); ?></strong>
                          <?php if( get_field('PGCP_address', $cat->ID ) ): ?>
                            <div><?= get_field('PGCP_address', $cat->ID ); ?></div>
                          <?php endif; ?>
                        </div>
                      </div>
                    </div>
                  </a>
                </li>
              <?php endforeach;?>
            </ul>
          </div>
        <?php endif; ?>
      </div>
      
      <?php if( get_field('PGCP_list') ): ?>
        <?php  foreach ( get_field('PGCP_list') as $key => $item ):?>
          <div class="mb-3 <?= $key == 0 ? 'pt-4': ''; ?>">
            <?php if(isset($item['title']) && $item['title'] != ''): ?>
              <h4 class="h4"> <?=  $item['title'];  ?> </h4>
            <?php endif; ?>
            <div class="mt-3">
            <?php if(isset($item['content']) && $item['content'] != ''): ?>
               <?= $item['content']; ?>
            <?php endif; ?>
            </div>
            <?php if(isset($item['file']) && $item['file'] != ''): ?>
              <a href="<?= $item['file']; ?>" class="butn butn-1" download> Descargar PDF </a>
            <?php endif; ?>
          </div>
        <?php endforeach;?>
      <?php endif; ?>
    </div>
  </div>

<?php
  get_footer();