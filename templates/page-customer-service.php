<?php
  /**
   *  Template Name: Customer Service
   * @package WordPress
  */
  __('Customer Service', 'arteco' );

  get_header();
  ?>
  <!-- Breadcrumb -->
  <div class="custom-breadcrumb">
    <nav aria-label="breadcrumb">
      <div class="container">
        <ol class="breadcrumb">
          <?php if(wp_get_post_parent_id(get_the_ID())): ?>
            <li class="breadcrumb-item" >
              <a href="<?= get_the_permalink(wp_get_post_parent_id($post->ID));?>"><?= get_the_title(wp_get_post_parent_id($post->ID));  ?></a>
            </li>
          <?php endif; ?>
          <li class="breadcrumb-item active" aria-current="page"> <?= get_the_title($post->ID)?> </li>
        </ol>
      </div>
    </nav>
    <?php get_template_part( 'template-parts/banner','top' ); ?>
  </div>

  <!-- Elegir proyecto -->
  <div class="section">
    <div class="container">
      <div class="position-relative">
        <div class="mt-4">
          <div class="row">
            <div class="col-lg-6">
              <h2 class="h2"><?= get_the_title($post->ID) ?>  </h2>
              <div class="mt-3 mb-5"> <?= get_the_content(); ?> </div>
              <div class="mb-1"><strong>LIMA</strong> </div>
              <?php if( get_field('admin_schedule', 'option')  ): ?>
                <div class="relative"><?= get_field('admin_schedule', 'option'); ?></div>
              <?php endif; ?>

              <div class="mt-3">
                <?php if( get_field('admin_phone', 'option') ): ?>
                  <div class="d-flex align-items-center my-3">
                    <img src="<?= get_template_directory_uri(); ?>/src/img/servicio-cliente-1.png" alt="">
                    <span class="ml-2"> <?= get_field('admin_phone', 'option'); ?></span>
                  </div>
                <?php endif; ?>
                <?php if( get_field('admin_whatsapp', 'option') ): ?>
                  <div class="d-flex align-items-center my-3">
                    <img src="<?= get_template_directory_uri(); ?>/src/img/servicio-cliente-2.png" alt="">
                    <span class="ml-2"> <?= get_field('admin_whatsapp', 'option'); ?></span>
                  </div>
                <?php endif; ?>
                <?php if( get_field('admin_company_email', 'option') ): ?>
                  <div class="d-flex align-items-center my-3">
                    <img src="<?= get_template_directory_uri(); ?>/src/img/servicio-cliente-3.png" alt="">
                    <span class="ml-2"> <?= get_field('admin_company_email', 'option'); ?></span>
                  </div>
                <?php endif; ?>
              </div>
            </div>
            
            <form class="col-lg-6" method="POST">
              <div class="row">
                <input type="hidden" name="action" value="ar_ajax_contact">
                <input type="hidden" name="page" value="<?= $post->ID; ?>">
                <input type="hidden" name="type" value="service">
                <div class="col-lg-6">
                  <input placeholder="Nombre *" name="firstname" type="text" class="form-control mb-4" required>
                </div>
                <div class="col-lg-6">
                  <input placeholder="Apellido *" name="lastname"  type="text" class="form-control mb-4" required>
                </div>
                <div class="col-lg-6">
                  <input placeholder="Teléfono" name="phone" type="text" class="form-control mb-4" onkeypress="return event.keyCode === 8 || event.charCode >= 48 && event.charCode <= 57" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength = "9" required>
                </div>
                <div class="col-lg-6">
                  <input placeholder="Correo electronico" name="email" type="email" class="form-control mb-4" required>
                </div>
                <div class="col-lg-6">
                  <input placeholder="DNI *" name="dni" type="text" class="form-control mb-4" required>
                </div>
                <div class="col-lg-6">
                  <div class="ar-select mb-4">
                    <select class="form-control ar-select2 ar-select2-proyect" name="proyect" data-minimum-results-for-search="Infinity" required>
                      <option value="" hidden> Elegir proyecto </option>
                      <?php 
                        $args = array(
                          'post_type' => array( 'proyecto_arteco' ),
                          'order'     => 'ASC',
                        );
                        $proyect = new WP_Query( $args );
                        if ( $proyect->have_posts() ):
                          while ( $proyect->have_posts() ):
                            $proyect->the_post();
                            ?>
                            <option value="<?= get_the_ID(); ?>"><?= get_the_title(); ?></option>
                            <?php
                          endwhile;
                        endif;
                      ?>
                    </select>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="ar-select mb-4">
                    <select class="form-control ar-select2 ar-select2-tower" name="tower" data-minimum-results-for-search="Infinity" required disabled>
                      <option value="" hidden> Torre </option>
                    </select>
                  </div>
                </div>

                <div class="col-lg-6">
                  <div class="ar-select mb-4">
                    <select class="form-control ar-select2 ar-select2-departament" name="departament" data-minimum-results-for-search="Infinity" required disabled>
                      <option value="" hidden> N° de departamento </option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="row mt-4">
                <div class="col-lg-12">
                  <div class="d-block text-left my-2 custom-control custom-checkbox ">
                    <input type="checkbox" class="custom-control-input" id="cbox-terminos" name="terms" checked required>
                    <label for="cbox-terminos" class="custom-control-label ">Aceptar términos y condiciones</label>
                  </div>
                </div>
                <div class="col-lg-12">
                  <div class="d-block text-left my-2 custom-control custom-checkbox ">
                    <input type="checkbox" class="custom-control-input" id="cbox-autorizo" name="privacy_policies">
                    <label for="cbox-autorizo" class="custom-control-label ">Autorizo a Arteco para que relice las actividades descritas en la <a class="color-3" href="#">Política de Privacidad</a></label>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-lg-12 text-center pt-4">
                  <button class="butn butn-1 ar-send-contact" class="button">Enviar</button>
                </div>
              </div>
            </form>

          </div>
        </div>
      </div>
    </div>
  </div>
  <?php
  get_footer();