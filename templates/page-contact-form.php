<?php
  /**
   *  Template Name: Contact Form
   * @package WordPress
  */
  __('Contact Form', 'arteco' );

  get_header();
  ?>
  <!-- Breadcrumb -->
  <div class="custom-breadcrumb">
    <nav aria-label="breadcrumb">
      <div class="container">
        <ol class="breadcrumb">
          <?php if(wp_get_post_parent_id(get_the_ID())): ?>
            <li class="breadcrumb-item" >
              <a href="<?= get_the_permalink(wp_get_post_parent_id($post->ID));?>"><?= get_the_title(wp_get_post_parent_id($post->ID));  ?></a>
            </li>
          <?php endif; ?>
          <li class="breadcrumb-item active" aria-current="page"> <?= get_the_title($post->ID)?> </li>
        </ol>
      </div>
    </nav>
    <?php get_template_part( 'template-parts/banner','top' ); ?>
  </div>
  <div class="contacto-formulario section">
    <?php 
      $poster =  'style="background-image:url('.get_template_directory_uri().'/src/img/contacto-formulario-bg.png)"';
      if(get_the_post_thumbnail()):
        $poster =  'style="background-image:url('.get_the_post_thumbnail_url().')"';
      endif;
    ?>
    <div class="contacto-formulario__bg" <?= $poster; ?>></div>
    <div class="container container--extra-small">
      
      <form class="bg-color-w border-radius contacto-formulario__content " method="POST"  >
        <input type="hidden" name="action" value="ar_ajax_contact">
        <input type="hidden" name="page" value="<?= $post->ID; ?>">
        <input type="hidden" name="type" value="contact">
        <h3 class="h2"> <?= get_the_title($post->ID) ?> </h3>
        <div class="pt-2 pb-4">
          <?= get_the_content(); ?>
        </div>
        <div class="row">
          <div class="col-lg-6">
            <input placeholder="Nombre *" name="firstname" type="text" class="form-control" required>
          </div>
          <div class="col-lg-6">
            <input placeholder="Apellido *" name="lastname"  type="text" class="form-control" required>
          </div>
          <div class="col-lg-6">
            <input placeholder="Teléfono" name="phone" type="text" class="form-control" onkeypress="return event.keyCode === 8 || event.charCode >= 48 && event.charCode <= 57" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength = "9" required>
          </div>
          <div class="col-lg-6">
            <input placeholder="Correo electronico " name="email" type="email" class="form-control" required>
          </div>
          <div class="col-lg-12">
            <div class="ar-select mb-2">
              <select class="form-control ar-select2 ar-select2-subject" name="subject" data-minimum-results-for-search="Infinity" required>
                <option value="" hidden> Asunto </option>
                <option value="Contáctanos" > Contáctanos </option>
                <option value="Trabaja con nosotros" > Trabaja con nosotros </option>
              </select>
            </div>
          </div>
          <div class="col-lg-12">
            <textarea placeholder="Mensaje" name="" id="" cols="30" rows="5" class="form-control my-3" required></textarea>
          </div>
        </div>
        <div class="text-center mt-4">
          <button class="butn butn-1 ar-send-contact" class="button" >Enviar</button>
        </div>
      </form>
    </div>
  </div>
  <?php
  get_footer();