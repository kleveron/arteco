<?php /* Template Name: Template Proyectos */ ?>

<?php get_header('dark');?>
<?php get_template_part( 'template-parts/proyectos', 'hero' ); ?>
<?php get_template_part( 'template-parts/proyectos', 'search' ); ?>
<?php get_template_part( 'template-parts/proyectos', 'grid' ); ?>
<?php get_template_part( 'template-parts/testimonios', 'widget' ); ?>

<?php get_footer();
