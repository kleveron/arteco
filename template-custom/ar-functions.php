<?php
require JC_DIRECTORY.'/lib/html2pdf/vendor/autoload.php';
use Spipu\Html2Pdf\Html2Pdf;
if (file_exists( JC_DIRECTORY . '/lib/google-sheet/functions.php')) :
	require_once JC_DIRECTORY . '/lib/google-sheet/functions.php';
endif;

add_action('wp_enqueue_scripts', 'ar_theme_scripts_styles');
function ar_theme_scripts_styles(){
	wp_enqueue_style( 'select2', get_stylesheet_directory_uri()  . '/src/css/select2.css');
	wp_enqueue_style( 'datepicker', get_stylesheet_directory_uri()  . '/src/css/datepicker.css');
	wp_enqueue_style( 'arteco', get_stylesheet_directory_uri()  . '/src/css/arteco.css', array(), time());

	wp_enqueue_script('select2', get_stylesheet_directory_uri() . '/src/js/select2.js', array( ), false, true);
	wp_enqueue_script('datepicker', get_stylesheet_directory_uri() . '/src/js/datepicker.js', array( ), false, true);
	wp_enqueue_script('datepicker-es', get_stylesheet_directory_uri() . '/src/js/datepicker.es-ES.js', array( ), false, true);
	wp_enqueue_script('jquery-blockui', get_stylesheet_directory_uri() . '/src/js/jquery.blockUI.js', array( 'jquery' ), false, true);
	wp_enqueue_script('arteco',get_stylesheet_directory_uri() . '/src/js/arteco.js', array( 'jquery' ), time(), true);

	wp_localize_script( 'arteco', 'ajax_script', array(
		'home_url'    => get_bloginfo( 'url' ),
		'ajax_url'    => admin_url( 'admin-ajax.php' ),
		'ajax_nonce'  => wp_create_nonce('ajax-nonce'),
	));
}

add_action( 'init', 'ar_generate_tables_contact');
function ar_generate_tables_contact(){
	global $wpdb;

	$db_table_contact = $wpdb->prefix.'contact';
	if( $wpdb->get_var( "SHOW TABLES LIKE '$db_table_contact'" ) != $db_table_contact ) {
		if ( ! empty( $wpdb->charset ) )
			$charset_collate = "DEFAULT CHARACTER SET {$wpdb->charset}";
		if ( ! empty( $wpdb->collate ) )
			$charset_collate .= " COLLATE {$wpdb->collate}";

			$sqlc = "CREATE TABLE {$db_table_contact} (
					contact_id bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT,
					contact_firstname varchar(255) NULL ,
					contact_lastname varchar(255) NULL ,
					contact_phone varchar(255) NULL ,
					contact_email varchar(255) NULL ,
					contact_dni varchar(255) NULL ,
					contact_bedrooms varchar(255) NULL ,
					contact_flat varchar(255) NULL ,
					contact_view varchar(255) NULL ,
					contact_departament varchar(255) NULL ,
					contact_towe varchar(255) NULL ,
					contact_hour varchar(255) NULL ,
					contact_subject varchar(255) NULL ,
					contact_msg varchar(255) NULL ,
					contact_post bigint(10) NULL ,
					contact_proyect bigint(10) NULL ,
					contact_sheet boolean NULL ,
					contact_register  datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
					PRIMARY KEY (contact_id)
			) {$charset_collate};";
			require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
			dbDelta($sqlc);
	}
}

add_action( 'admin_menu', 'ar_contact_menu');
function ar_contact_menu() {
	add_menu_page( 'Contactos', 'Contactos', 'manage_options', 'manage_contact', 'ar_manage_contacts', 'dashicons-admin-users', 3 );
}

add_action( 'admin_enqueue_scripts', 'ar_contact_admin_script' );
function ar_contact_admin_script( $hook ) {
  if ( 'toplevel_page_manage_contact' == $hook ) {
    wp_enqueue_style( 'boostrap-datatable', '//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css', false, '4.5.2' );
    wp_enqueue_style( 'datatable-css', '//cdn.datatables.net/1.10.23/css/dataTables.bootstrap4.min.css', false, '1.10.23' );
    wp_enqueue_style( 'dataTables-css', '//cdn.datatables.net/buttons/1.6.5/css/buttons.dataTables.min.css', false, '1.6.5' );
    wp_enqueue_style( 'contact-css', get_template_directory_uri().'/src/contact/contact.css', false, '1.6' );

    wp_enqueue_script('dat-js', '//cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js', array( 'jquery' ), time(), true);
    wp_enqueue_script('dat-js-button', '//cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js', array( 'jquery' ), time(), true);
    wp_enqueue_script('dat-js-zip', '//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js', array( 'jquery' ), time(), true);
    wp_enqueue_script('dat-js-pdf', '//cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js', array( 'jquery' ), time(), true);
    wp_enqueue_script('dat-js-fonts', '//cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js', array( 'jquery' ), time(), true);
    wp_enqueue_script('dat-js-html', '//cdn.datatables.net/buttons/1.6.5/js/buttons.html5.min.js', array( 'jquery' ), time(), true);
    wp_enqueue_script('dat-js-print', '//cdn.datatables.net/buttons/1.6.5/js/buttons.print.min.js', array( 'jquery' ), time(), true);
    wp_enqueue_script('dat-boostrap-js', '//cdn.datatables.net/1.10.23/js/dataTables.bootstrap4.min.js', array( 'jquery' ), time(), true);
    wp_enqueue_script('contact-js', get_template_directory_uri().'/src/contact/contact.js', array( 'jquery' ), time(), true);
  }
}

function ar_manage_contacts(){
	global $wpdb;
	$contact = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}contact ", OBJECT );
	?>
	<div class="wrap">
    <h2 class="--h2">Contacto <?= get_bloginfo('name'); ?></h2>
    <div class="clear" style="height:20px;width:100%"></div>
			<table id="contact" class="table table-striped table-bordered" style="width:100%">
				<thead>
					<tr>
						<th>Nombre</th>
						<th>Apellido</th>
						<th>Teléfono</th>
						<th>Correo</th>
						<th>Dni</th>
						<th>Dormitorio</th>
						<th>Piso</th>
						<th>Vista</th>
						<th>Departamento</th>
						<th>Hora</th>
						<th>Asunto</th>
						<th>mensaje</th>
						<th>Pagina</th>
						<th>Fecha</th>
					</tr>
        </thead>
				<tbody>
					<?php foreach ($contact as $ky => $c): ?>
						<tr>
							<td> <?= $c->contact_firstname; ?> </td>
							<td> <?= $c->contact_lastname; ?> </td>
							<td> <?= $c->contact_phone; ?> </td>
							<td> <?= $c->contact_email; ?> </td>
							<td> <?= $c->contact_dni; ?> </td>
							<td> <?= $c->contact_bedrooms; ?> </td>
							<td> <?= $c->contact_flat; ?> </td>
							<td> <?= $c->contact_view; ?> </td>
							<td> <?= $c->contact_departament; ?> </td>
							<td> <?= $c->contact_hour; ?> </td>
							<td> <?= $c->contact_subject; ?> </td>
							<td> <?= $c->contact_msg; ?> </td>
							<td> <?= get_the_title($c->contact_post); ?> </td>
							<td> <?= human_time_diff( strtotime($c->contact_register) ); ?> </td> <!-- date('j \d\e F \d\e Y g:i A', strtotime($c->contact_register) ) -->
						</tr>
          <?php endforeach; ?>
				</tbody>
				<tfoot>
					<tr>
						<th>Nombre</th>
						<th>Apellido</th>
						<th>Teléfono</th>
						<th>Correo</th>
						<th>Dni</th>
						<th>Dormitorio</th>
						<th>Piso</th>
						<th>Vista</th>
						<th>Departamento</th>
						<th>Hora</th>
						<th>Asunto</th>
						<th>mensaje</th>
						<th>Pagina</th>
						<th>Fecha</th>
					</tr>
				<tfoot>
	<?php
}

function ar_wp_mail_content_type() { return 'text/html'; }

function ar_wp_mail_from_name() { return get_bloginfo('name'); }

add_action('wp_ajax_nopriv_ar_ajax_contact', 'ar_ajax_contact');
add_action('wp_ajax_ar_ajax_contact', 'ar_ajax_contact');
function ar_ajax_contact(){
	global $wpdb;
	$firstname		= $_POST['firstname'];
	$lastname			= $_POST['lastname'];
	$phone				= $_POST['phone'];
	$email				= $_POST['email'];
	$dni					= $_POST['dni'];
	$bedrooms			= $_POST['bedrooms'];
	$flat					= $_POST['flat'];
	$view					= $_POST['view'];
	$departament	= $_POST['departament'];
	$hour					= $_POST['hour'];
	$subject			= $_POST['subject'];
	$msg					= $_POST['msg'];
	$post					= $_POST['page'];
	$type					= $_POST['type'];
	$proyect			= $_POST['proyect']; //int
	$tower				= $_POST['tower']; //torre
	$url 					= $_POST['url'];
	$date 				= $_POST['date'];

	$contact_sheet	=  false;
	$headers 				= 'From: Arteco Inmobiliaria < informeventas@arteco.pe >' . "\r\n";
	$mail_contact 	= ar_mails_admin();
	$attachments		= array();

	$Hubspot 	= '';
	$codex		= '';
	$popup 		= '';
	$_dat			= '';

	if($type == 'contact'):
		//ENVIO AL USUARIO
		if($subject == 'Trabaja con nosotros'):
			$subject_cli = 'Gracias por contactarnos';
			$title_cli 	 = 'Hola '.$firstname .' '.$lastname;
			$body_cli		 = '';
			$body_cli 	 .= '<p>Hemos recibido un correo con información sobre tu perfil profesional.</p>';
			$body_cli 	 .= '<p>Te agradecemos haber mostrado interés en ser parte de nuestra familia para seguir creciendo profesionalmente.</p>';
			$body_cli 	 .= '<p>Pronto estaremos en contacto.</p>';
			$body_cli 	 .= '</br><p>Saludos,</p>';
			$popup 				= ar_modal('Tus datos se han enviado correctamente', 'Te enviaremos un correo indicandote los pasos a seguir en tu proceso.');
		else :
			$subject_cli = 'Gracias por contactarnos';
			$title_cli 	 = 'Hola '.$firstname .' '.$lastname;
			$body_cli		 = '';
			$body_cli 	 .= '<p>Hemos recibido un correo con una solicitud de contacto.</p>';
			$body_cli 	 .= '<p>En breve nos estaremos comunicando contigo.</p>';
			$body_cli 	 .= '</br><p>Saludos,</p>';
			$popup 				= ar_modal('Gracias por comunicarme con nuestro equipo', 'En breve uno de nuestros asesores se estará comunicando directamente contigo');
		endif;
		$message_cli  = ar_mail_contact( $title_cli, $body_cli );

		//ENVIO ADMIN
		$subject_admin 	= 'Nuevo Contacto';
		$title_admin 		= 'Hola Admin';
		$body_admin 		= '';
		if($firstname):
			$body_admin 	.= '<p><strong>Nombre : </strong> '.$firstname.'</p>';
		endif;
		if($lastname):
			$body_admin 	.= '<p><strong>Apellido : </strong> '.$lastname.'</p>';
		endif;
		if($phone):
			$body_admin 	.= '<p><strong>Teléfono : </strong> '.$phone.'</p>';
		endif;
		if($email):
			$body_admin 	.= '<p><strong>Correo : </strong> '.$email.'</p>';
		endif;
		if($subject):
			$body_admin 	.= '<p><strong>Asunto : </strong> '.$subject.'</p>';
		endif;
		if($msg):
			$body_admin 	.= '<p><strong>Mensaje : </strong> '.$msg.'</p>';
		endif;
		$message_admin = ar_mail_contact( $title_admin , $body_admin );

	endif;

	if($type == 'proyect'):
		$proyectName = get_field('sheet_name_proyect', $post ) ? get_field('sheet_name_proyect', $post ) : get_the_title($post);
		//ENVIO AL USUARIO
		$subject_cli = 'Proforma virtual de '.$proyectName.' | Arteco Inmobiliaria';
		$title_cli 	 = 'Hola '.$firstname .' '.$lastname;
		$body_cli		 = '';
		$body_cli 	 .= '<p>Un gusto saludarte, adjuntamos una copia de la cotización realizada en <a href="'.get_bloginfo('url').'">Arteco Inmobiliaria</a>.</p>';
		$body_cli 	 .= '<p>En ella encontrarás todos los detalles del inmueble elegido, plano, precio y datos de contacto.</p>';
		if($date):
			$body_cli 	 .= '<p>Nos contactaremos contigo en el horario de: '.$hour.' para coordinar la visita del dia '.$date.'</p>';
		else:
			$body_cli 	 .= '<p>Nos contactaremos contigo en el horario de: '.$hour.'</p>';
		endif;

		$body_cli 	 .= '</br><p>Saludos cordiales,</p>';
		$popup 				= ar_modal('Gracias por comunicarme con nuestro equipo', 'En breve uno de nuestros asesores se estará comunicando directamente contigo');

		//ENVIO ADMIN
		$subject_admin 	= 'Nuevo Contacto';
		$title_admin 		= 'Hola Admin';
		$body_admin 		= '';
		$body_admin 	.= '<p>Un nuevo lead ha dejado sus datos para recibir más información sobre el proyecto '.get_the_title($post).'.</p>';
		if($proyect):
			$body_admin 	.= '<p><strong>Proyecto : </strong> '.get_the_title($proyect).'</p>';
		endif;
		if($firstname):
			$body_admin 	.= '<p><strong>Nombre : </strong> '.$firstname.'</p>';
		endif;
		if($lastname):
			$body_admin 	.= '<p><strong>Apellido : </strong> '.$lastname.'</p>';
		endif;
		if($phone):
			$body_admin 	.= '<p><strong>Teléfono : </strong> '.$phone.'</p>';
		endif;
		if($email):
			$body_admin 	.= '<p><strong>Correo : </strong> '.$email.'</p>';
		endif;
		if($dni):
			$body_admin 	.= '<p><strong>DNI : </strong> '.$dni.'</p>';
		endif;
		if($bedrooms):
			$body_admin 	.= '<p><strong>Dormitorios: </strong> '.$bedrooms.'</p>';
		endif;
		if($flat):
			$body_admin 	.= '<p><strong>Piso: </strong> '.$flat.'</p>';
		endif;
		if($view):
			$body_admin 	.= '<p><strong>Vista: </strong> '.$view.'</p>';
		endif;
		if($departament):
			$body_admin 	.= '<p><strong>Departameto: </strong> '.$departament.'</p>';
		endif;
		if($hour):
			$body_admin 	.= '<p><strong>Hora : </strong> '.$hour.'</p>';
		endif;
		if($date):
			$body_admin 	.= '<p><strong>Dia de visita : </strong> '.$date.'</p>';
		endif;

		//SAVE GOOGLE SHEET
		$correlative 	= ar_generate_code( $post );
		$executive   	= ar_get_ejecutive( $post, $correlative['number']);
		$codex				= $correlative['correlative'];

		$data = array(
			'departament' => ar_get_departament($post , $departament, true),
			'executive'		=> $executive,
			'proyect'			=> 	array(
				'name' 			=> $proyectName,
				'address' 	=> get_field('sheet_address_proyect', $post ),
				'status'		=> get_field('sheet_state_proyect', $post ),
				'currency'	=> $correlative['correlative'],
			),
			'client'			=> array(
				'firstname' => $firstname,
				'lastname' 	=> $lastname,
				'document'  => $dni,
				'phone'			=> $phone ,
				'email'			=> $email,
			),
			'extra'				=> array(
				'logo_footer' 	=> get_field('footer_pdf', $post ) ? wp_get_original_image_path(get_field('footer_pdf', $post )) : '',
				'logo_proyect' 	=> get_field('logo_pdf', $post ) ? wp_get_original_image_path(get_field('logo_pdf', $post )) : '',
			),
			'utm'					=> ar_get_utm($url)
		);

		$executiveM = array(
			'name' 		=> $executive['name_ejecutive'],
			'address' => get_field('sheet_address_proyect', $post ),
			'phone' 	=> $executive['phone_ejecutive'],
			'firm' 		=> get_field('logo_firm', $post ) ? get_field('logo_firm', $post ) : get_template_directory_uri().'/src/img/mails/logo-firma.jpg',
			'proyect' => $proyectName,
		);

		$contact_sheet =  true;

		$html2pdf = new HTML2PDF('P', array(220,470), 'en',true, 'UTF-8', array(0, 0, 0, 0));
		$html2pdf->pdf->SetDisplayMode('fullpage');
		$pdf   = ar_generate_pdf( $data );
		$html2pdf->writeHTML( $pdf );

		$pdf_uploads_dir = wp_upload_dir()['basedir'] . '/pdf/';
		if ( ! is_dir( $pdf_uploads_dir ) ) :
			mkdir( $pdf_uploads_dir, 0755 );
		endif;
		$file_path = $pdf_uploads_dir . $correlative['correlative'].'.pdf';
		$files = $html2pdf->output( $file_path,'F');
		$_ID = get_field('sheet_id_document', $post );
		$_NAME = get_field('sheet_name_document', $post );
		$sheet = ar_save_sheet($data , $_ID, $_NAME );

		$attachments = array(WP_CONTENT_DIR . '/uploads/pdf/'.$correlative['correlative'].'.pdf');


		$message_cli  = ar_mail_contact( $title_cli, $body_cli , $executiveM);
		$message_admin = ar_mail_contact( $title_admin , $body_admin);
		add_filter('wp_mail_from', create_function('', 'return "'.$executive['email_ejecutive'].'";' ));

		$mail_contact = $executive['email_ejecutive'];
		$Hubspot = array(
			'url' => get_field('hubspot_api' , $post),
			'firstname' => $firstname,
			'lastname' 	=> $lastname,
			'email' 		=> $email,
			'phone' 		=> $phone,
			'website' 	=> $url,
			'dni' 			=> $dni,
			'bedrooms' 	=> $bedrooms,
			'flat' 			=> $flat,
		);

	endif;

  if($type == 'proyect_post'):
		$proyectName = get_field('sheet_name_proyect', $post ) ? get_field('sheet_name_proyect', $post ) : get_the_title($post);
		//ENVIO AL USUARIO
		$subject_cli = 'Proforma virtual de '.$proyectName.' | Arteco Inmobiliaria';
		$title_cli 	 = 'Hola '.$firstname .' '.$lastname;
		$body_cli		 = '';
		$body_cli 	 .= '<p>Un gusto saludarte, adjuntamos una copia de la cotización realizada en <a href="'.get_bloginfo('url').'">Arteco Inmobiliaria</a>.</p>';
		$body_cli 	 .= '<p>En ella encontrarás todos los detalles del inmueble elegido, plano, precio y datos de contacto.</p>';
		if($date):
			$body_cli 	 .= '<p>Nos contactaremos contigo en el horario de: '.$hour.' para coordinar la visita del dia '.$date.'</p>';
		else:
			$body_cli 	 .= '<p>Nos contactaremos contigo en el horario de: '.$hour.'</p>';
		endif;

		$body_cli 	 .= '</br><p>Saludos cordiales,</p>';
		$popup 				= ar_modal('Gracias por comunicarme con nuestro equipo', 'En breve uno de nuestros asesores se estará comunicando directamente contigo');

		//ENVIO ADMIN
		$subject_admin 	= 'Nuevo Contacto';
		$title_admin 		= 'Hola Admin';
		$body_admin 		= '';
		$body_admin 	.= '<p>Un nuevo lead ha dejado sus datos para recibir más información sobre el proyecto '.get_the_title($post).'.</p>';
		if($proyect):
			$body_admin 	.= '<p><strong>Proyecto : </strong> '.get_the_title($proyect).'</p>';
		endif;
		if($firstname):
			$body_admin 	.= '<p><strong>Nombre : </strong> '.$firstname.'</p>';
		endif;
		if($lastname):
			$body_admin 	.= '<p><strong>Apellido : </strong> '.$lastname.'</p>';
		endif;
		if($phone):
			$body_admin 	.= '<p><strong>Teléfono : </strong> '.$phone.'</p>';
		endif;
		if($email):
			$body_admin 	.= '<p><strong>Correo : </strong> '.$email.'</p>';
		endif;
		if($dni):
			$body_admin 	.= '<p><strong>DNI : </strong> '.$dni.'</p>';
		endif;
		if($bedrooms):
			$body_admin 	.= '<p><strong>Dormitorios: </strong> '.$bedrooms.'</p>';
		endif;
		if($flat):
			$body_admin 	.= '<p><strong>Piso: </strong> '.$flat.'</p>';
		endif;
		if($view):
			$body_admin 	.= '<p><strong>Vista: </strong> '.$view.'</p>';
		endif;
		if($departament):
			$body_admin 	.= '<p><strong>Departameto: </strong> '.$departament.'</p>';
		endif;
		if($hour):
			$body_admin 	.= '<p><strong>Hora : </strong> '.$hour.'</p>';
		endif;
		if($date):
			$body_admin 	.= '<p><strong>Dia de visita : </strong> '.$date.'</p>';
		endif;

		$message_cli  = ar_mail_contact( $title_cli, $body_cli , $executiveM);
		$message_admin = ar_mail_contact( $title_admin , $body_admin);

    $Hubspot = array(
			'url' => get_field('hubspot_api' , $post),
			'firstname' => $firstname,
			'lastname' 	=> $lastname,
			'email' 		=> $email,
			'phone' 		=> $phone,
		);

	endif;


	if($type == 'service'):
		//ENVIO AL USUARIO
		$subject_cli = 'Gracias por contactarnos';
		$title_cli 	 = 'Hola '.$firstname .' '.$lastname;
		$body_cli		 = '';
		$body_cli 	 .= '<p>Gracias por enviar tus datos alguien se comunicara contigo</p>';
		$body_cli 	 .= '<p>Pronto estaremos en contacto.</p>';
		$body_cli 	 .= '</br><p>Saludos,</p>';
		$popup 				= ar_modal('Gracias por comunicarme con nuestro equipo', 'En breve uno de nuestros asesores se estará comunicando directamente contigo');
		$message_cli  = ar_mail_contact( $title_cli, $body_cli );

		//ENVIO ADMIN
		$subject_admin 	= 'Nuevo Contacto';
		$title_admin 		= 'Hola Admin';
		$body_admin 		= '';
		$body_admin 	.= '<p>Un nuevo lead ha dejado sus datos para recibir más información sobre el proyecto '.get_the_title($proyect).'.</p>';
		if($firstname):
			$body_admin 	.= '<p><strong>Nombre : </strong> '.$firstname.'</p>';
		endif;
		if($lastname):
			$body_admin 	.= '<p><strong>Apellido : </strong> '.$lastname.'</p>';
		endif;
		if($phone):
			$body_admin 	.= '<p><strong>Teléfono : </strong> '.$phone.'</p>';
		endif;
		if($email):
			$body_admin 	.= '<p><strong>Correo : </strong> '.$email.'</p>';
		endif;
		if($dni):
			$body_admin 	.= '<p><strong>DNI : </strong> '.$dni.'</p>';
		endif;
		if($proyect):
			$body_admin 	.= '<p><strong>Proyecto : </strong> '.get_the_title($proyect).'</p>';
		endif;
		if($flat):
			$body_admin 	.= '<p><strong>Piso : </strong> '.$flat.'</p>';
		endif;
		if($tower):
			$body_admin 	.= '<p><strong>Torre : </strong> '.$tower.'</p>';
		endif;
		if($departament):
			$body_admin 	.= '<p><strong>Departameto: </strong> '.$departament.'</p>';
		endif;
		$message_admin = ar_mail_contact( $title_admin , $body_admin );
	endif;

	add_filter( 'wp_mail_content_type', 'ar_wp_mail_content_type' );
	add_filter( 'wp_mail_from_name', 'ar_wp_mail_from_name' );
	@wp_mail( $email , $subject_cli , $message_cli, $headers, $attachments);

	add_filter( 'wp_mail_content_type', 'ar_wp_mail_content_type' );
	add_filter( 'wp_mail_from_name', 'ar_wp_mail_from_name' );
	$send_admin = wp_mail(  $mail_contact , $subject_admin , $message_admin, $headers, $attachments );

	if(!is_wp_error($send_admin)):

		$wpdb->insert(
			$wpdb->prefix.'contact',
			array(
				'contact_firstname' 	=> $firstname,
				'contact_lastname' 		=> $lastname,
				'contact_phone' 			=> $phone,
				'contact_email' 			=> $email,
				'contact_dni' 				=> $dni,
				'contact_bedrooms' 		=> $bedrooms,
				'contact_flat' 				=> $flat,
				'contact_view' 				=> $view,
				'contact_departament' => $departament,
				'contact_hour' 				=> $hour,
				'contact_towe'				=> $tower,
				'contact_subject' 		=> $subject,
				'contact_msg' 				=> $msg,
				'contact_post' 				=> $post,
				'contact_proyect'			=> $proyect,
				'contact_sheet'				=> $contact_sheet,
				'contact_register' 		=> date('Y-m-d H:i:s', current_time('timestamp')),
			)
		);

		wp_send_json( array( 'status' => true , 'fragments' => $popup , 'hubspot' => $Hubspot, 'code' => $codex ));
	else:
		wp_send_json( array( 'status' => false ));
	endif;

	wp_die();
}

function ar_mail_contact( $title, $body , $executive = ''){
	ob_start();
  ?>
	<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
  <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
    <head>
			<!--[if gte mso 9]>
			<xml>
				<o:OfficeDocumentSettings>
					<o:AllowPNG/>
					<o:PixelsPerInch>96</o:PixelsPerInch>
				</o:OfficeDocumentSettings>
			</xml>
			<![endif]-->
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
			<meta name="viewport" content="width=device-width, initial-scale=1.0">
			<meta name="x-apple-disable-message-reformatting">
			<!--[if !mso]><!--><meta http-equiv="X-UA-Compatible" content="IE=edge"><!--<![endif]-->
			<title><?php echo get_bloginfo( 'name', 'display' ); ?></title>
		</head>

		<body class="" style="margin: 0;padding: 20px 0px;-webkit-text-size-adjust: 100%;background-color: #F2F2F2;color: #000000">
			<table style="max-width:600px; border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;min-width: 320px;Margin: 0 auto;background-color: #FFF;width:100%" cellpadding="0" cellspacing="0">
				<tbody>
					<tr style="vertical-align: top">
						<td style="word-break: break-word;border-collapse: collapse !important;vertical-align: top">
							<table bgcolor="#FDA100" style="background:#FDA100;font-family:arial,helvetica,sans-serif;padding-top: 20px;padding-bottom: 20px;" width="100%" cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td style="padding-right: 0px;padding-left: 0px;" align="center">
										<img align="center" border="0" src="<?= get_template_directory_uri(); ?>/src/img/arteco-logo-mail.png" alt="" title="" style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: inline-block !important;border: none;height: auto;float: none;width: 100%;max-width: 147px;" width="147"/>
									</td>
								</tr>
							</table>
							<table bgcolor="#FFFFFF" style="background:#FFFFFF;font-family:arial,helvetica,sans-serif;padding: 10px 0px;" width="100%" cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td style="padding-right: 30px;padding-left: 30px; padding-top: 10px;padding-bottom: 10px;" align="left">
										<h3 style="font-size:2rem;"><?= $title ?></h3>
										<?= $body ?>
									</td>
								</tr>
							</table>
							<table bgcolor="#22202D" style="background: #22202D;font-family:arial,helvetica,sans-serif;padding: 10px 0px;color: #FFFFFF" width="100%" cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td style="padding-right: 30px;padding-left: 30px; padding-top: 0px;padding-bottom: 0px;line-height: 1.5rem;" align="left">
										<table bgcolor="#22202D" style="background: #22202D;font-family:arial,helvetica,sans-serif;padding: 10px 0px;" width="50%" cellpadding="0" cellspacing="0" border="0">
											<tr>
												<td style="padding-right: 0px;padding-left: 0px; padding-top: 10px;padding-bottom: 10px;line-height: 1.5rem;" align="left">
													<p style="padding: 0px;margin:0px;">Todos los derechos reservados</p>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</tbody>
			</table>
			<?php if($executive): ?>
				</br>
				</br>
				</br>
				</br>
				<table  width="430" style="width:430px;background-color:#ffffff;margin-top:50px;margin-left:20px;">
					<tr>
						<td width="430" height="170" style="padding-left:20px;padding-bottom: 20px; font-family: Helvetica, Arial, sans-serif; font-size:13px; border-left: none; border-right: none; line-height:16px;" valign="bottom">
							<p style="font-size:24px;margin-bottom: 10px;color:#1B1464;"><b><?= ucfirst($executive['name']) ?></b></p>
							<p style="font-size:18px;margin:0px 0px 10px 0px;color:#1B1464;"><b>Asesor Inmobiliario</b></p>
							<p style="font-size:18px;margin:0px 0px 10px 0px;color:#1B1464;"><b>&Aacute;rea comercial | <?= $executive['proyect']; ?></b></p>
							<p style="font-size:14px;margin:0px 0px 10px 0px;color:#1B1464;"> <?= $executive['address']; ?></p>
							<p style="font-size:14px;margin:0px 0px 10px 0px;color:#1B1464;">Cel: +51 <?= $executive['phone'] ?></p>
							<p style="font-size:14px;margin:0px 0px 10px 0px;color:#1B1464;"> <a href="www.arteco.pe" style="color:#1B1464;"> www.arteco.pe </a></p>
							<p></p>
							<p>
								<a href="https://www.facebook.com/artecoperu" target="_blank">
									<img src="<?= get_template_directory_uri(); ?>/src/img/mails/facebook-logo-firma.jpg" width="32" height="32">
								</a>
								<a href="https://www.linkedin.com/company/arteco-inmobiliaria/" target="_blank">
									<img src="<?= get_template_directory_uri(); ?>/src/img/mails/linkedin-logo-firma.jpg" width="32" height="32">
								</a>
								<a href="https://www.youtube.com/user/InmobiliariaArteco" target="_blank">
									<img src="<?= get_template_directory_uri(); ?>/src/img/mails/youtube-logo-firma.jpg" width="32" height="32">
								</a>
								<a href="https://www.instagram.com/artecoperu/" target="_blank">
									<img src="<?= get_template_directory_uri(); ?>/src/img/mails/instagram-logo-firma.jpg" width="32" height="32">
								</a>
							</p>
							<p></p>
							<div style="width: 100%;position: relative;">
								<img src="<?= $executive['firm'] ?>" alt="Logo Acordeco" width="200" style="height: auto;">
							</div>
						</td>
					</tr>
				</table>
			<?php endif; ?>
		</body>

	</html>
	<?php
	$message = ob_get_clean();
	return  $message;
}

function ar_modal($title, $body){
	ob_start();
  ?>
  <div class="modal fade" id="modalThank" tabindex="-1" role="dialog" aria-labelledby="modalThank" aria-hidden="true">
    <div class="modal-dialog modal-sm modal-dialog-centered" role="document">
      <div class="modal-content border-radius-1 py-2 px-3">
        <div class="">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <img src="<?= get_template_directory_uri(); ?>/src/img/equis.png" alt="close">
          </button>
          <div class="py-5">
            <div class="pt-3 pb-1 px-4">
              <h2 class="h2 text-center"><?= $title ?></h2>
              <div class="text-center color-3 my-4"> <?= $body ?></div>
              <div class="text-center">
								<button class="butn butn-1" data-dismiss="modal" >Aceptar</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
	<?php
	$message = ob_get_clean();
	return  $message;
}

function ar_get_proyect( $post_id , $type){
	$departaments = get_field('planos', $post_id);
	$list  = array();
	if($departaments):
		foreach ( $departaments as $departament):
			if(isset($departament[$type]) && $departament[$type] != '' &&  $departament['enabled'] ):
				array_push($list, trim($departament[$type]));
			endif;
		endforeach;
	endif;
	return array_unique($list);
}

function ar_get_hour($start, $end , $interval){
	$list 	= array();
	$_start = new DateTime($start);
	$_end 	= new DateTime($end);
	$range 	= new DatePeriod($_start, new DateInterval('PT'.$interval.'M'), $_end);
	foreach($range as $time):
		array_push($list, $time->format('G:i a'));
	endforeach;

	return $list;
}

function ar_mails_admin(){
	$mail = get_option('admin_email');
	if( get_field('admin_contacto', 'option') ):
		$mail = explode('|',get_field('admin_contacto', 'option'));
	endif;
	return $mail;
}

add_action('wp_ajax_nopriv_ar_ajax_departament', 'ar_ajax_departament');
add_action('wp_ajax_ar_ajax_departament', 'ar_ajax_departament');
function ar_ajax_departament( ){
	$post_id = $_POST['post_id'];
	$dep = array();
	if( $post_id ):
		$dep = get_field('planos', $post_id);
	endif;
	wp_send_json($dep);
	wp_die();
}

function ar_generate_code( $post_id ){
	global $wpdb;
	$db_table 		= $wpdb->prefix.'contact';
	$c 						= date('Y-m-d',current_time('timestamp'));
	$rows 				= $wpdb->get_results("SELECT *  FROM $db_table WHERE DATE(contact_register) = '".$c."' AND contact_proyect ='".$post_id."' AND  contact_sheet ='1'" );
	$code 				= '20';
	$code_proyect = get_field('sheet_code_proyect', $post_id);
	$items 				= ar_zero_fill(count($rows) + 1 , 5);
	$day 					= date( 'd', current_time('timestamp') );
  $month 				= date( 'm', current_time('timestamp') );
  $year 				= date( 'Y', current_time('timestamp') );
	return array(
		'correlative' => $code .''.$code_proyect.''.$day.''.$month.''.$year.''. $items,
    'number'      => count($rows) + 1
	);
}

function ar_zero_fill( $valor, $long = 0 ){
	return str_pad($valor, $long, '0', STR_PAD_LEFT);
}

function ar_get_ejecutive( $post_id, $int){
	$ejecutives = get_field('sheet_ejecutive_proyect', $post_id);
	if(intval($int) % 2 == 0):
		$ejecutive = $ejecutives[0];
	else:
		$ejecutive = $ejecutives[1];
	endif;
	return $ejecutive;
}

function ar_get_departament($post_id , $number, $object = false){
	$list = get_field('planos', $post_id);
	$return = '';
	if($list):
		foreach ( $list as $item):
			if($item['depa'] === $number):
				if($object):
					$return = ar_departament_obj( $item );
				else:
					$return = $item;
				endif;
			endif;
		endforeach;
	endif;
	return $return;
}

function ar_departament_obj( $ob ){
	$obj = new \stdClass;
	if( $obj ):
    $obj->number              = $ob['depa']; //NÚMERO
    $obj->floor               = (int) $ob['pisos']; //PISO
    $obj->bedroom             = (int) $ob['dormitorios']; //DORMITORIOS
    $obj->view                = $ob['tipo_de_vista']; //VISTA
    $obj->price               = $ob['precio']; //PRECIO
    $obj->footage             = $ob['footage']; //METRAJE
    $obj->toilets             = $ob['banos']; //BAÑOS
		$obj->tower             	= $ob['torre'] ? $ob['torre'] : ''; //TORRE
    $obj->flat_image 					= wp_get_original_image_path($ob['distribution']);//IMG PLANO BRO
	endif;
	return $obj;
}

function ar_url_server($server , $forwarded_host = false , $request = true){
	$ssl      = ( ! empty( $server['HTTPS'] ) && $server['HTTPS'] == 'on' );
	$sp       = strtolower( $server['SERVER_PROTOCOL'] );
	$protocol = substr( $sp, 0, strpos( $sp, '/' ) ) . ( ( $ssl ) ? 's' : '' );
	$port     = $server['SERVER_PORT'];
	$port     = ( ( ! $ssl && $port=='80' ) || ( $ssl && $port == '443' ) ) ? '' : ':'.$port;
	$host     = ( $forwarded_host && isset( $server['HTTP_X_FORWARD$ED_HOST'] ) ) ? $server['HTTP_X_FORWARDED_HOST'] : ( isset( $server['HTTP_HOST'] ) ? $server['HTTP_HOST'] : null );
	$host     = isset( $host ) ? $host : $server['SERVER_NAME'] . $port;

	$url = $protocol . '://' . $host;
	if($request):
		$url = $url . $server['REQUEST_URI'];
	endif;
	return $url;
}

function ar_get_utm($url){
	$query = parse_url($url);
  parse_str($query['query'], $results);
  return $results;
}

function ar_generate_pdf( $data ){
	ob_start();
    ?>

    <style type="text/css">
      table.page_table {width: 100%; border: none;border-collapse: collapse;}
      .page_border-bottom {border-bottom : 2px solid #000000!important;}
      .page_border-top {border-top : 2px solid #000000!important;}
      .page_border-right {border-right : 2px solid #000000!important;}
      .page_border-left {border-left : 2px solid #000000!important;}
      div.note {padding: 2mm;  width: 100%; font-size: 8pt ;line-height:9.6pt;}
    </style>
    <page backtop="14mm" backbottom="14mm" backleft="10mm" backright="10mm" style="font-size: 12pt">
      <page_header></page_header>
      <?php if($data['extra']['logo_footer']): ?>
        <page_footer>
          <img src="<?= $data['extra']['logo_footer'] ; ?>" alt="Logo Acordeco" style="width: 100%;" />
        </page_footer>
      <?php endif; ?>
      <table class="page_table">
        <tr>
          <td style="width: 50%; text-align: left;">

            <table class="page_table page_border" >
              <tr>
                <td class="page_border-left page_border-bottom page_border-top" style="width: 25mm; text-align: left;font-size:12pt;padding:2.5mm 3mm;">
									<strong>Proforma:</strong>
								</td>
                <td class="page_border-right page_border-bottom page_border-top" style="width: 38mm; text-align: right;font-size:12pt;color:#706F6F;padding:2.5mm 3mm;">
									<span><?= $data['proyect']['currency']; ?></span>
								</td>
              </tr>
              <tr>
                <td class="page_border-left page_border-bottom page_border-top" style="width: 25mm; text-align: left;font-size:12pt;padding:2.5mm 3mm;">
									<strong>Fecha y hora:</strong>
								</td>
                <td class="page_border-right page_border-bottom page_border-top" style="width: 38mm; text-align: right;font-size:12pt;color:#706F6F;padding:2.5mm 3mm;">
									<span><?= date('d/m/Y h:i A',current_time('timestamp')); ?></span>
								</td>
              </tr>
            </table>

          </td>
          <td style="width: 50%; text-align: right">
            <?php if($data['extra']['logo_proyect']): ?>
              <img src="<?= $data['extra']['logo_proyect']; ?>" alt="Logo Acordeco" style="width:53.5673 mm;height:15.527mm">
            <?php endif; ?>
          </td>
        </tr>
      </table>
      <br><br>
      <h2>Cliente:</h2>
      <table class="page_table">
        <tr>
          <td style="width: 50%; text-align: left;">
            <table class="page_table page_border" style="width:100%">
              <tr>
                <td style="width: 20mm; text-align: left;font-size:12pt;padding:2.5mm 3mm;padding-left:0;">
									<strong>Estimado(a)</strong>
								</td>
                <td style="width: 30mm; text-align: left;font-size:12pt;color:#706F6F;padding:2.5mm 3mm;">
									<span><?= $data['client']['firstname']; ?></span>
								</td>
              </tr>
              <tr>
                <td style="width: 20mm; text-align: left;font-size:12pt;padding:2.5mm 3mm;padding-left:0;">
									<strong>DNI:</strong>
								</td>
                <td style="width: 30mm; text-align: left;font-size:12pt;color:#706F6F;padding:2.5mm 3mm;">
									<span><?= $data['client']['document']; ?></span>
								</td>
              </tr>
            </table>
          </td>
          <td style="width: 50%; text-align: left">
            <table class="page_table page_border" style="width:100%">
              <tr>
                <td style="width: 20mm; text-align: left;font-size:12pt;padding:2.5mm 3mm;padding-left:0;">
									<strong>Teléfono</strong>
								</td>
                <td style="width: 30mm; text-align: left;font-size:12pt;color:#706F6F;padding:2.5mm 3mm;">
									<span><?= $data['client']['phone']; ?></span>
								</td>
              </tr>
              <tr>
                <td style="width: 20mm; text-align: left;font-size:12pt;padding:2.5mm 3mm;padding-left:0;">
									<strong>E-mail</strong>
								</td>
                <td style="width: 35mm; text-align: left;font-size:12pt;color:#706F6F;padding:2.5mm 3mm;">
									<span><?= $data['client']['email']; ?></span>
								</td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
      <br><br>

      <h2>Proyecto:</h2>
      <table class="page_table">
        <tr>
          <td style="width: 80%; text-align: left;">
            <table class="page_table page_border" style="width:120mm">
              <tr>
                <td style="width: 25mm; text-align: left;font-size:12pt;padding:2.5mm 3mm;padding-left:0;"><strong>Nombre</strong></td>
                <td style="width: 80mm; text-align: left;font-size:12pt;color:#706F6F;padding:2.5mm 3mm;"><span><?= $data['proyect']['name']; ?></span></td>
              </tr>
              <tr>
                <td style="width: 25mm; text-align: left;font-size:12pt;padding:2.5mm 3mm;padding-left:0;"><strong>Dirección</strong></td>
                <td style="width: 80mm; text-align: left;font-size:12pt;color:#706F6F;padding:2.5mm 3mm;"><span><?= $data['proyect']['address']; ?></span></td>
              </tr>
              <tr>
                <td style="width: 25mm; text-align: left;font-size:12pt;padding:2.5mm 3mm;padding-left:0;"><strong>Etapa</strong></td>
                <td style="width: 80mm; text-align: left;font-size:12pt;color:#706F6F;padding:2.5mm 3mm;"><span><?= $data['proyect']['status']; ?></span></td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
      <br><br>

      <h2>Ejecutivo:</h2>
      <table class="page_table">
        <tr>
          <td style="width: 80%; text-align: left;">
            <table class="page_table page_border" style="width:120mm">
              <tr>
                <td style="width: 25mm; text-align: left;font-size:12pt;padding:2.5mm 3mm;padding-left:0;"><strong>Nombre</strong></td>
                <td style="width: 80mm; text-align: left;font-size:12pt;color:#706F6F;padding:2.5mm 3mm;"><span><?= $data['executive']['name_ejecutive']; ?></span></td>
              </tr>
              <tr>
                <td style="width: 25mm; text-align: left;font-size:12pt;padding:2.5mm 3mm;padding-left:0;"><strong>Celular</strong></td>
                <td style="width: 80mm; text-align: left;font-size:12pt;color:#706F6F;padding:2.5mm 3mm;"><span><?= $data['executive']['phone_ejecutive']; ?></span></td>
              </tr>
              <tr>
                <td style="width: 25mm; text-align: left;font-size:12pt;padding:2.5mm 3mm;padding-left:0;"><strong>Correo</strong></td>
                <td style="width: 80mm; text-align: left;font-size:12pt;color:#706F6F;padding:2.5mm 3mm;"><span><?= $data['executive']['email_ejecutive']; ?></span></td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
      <br><br>

      <h2>Datos de la propiedad:</h2>
      <table class="page_table">
        <tr>
          <td style="width: 50%; text-align: left;">
            <table class="page_table page_border" style="width:50%">
              <tr>
                <td style="width: 30mm; text-align: left;font-size:12pt;padding:2.5mm 3mm;padding-left:0;"><strong>N° Dpto. Torre</strong></td>
                <td style="width: 30mm; text-align: left;font-size:12pt;color:#706F6F;padding:2.5mm 3mm;"><span><?= $data['departament']->number; ?></span></td>
              </tr>
              <tr>
                <td style="width: 30mm; text-align: left;font-size:12pt;padding:2.5mm 3mm;padding-left:0;"><strong>Vista:</strong></td>
                <td style="width: 30mm; text-align: left;font-size:12pt;color:#706F6F;padding:2.5mm 3mm;"><span><?= $data['departament']->view; ?></span></td>
              </tr>
              <tr>
                <td style="width: 30mm; text-align: left;font-size:12pt;padding:2.5mm 3mm;padding-left:0;"><strong>Precio:</strong></td>
                <td style="width: 30mm; text-align: left;font-size:12pt;color:#706F6F;padding:2.5mm 3mm;"><span>S/ <?= $data['departament']->price; ?></span></td>
              </tr>
            </table>
          </td>
          <td style="width: 50%; text-align: left;">
            <table class="page_table page_border" style="width:50%">
              <tr>
                <td style="width: 30mm; text-align: left;font-size:12pt;padding:2.5mm 3mm;padding-left:0;"><strong>Piso</strong></td>
                <td style="width: 80mm; text-align: left;font-size:12pt;color:#706F6F;padding:2.5mm 3mm;"><span><?= $data['departament']->floor; ?></span></td>
              </tr>
              <tr>
                <td style="width: 30mm; text-align: left;font-size:12pt;padding:2.5mm 3mm;padding-left:0;"><strong>Metraje</strong></td>
                <td style="width: 80mm; text-align: left;font-size:12pt;color:#706F6F;padding:2.5mm 3mm;"><span><?= $data['departament']->footage; ?> m²</span></td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
      <br><br>

      <h2>Plano del departamento:</h2>
        <br><br><br><br><br>
        <div style="width:220mm;height:2px;"></div>
        <div style="text-align: center; width: 100%;">
          <?php if($data['departament']->flat_image != ''): ?>
            <br>
              <img src="<?= $data['departament']->flat_image; ?>" alt="plano" style="width: 150mm;">
            <br>
          <?php endif; ?>
        </div>
        <div style="width:220mm;height:2px;"></div>
        <br><br>
        <div class="note">
            Horario de atención: De lunes a domingo de 10 am. a 7 pm. (horario corrido)<br><br>
            La presente cotización tiene una vigencia de 72 horas desde la fecha de emisión del documento. No constituye reserva de
            compra ni una fijación en el precio.<br><br>
            A través de la presente nos autoriza el uso de sus datos personales para tratamientos que supongan desarrollo del objetoo social
            de ARTECO y/o de otras empresas del Grupo. Garantizamos la confidencialidad y seguridad de los datos proporcionados por
            cada usuario, cumpliendo con lo dispuesto en la Ley N°29733, ley de Protección de Datos Personales y su reglamento.';
        </div>
        <br><br><br><br><br>
    </page>

    <?php
    $content = ob_get_clean();
    return $content;
}

function ar_save_sheet( $data , $id , $name){
	$list = array();
	if($id != '' && $name != ''):
		array_push($list, date("d/m/Y",current_time("timestamp")));        //FECHA j \d\e F \d\e Y
		array_push($list, date('H:i:s',current_time('timestamp')));        //HORA
		array_push($list, $data['proyect']['currency'] 	? $data['proyect']['currency'] 		: '');        //ID
		array_push($list, $data['client']['firstname'] 	? $data['client']['firstname'] 		: '');        //NOMBRE
		array_push($list, $data['client']['lastname'] 	? $data['client']['lastname'] 		: '');        //APELLIDO
		array_push($list, $data['client']['document'] 	? (int) $data['client']['document'] : '');      //DNI
		array_push($list, $data['client']['phone'] 			? (int)$data['client']['phone'] 	: '' );       //TELEFONO
		array_push($list, $data['client']['email'] 			? $data['client']['email'] 				: '');        //CORREO
		array_push($list, $data['proyect']['name'] 			? $data['proyect']['name'] 				: '');        //PROYECTO
		array_push($list, $data['proyect']['status'] 		? $data['proyect']['status'] 			: '');        //ETAPA
		array_push($list, $data['executive']['name_ejecutive'] 	? $data['executive']['name_ejecutive'] 		: '');        //EJECUTIVO
		if($data['departament'] != ''):
			array_push($list, $data['departament']->number 	? $data['departament']->number 					: '');        //NUMERO DE DEPARTAMENTO
			array_push($list, $data['departament']->tower   ? $data['departament']->tower  					: '');        //TORRE
			array_push($list, $data['departament']->floor 	? (int)$data['departament']->floor 			: '');        //PISO
			array_push($list, $data['departament']->bedroom ? (int)$data['departament']->bedroom 		: '');        //NUMERO DE DORMITORIOS
			array_push($list, $data['departament']->view 		? $data['departament']->view 						: '');        //VISTA
			array_push($list, $data['departament']->price 	? (float)$data['departament']->price 		: '');        //PRECIO
			array_push($list, $data['departament']->footage ? (float)$data['departament']->footage 	: '');        //METRAJE
		else:
			array_push($list, '');
			array_push($list, '');
			array_push($list, '');
			array_push($list, '');
			array_push($list, '');
			array_push($list, '');
			array_push($list, '');
		endif;
		if($data['utm']):
			array_push($list, $data['utm']['utm_source'] 		? $data['utm']['utm_source'] 	: '');        //SOURCE
			array_push($list, $data['utm']['utm_medium'] 		? $data['utm']['utm_medium'] 	: '');        //MEDIUM
			array_push($list, $data['utm']['utm_campaign'] 	? $data['utm']['utm_campaign']: '');        //CHAMPAIGN
			array_push($list, $data['utm']['utm_term'] 			? $data['utm']['utm_term'] 		: '');        //TERM
			array_push($list, $data['utm']['utm_content'] 	? $data['utm']['utm_content'] : '');        //CONTENT
		endif;

		return ar_wp_insert_google( $id , $name , $list );
	else:
		return false;
	endif;
}

//LIBRO DE RECLAMACIONES
add_action( 'init', 'ar_generate_tables_claim');
function ar_generate_tables_claim(){
	global $wpdb;

	$db_table_claim = $wpdb->prefix.'claim';
	if( $wpdb->get_var( "SHOW TABLES LIKE '$db_table_claim'" ) != $db_table_claim ) {
		if ( ! empty( $wpdb->charset ) )
			$charset_collate = "DEFAULT CHARACTER SET {$wpdb->charset}";
		if ( ! empty( $wpdb->collate ) )
			$charset_collate .= " COLLATE {$wpdb->collate}";

			$sqlc = "CREATE TABLE {$db_table_claim} (
					claim_id bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT,
					claim_code varchar(255) NULL ,
					claim_proyect bigint(10) NULL ,
					claim_firstname text NULL ,
					claim_lastname text NULL ,
					claim_phone varchar(255) NULL ,
					claim_email text NULL ,
					claim_dni varchar(255) NULL ,
					claim_address text NULL ,
					claim_adult varchar(255) NULL ,
					legal_firstname text NULL ,
					legal_lastname text NULL ,
					legal_phone varchar(255) NULL ,
					legal_email text NULL ,
					legal_dni varchar(255) NULL ,
					legal_address text NULL ,
					claim_service varchar(255) NULL ,
					claim_type varchar(255) NULL ,
					claim_msg text NULL ,
					claim_gcaptcha text NULL ,
					claim_post bigint(10) NULL ,
					claim_register  datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
					PRIMARY KEY (claim_id)
			) {$charset_collate};";
			require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
			dbDelta($sqlc);
	}
}

add_action( 'admin_menu', 'ar_menu_claim');
function ar_menu_claim() {
	add_menu_page( 'Reclamaciones', 'Reclamaciones', 'manage_options', 'manage_claim', 'ar_manage_claim', 'dashicons-format-aside', 3 );
}

add_action( 'admin_enqueue_scripts', 'ar_claim_admin_script' );
function ar_claim_admin_script( $hook ) {
	if ( 'toplevel_page_manage_claim' == $hook ) {
		wp_enqueue_style( 'boostrap-datatable', '//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css', false, '4.5.2' );
		wp_enqueue_style( 'datatable-css', '//cdn.datatables.net/1.10.23/css/dataTables.bootstrap4.min.css', false, '1.10.23' );
		wp_enqueue_style( 'dataTables-css', '//cdn.datatables.net/buttons/1.6.5/css/buttons.dataTables.min.css', false, '1.6.5' );
		wp_enqueue_style( 'contact-css', get_template_directory_uri().'/src/contact/contact.css', false, '1.6' );

		wp_enqueue_script('dat-js', '//cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js', array( 'jquery' ), time(), true);
		wp_enqueue_script('dat-js-button', '//cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js', array( 'jquery' ), time(), true);
		wp_enqueue_script('dat-js-zip', '//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js', array( 'jquery' ), time(), true);
		wp_enqueue_script('dat-js-pdf', '//cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js', array( 'jquery' ), time(), true);
		wp_enqueue_script('dat-js-fonts', '//cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js', array( 'jquery' ), time(), true);
		wp_enqueue_script('dat-js-html', '//cdn.datatables.net/buttons/1.6.5/js/buttons.html5.min.js', array( 'jquery' ), time(), true);
		wp_enqueue_script('dat-js-print', '//cdn.datatables.net/buttons/1.6.5/js/buttons.print.min.js', array( 'jquery' ), time(), true);
		wp_enqueue_script('dat-boostrap-js', '//cdn.datatables.net/1.10.23/js/dataTables.bootstrap4.min.js', array( 'jquery' ), time(), true);
		wp_enqueue_script('contact-js', get_template_directory_uri().'/src/contact/contact.js', array( 'jquery' ), time(), true);
	}
}

function ar_manage_claim(){
	global $wpdb;
	$contact = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}claim ", OBJECT );
	?>
	<div class="wrap">
    <h2 class="--h2">Reclamaciones <?= get_bloginfo('name'); ?></h2>
    <div class="clear" style="height:20px;width:100%"></div>
			<table id="contact" class="table table-striped table-bordered" style="width:100%">
				<thead>
					<tr>
						<th>Codigo</th>
						<th>Proyecto</th>
						<th>Reclamante</th>
						<th>Adulto</th>
						<th>Representante</th>
						<th>Servicio</th>
						<th>Tipo</th>
						<th>Mensaje</th>
						<th>Pagina</th>
						<th>Fecha</th>
					</tr>
        </thead>
				<tbody>
					<?php foreach ($contact as $ky => $c): ?>
						<tr>
							<td><?= $c->claim_code; ?></td>
							<td><?= get_the_title( $c->claim_proyect ); ?></td>
							<td>
								<div style="position:relative;font-size: 12px;">
									<span> <strong> Nombre: </strong> <?= $c->claim_firstname; ?> </span></br>
									<span> <strong> Apellido: </strong> <?= $c->claim_lastname; ?> </span></br>
									<span> <strong> Teléfono: </strong> <?= $c->claim_phone; ?> </span></br>
									<span> <strong> Correo: </strong> <?= $c->claim_email; ?> </span></br>
									<span> <strong> DNI: </strong> <?= $c->claim_dni; ?> </span></br>
									<span> <strong> Domicilio: </strong> <?= $c->claim_address; ?> </span>
								</div>
							</td>
							<td><?= ($c->claim_adult == 'yes' ? 'SI' : 'NO'); ?></td>
							<td>
								<div style="position:relative;font-size: 12px;">
									<span> <strong> Nombre: </strong> <?= $c->legal_firstname; ?> </span></br>
									<span> <strong> Apellido: </strong> <?= $c->legal_lastname; ?> </span></br>
									<span> <strong> Teléfono: </strong> <?= $c->legal_phone; ?> </span></br>
									<span> <strong> Correo: </strong> <?= $c->legal_email; ?> </span></br>
									<span> <strong> DNI: </strong> <?= $c->legal_dni; ?> </span></br>
									<span> <strong> Domicilio: </strong> <?= $c->legal_address; ?> </span>
								</div>
							</td>
							<td> <?= $c->claim_service; ?> </td>
							<td> <?= $c->claim_type; ?> </td>
							<td> <?= $c->claim_msg; ?> </td>
							<td> <?= get_the_title( $c->claim_post ); ?> </td>
							<td> <?= $c->claim_register; ?> </td>
						</tr>
          <?php endforeach; ?>
				</tbody>
				<tfoot>
					<tr>
						<th>Codigo</th>
						<th>Proyecto</th>
						<th>Reclamante</th>
						<th>Adulto</th>
						<th>Representante</th>
						<th>Servicio</th>
						<th>Tipo</th>
						<th>Mensaje</th>
						<th>Pagina</th>
						<th>Fecha</th>
					</tr>
				<tfoot>
	<?php
}

function ar_generate_code_claim( $type , $proyect ){
	global $wpdb;
	$db_table 		= $wpdb->prefix.'claim';
	$c 						= date('Y-m-d',current_time('timestamp'));
	$code 				= ($type == 'Queja' ? 'QA' : 'RA');
	$rows 				= $wpdb->get_results("SELECT *  FROM $db_table WHERE claim_type = '".$type."'" );
	$code_proyect = get_field('sheet_code_proyect', $proyect );
	$items 				= ar_zero_fill(count($rows) + 1 , 5);
	$day 					= date( 'd', current_time('timestamp') );
  $month 				= date( 'm', current_time('timestamp') );
  $year 				= date( 'Y', current_time('timestamp') );

	return  $code.$code_proyect.$items;
}

add_action('wp_ajax_nopriv_ar_ajax_claim', 'ar_ajax_claim');
add_action('wp_ajax_ar_ajax_claim', 'ar_ajax_claim');
function ar_ajax_claim(){
	global $wpdb;
	$claim_proyect 		= $_POST['proyect'];
	$claim_firstname 	= $_POST['claimant-first-name'];
	$claim_lastname 	= $_POST['claimant-last-name'];
	$claim_phone 			= $_POST['claimant-phone'];
	$claim_email 			= $_POST['claimant-email'];
	$claim_dni 				= $_POST['claimant-dni'];
	$claim_address 		= $_POST['claimant-address'];
	$claim_adult 			= $_POST['claimant-adult'];
	$legal_firstname 	= $_POST['legal-first-name'];
	$legal_lastname 	= $_POST['legal-last-name'];
	$legal_phone 			= $_POST['legal-phone'];
	$legal_email 			= $_POST['legal-email'];
	$legal_dni 				= $_POST['legal-dni'];
	$legal_address 		= $_POST['legal-address'];
	$claim_service 		= $_POST['legal-service'];
	$claim_type 			= $_POST['claim-type'];
	$claim_msg 				= $_POST['claim-messange'];
	$claim_gcaptcha 	= $_POST['gcaptcha'];
	$claim_post 			= $_POST['page'];

	$claim_code 			= ar_generate_code_claim( $claim_type , $claim_proyect );

	//ENVIO AL USUARIO
	$subject_cli = 'Gracias por contactarnos';
	$title_cli 	 = 'Hola '.$claim_firstname .' '.$claim_lastname .' Gracias por ayudar a mejorar nuestro servicio';
	$body		 = '';
	$body 	 .= '<div style="padding:20px;border: 1px solid #000000;">';
	$body 	 .= '<p><strong>N° Reclamo: '.$claim_code.'</strong></p></br>';
	$body 	 .= '<p><strong>Identificación del bien contratado</strong></p>';
	if($claim_service):
		$body 	 .= '<p>'.$claim_service.'</p>';
	endif;
	if($claim_msg):
		$body 	 .= '<p>Descripción del '.$claim_type.': '.$claim_msg.'</p>';
	endif;
	$body 	 .= '</div></br>';

	if( $claim_proyect ):
		$body 	 .= '<p><strong>Proyecto : </strong>'.get_The_title($claim_proyect).'</p></br>';
	endif;

	$body 	 .= '<p><strong>Reclamante</strong></p></br>';

	if( $claim_firstname && $claim_lastname ):
		$body 	 .= '<p><strong>Nombres y Apellidos : </strong>'.$claim_firstname .' '.$claim_lastname.'</p>';
	endif;

	if( $claim_dni ):
		$body 	 .= '<p><strong>Documento: : </strong>'.$claim_dni.'</p>';
	endif;

	if( $claim_email ):
		$body 	 .= '<p><strong>Email : </strong>'.$claim_email.'</p>';
	endif;

	if( $claim_phone ):
		$body 	 .= '<p><strong>Número de celular : </strong>'.$claim_phone.'</p>';
	endif;

	if( $claim_address ):
		$body 	 .= '<p><strong>Domicilio : </strong>'.$claim_address.'</p>';
	endif;

	if( $claim_adult ):
		$body 	 .= '<p><strong>Eres Adulto: : </strong>'.($claim_adult == 'yes' ? 'SI' : 'NO' ).'</p>';
	endif;

	$body 	 .= '</br><p><strong>Representante Legal</strong></p></br>';

	if( $legal_firstname && $legal_lastname ):
		$body 	 .= '<p><strong>Nombres y Apellidos : </strong>'.$legal_firstname .' '.$legal_lastname.'</p>';
	endif;

	if( $legal_dni ):
		$body 	 .= '<p><strong>Documento: : </strong>'.$legal_dni.'</p>';
	endif;

	if( $legal_email ):
		$body 	 .= '<p><strong>Email : </strong>'.$legal_email.'</p>';
	endif;

	if( $legal_phone ):
		$body 	 .= '<p><strong>Número de celular : </strong>'.$legal_phone.'</p>';
	endif;

	if( $legal_address ):
		$body 	 .= '<p><strong>Domicilio : </strong>'.$legal_address.'</p>';
	endif;

	$body 	 .= '</br><p>Pronto estaremos en contacto.</p>';

	$popup 				= ar_modal('Gracias por ayudarnos a mejorar', 'Tu '.$claim_type.' ha sido registrado exitosamente con <br> el <strong>N° '.$claim_code.'</strong> en nuestra base de datos.');
	$message_cli  = ar_mail_contact( $title_cli, $body );

	//ENVIO ADMIN
	if($claim_type == 'Queja'):
		$subject_admin 	= 'Nueva Queja ( '.$claim_code.' )';
	else:
		$subject_admin 	= 'Nuevo Reclamo ( '.$claim_code.' )';
	endif;

	$title_admin 		= 'Hola Admin';

	$message_admin = ar_mail_contact( $title_admin , $body );

	add_filter( 'wp_mail_content_type', 'ar_wp_mail_content_type' );
	add_filter( 'wp_mail_from_name', 'ar_wp_mail_from_name' );
	@wp_mail( $claim_email , $subject_cli , $message_cli);

	add_filter( 'wp_mail_content_type', 'ar_wp_mail_content_type' );
	add_filter( 'wp_mail_from_name', 'ar_wp_mail_from_name' );
	$send_admin = wp_mail(  ar_mails_admin() , $subject_admin , $message_admin );

	if(!is_wp_error($send_admin)):

		$bdStatu = $wpdb->insert(
			$wpdb->prefix.'claim',
			array(
				'claim_code' 			=> $claim_code,
				'claim_proyect' 	=> $claim_proyect,
				'claim_firstname' => $claim_firstname,
				'claim_lastname'	=> $claim_lastname,
				'claim_phone' 		=> $claim_phone,
				'claim_email' 		=> $claim_email,
				'claim_dni' 			=> $claim_dni,
				'claim_address' 	=> $claim_address,
				'claim_adult' 		=> $claim_adult,
				'legal_firstname' => $legal_firstname,
				'legal_lastname' 	=> $legal_lastname,
				'legal_phone' 		=> $legal_phone,
				'legal_email' 		=> $legal_email,
				'legal_dni' 			=> $legal_dni,
				'legal_address' 	=> $legal_address,
				'claim_service' 	=> $claim_service,
				'claim_type' 			=> $claim_type,
				'claim_msg' 			=> $claim_msg,
				'claim_gcaptcha' 	=> $claim_gcaptcha,
				'claim_post'			=> $claim_post,
				'claim_register'	=> date('Y-m-d H:i:s', current_time('timestamp'))
			)
		);

		wp_send_json( array( 'status' => true , 'fragments' => $popup ));
	else:
		wp_send_json( array( 'status' => false ));
	endif;

	wp_die();

}

//SUSCRIPCIONES
add_action( 'init', 'ar_generate_tables_newsletter');
function ar_generate_tables_newsletter(){
	global $wpdb;

	$db_table_newsletter = $wpdb->prefix.'newsletter';
	if( $wpdb->get_var( "SHOW TABLES LIKE '$db_table_newsletter'" ) != $db_table_newsletter ) {
		if ( ! empty( $wpdb->charset ) )
			$charset_collate = "DEFAULT CHARACTER SET {$wpdb->charset}";
		if ( ! empty( $wpdb->collate ) )
			$charset_collate .= " COLLATE {$wpdb->collate}";

			$sqlc = "CREATE TABLE {$db_table_newsletter} (
					newsletter_id bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT,
					newsletter_firstname text NULL ,
					newsletter_lastname text NULL ,
					newsletter_email text NULL ,
					newsletter_post bigint(10) NULL ,
					newsletter_register  datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
					PRIMARY KEY (newsletter_id)
			) {$charset_collate};";
			require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
			dbDelta($sqlc);
	}
}

add_action( 'admin_menu', 'ar_menu_newsletter');
function ar_menu_newsletter() {
	add_menu_page( 'Suscripciones', 'Suscripciones', 'manage_options', 'manage_newsletter', 'ar_manage_newsletter', '', 3 );
}

add_action( 'admin_enqueue_scripts', 'ar_newsletter_admin_script' );
function ar_newsletter_admin_script( $hook ) {
	if ( 'toplevel_page_manage_newsletter' == $hook ) {
		wp_enqueue_style( 'boostrap-datatable', '//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css', false, '4.5.2' );
		wp_enqueue_style( 'datatable-css', '//cdn.datatables.net/1.10.23/css/dataTables.bootstrap4.min.css', false, '1.10.23' );
		wp_enqueue_style( 'dataTables-css', '//cdn.datatables.net/buttons/1.6.5/css/buttons.dataTables.min.css', false, '1.6.5' );
		wp_enqueue_style( 'contact-css', get_template_directory_uri().'/src/contact/contact.css', false, '1.6' );

		wp_enqueue_script('dat-js', '//cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js', array( 'jquery' ), time(), true);
		wp_enqueue_script('dat-js-button', '//cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js', array( 'jquery' ), time(), true);
		wp_enqueue_script('dat-js-zip', '//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js', array( 'jquery' ), time(), true);
		wp_enqueue_script('dat-js-pdf', '//cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js', array( 'jquery' ), time(), true);
		wp_enqueue_script('dat-js-fonts', '//cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js', array( 'jquery' ), time(), true);
		wp_enqueue_script('dat-js-html', '//cdn.datatables.net/buttons/1.6.5/js/buttons.html5.min.js', array( 'jquery' ), time(), true);
		wp_enqueue_script('dat-js-print', '//cdn.datatables.net/buttons/1.6.5/js/buttons.print.min.js', array( 'jquery' ), time(), true);
		wp_enqueue_script('dat-boostrap-js', '//cdn.datatables.net/1.10.23/js/dataTables.bootstrap4.min.js', array( 'jquery' ), time(), true);
		wp_enqueue_script('contact-js', get_template_directory_uri().'/src/contact/contact.js', array( 'jquery' ), time(), true);
	}
}

function ar_manage_newsletter(){
	global $wpdb;
	$contact = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}newsletter ", OBJECT );
	?>
	<div class="wrap">
    <h2 class="--h2">Reclamaciones <?= get_bloginfo('name'); ?></h2>
    <div class="clear" style="height:20px;width:100%"></div>
			<table id="contact" class="table table-striped table-bordered" style="width:100%">
				<thead>
					<tr>
						<th>Nombre</th>
						<th>Apellido</th>
						<th>Correo</th>
						<th>Pagina</th>
						<th>Fecha</th>
					</tr>
        </thead>
				<tbody>
					<?php foreach ($contact as $ky => $c): ?>
						<tr>
							<td><?= $c->newsletter_firstname; ?></td>
              <td><?= $c->newsletter_lastname; ?></td>
              <td><?= $c->newsletter_email; ?></td>
							<td> <?= get_the_title( $c->newsletter_post ); ?> </td>
							<td> <?= $c->newsletter_register; ?> </td>
						</tr>
          <?php endforeach; ?>
				</tbody>
				<tfoot>
					<tr>
            <th>Nombre</th>
						<th>Apellido</th>
						<th>Correo</th>
						<th>Pagina</th>
						<th>Fecha</th>
					</tr>
				<tfoot>
	<?php
}

add_action('wp_ajax_nopriv_ar_ajax_newsletter', 'ar_ajax_newsletter');
add_action('wp_ajax_ar_ajax_newsletter', 'ar_ajax_newsletter');
function ar_ajax_newsletter(){
	global $wpdb;
	$firstname 	= $_POST['firstname'];
	$lastname 	= $_POST['lastname'];
  $email 	    = $_POST['email'];
	$post 			= $_POST['post'];

	//ENVIO AL USUARIO
	$subject_cli = 'Gracias por suscribirte';
	$title_cli 	 = 'Hola '.$firstname .' '.$lastname .'';
	$body		 = '';
	$body 	 .= '<p>Gracias por suscribirte a nuestro boletin de noticias</p>';
	$body 	 .= '</br><p>Pronto estaremos enviandote informacion acerca de nosotros :).</p></br>';

	$popup 				= ar_modal('Gracias por suscribirte', 'Ahora podrás disfrutar d enuestro mejor contenido sobre los temas más actulizados en.');
	$message_cli  = ar_mail_contact( $title_cli, $body );

	//ENVIO ADMIN
  $subject_admin 	= 'Nuevo suscriptor ';

	$title_admin 		= 'Hola Admin';

	$message_admin = ar_mail_contact( $title_admin , $body );

	add_filter( 'wp_mail_content_type', 'ar_wp_mail_content_type' );
	add_filter( 'wp_mail_from_name', 'ar_wp_mail_from_name' );
	@wp_mail( $email , $subject_cli , $message_cli);

	add_filter( 'wp_mail_content_type', 'ar_wp_mail_content_type' );
	add_filter( 'wp_mail_from_name', 'ar_wp_mail_from_name' );
	$send_admin = wp_mail(  ar_mails_admin() , $subject_admin , $message_admin );

	if(!is_wp_error($send_admin)):

		$bdStatu = $wpdb->insert(
			$wpdb->prefix.'newsletter',
			array(
        'newsletter_firstname'  => $firstname,
        'newsletter_lastname'   =>  $lastname,
        'newsletter_email'      => $email,
        'newsletter_post'       => $post,
        'newsletter_register'   => date('Y-m-d H:i:s', current_time('timestamp'))
			)
		);

		wp_send_json( array( 'status' => true , 'fragments' => $popup ));
	else:
		wp_send_json( array( 'status' => false ));
	endif;

	wp_die();

}
