<?php

// Register Custom Taxonomy
function proyecto_categoria_tax() {

    $labels = array(
        'name'                       => _x( 'Categorías de Proyecto', 'Taxonomy General Name', 'arteco' ),
        'singular_name'              => _x( 'Categoría de Proyecto', 'Taxonomy Singular Name', 'arteco' ),
        'menu_name'                  => __( 'Categorías de Proyecto', 'arteco' ),
        'all_items'                  => __( 'Todas las categorías', 'arteco' ),
        'parent_item'                => __( 'Parent Item', 'arteco' ),
        'parent_item_colon'          => __( 'Parent Item:', 'arteco' ),
        'new_item_name'              => __( 'Nueva categoría', 'arteco' ),
        'add_new_item'               => __( 'Agregar nueva categoría', 'arteco' ),
        'edit_item'                  => __( 'Editar categoría', 'arteco' ),
        'update_item'                => __( 'Actualizar categoría', 'arteco' ),
        'view_item'                  => __( 'Ver categoría', 'arteco' ),
        'separate_items_with_commas' => __( 'Separate items with commas', 'arteco' ),
        'add_or_remove_items'        => __( 'Add or remove items', 'arteco' ),
        'choose_from_most_used'      => __( 'Choose from the most used', 'arteco' ),
        'popular_items'              => __( 'Popular Items', 'arteco' ),
        'search_items'               => __( 'Search Items', 'arteco' ),
        'not_found'                  => __( 'Not Found', 'arteco' ),
        'no_terms'                   => __( 'No items', 'arteco' ),
        'items_list'                 => __( 'Items list', 'arteco' ),
        'items_list_navigation'      => __( 'Items list navigation', 'arteco' ),
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => true,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
    );
    register_taxonomy( 'proyecto_categoria', array( 'proyecto_arteco' ), $args );

}
add_action( 'init', 'proyecto_categoria_tax', 0 );