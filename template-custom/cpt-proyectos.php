<?php

if ( ! function_exists('proyecto_arteco') ) {

// Register Custom Post Type
function proyecto_arteco() {

$labels = array(
'name'                  => _x( 'Proyectos Arteco', 'Post Type General Name', 'arteco' ),
'singular_name'         => _x( 'Proyecto Arteco', 'Post Type Singular Name', 'arteco' ),
'menu_name'             => __( 'Proyectos Arteco', 'arteco' ),
'name_admin_bar'        => __( 'Proyecto Arteco', 'arteco' ),
'archives'              => __( 'Proyecto Archive', 'arteco' ),
'attributes'            => __( 'Proyecto Atributos', 'arteco' ),
'parent_item_colon'     => __( 'Parent Item:', 'arteco' ),
'all_items'             => __( 'Todos los Proyecto', 'arteco' ),
'add_new_item'          => __( 'Agregar nuevo proyecto', 'arteco' ),
'add_new'               => __( 'Agregar Proyecto', 'arteco' ),
'new_item'              => __( 'Nuevo proyecto', 'arteco' ),
'edit_item'             => __( 'Editar Proyecto', 'arteco' ),
'update_item'           => __( 'Actualizar Proyecto', 'arteco' ),
'view_item'             => __( 'Ver Proyecto', 'arteco' ),
'view_items'            => __( 'Ver Proyectos', 'arteco' ),
'search_items'          => __( 'Buscar Proyecto', 'arteco' ),
'not_found'             => __( 'Not found', 'arteco' ),
'not_found_in_trash'    => __( 'Not found in Trash', 'arteco' ),
'featured_image'        => __( 'Imagen destacada', 'arteco' ),
'set_featured_image'    => __( 'Establecer imagen destacada', 'arteco' ),
'remove_featured_image' => __( 'Eliminar imagen destacada', 'arteco' ),
'use_featured_image'    => __( 'Usar imagen destacada', 'arteco' ),
'insert_into_item'      => __( 'Insert into item', 'arteco' ),
'uploaded_to_this_item' => __( 'Uploaded to this item', 'arteco' ),
'items_list'            => __( 'Items list', 'arteco' ),
'items_list_navigation' => __( 'Items list navigation', 'arteco' ),
'filter_items_list'     => __( 'Filter items list', 'arteco' ),
);
$rewrite = array(
'slug'                  => 'proyectos',
'with_front'            => true,
'pages'                 => true,
'feeds'                 => true,

);


$args = array(
'label'                 => __( 'Proyecto Arteco', 'arteco' ),
'description'           => __( 'Proyecto para arteco', 'arteco' ),
'labels'                => $labels,
'supports'              => array( 'title', 'thumbnail', 'revisions','custom-fields' ),
'hierarchical'          => false,
'public'                => true,
'show_ui'               => true,
'show_in_menu'          => true,
'menu_position'         => 5,
'menu_icon'             => 'dashicons-admin-multisite',
'show_in_admin_bar'     => true,
'show_in_nav_menus'     => true,
'can_export'            => true,
'has_archive'           => false,
'exclude_from_search'   => false,
'publicly_queryable'    => true,
'rewrite'               => $rewrite,
'capability_type'       => 'page',
);
register_post_type( 'proyecto_arteco', $args );

}
add_action( 'init', 'proyecto_arteco', 0 );

}
