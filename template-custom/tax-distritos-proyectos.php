<?php

// Register Custom Taxonomy
function proyecto_distrito_tax() {

    $labels = array(
        'name'                       => _x( 'Distritos de Proyecto', 'Taxonomy General Name', 'arteco' ),
        'singular_name'              => _x( 'Distrito de Proyecto', 'Taxonomy Singular Name', 'arteco' ),
        'menu_name'                  => __( 'Distritos de Proyecto', 'arteco' ),
        'all_items'                  => __( 'Todos los Distritos', 'arteco' ),
        'parent_item'                => __( 'Parent Item', 'arteco' ),
        'parent_item_colon'          => __( 'Parent Item:', 'arteco' ),
        'new_item_name'              => __( 'Nueva distrito', 'arteco' ),
        'add_new_item'               => __( 'Agregar nuevo distrito', 'arteco' ),
        'edit_item'                  => __( 'Editar distrito', 'arteco' ),
        'update_item'                => __( 'Actualizar distrito', 'arteco' ),
        'view_item'                  => __( 'Ver distrito', 'arteco' ),
        'separate_items_with_commas' => __( 'Separate items with commas', 'arteco' ),
        'add_or_remove_items'        => __( 'Add or remove items', 'arteco' ),
        'choose_from_most_used'      => __( 'Choose from the most used', 'arteco' ),
        'popular_items'              => __( 'Popular Items', 'arteco' ),
        'search_items'               => __( 'Search Items', 'arteco' ),
        'not_found'                  => __( 'Not Found', 'arteco' ),
        'no_terms'                   => __( 'No items', 'arteco' ),
        'items_list'                 => __( 'Items list', 'arteco' ),
        'items_list_navigation'      => __( 'Items list navigation', 'arteco' ),
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => false,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
    );
    register_taxonomy( 'proyecto_distrito', array( 'proyecto_arteco' ), $args );

}
add_action( 'init', 'proyecto_distrito_tax', 0 );
