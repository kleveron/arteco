<?php

if ( ! function_exists('slider_arteco') ) {

// Register Custom Post Type
    function slider_arteco() {

        $labels = array(
            'name'                  => _x( 'Sliders Arteco', 'Post Type General Name', 'arteco' ),
            'singular_name'         => _x( 'Slider Arteco', 'Post Type Singular Name', 'arteco' ),
            'menu_name'             => __( 'Sliders Arteco', 'arteco' ),
            'name_admin_bar'        => __( 'Slider Arteco', 'arteco' ),
            'archives'              => __( 'Slider Archive', 'arteco' ),
            'attributes'            => __( 'Slider Atributos', 'arteco' ),
            'parent_item_colon'     => __( 'Parent Item:', 'arteco' ),
            'all_items'             => __( 'Todos los Slider', 'arteco' ),
            'add_new_item'          => __( 'Agregar nuevo slider', 'arteco' ),
            'add_new'               => __( 'Agregar Slider', 'arteco' ),
            'new_item'              => __( 'Nuevo slider', 'arteco' ),
            'edit_item'             => __( 'Editar Slider', 'arteco' ),
            'update_item'           => __( 'Actualizar Slider', 'arteco' ),
            'view_item'             => __( 'Ver Slider', 'arteco' ),
            'view_items'            => __( 'Ver Sliders', 'arteco' ),
            'search_items'          => __( 'Buscar Slider', 'arteco' ),
            'not_found'             => __( 'Not found', 'arteco' ),
            'not_found_in_trash'    => __( 'Not found in Trash', 'arteco' ),
            'featured_image'        => __( 'Imagen destacada', 'arteco' ),
            'set_featured_image'    => __( 'Establecer imagen destacada', 'arteco' ),
            'remove_featured_image' => __( 'Eliminar imagen destacada', 'arteco' ),
            'use_featured_image'    => __( 'Usar imagen destacada', 'arteco' ),
            'insert_into_item'      => __( 'Insert into item', 'arteco' ),
            'uploaded_to_this_item' => __( 'Uploaded to this item', 'arteco' ),
            'items_list'            => __( 'Items list', 'arteco' ),
            'items_list_navigation' => __( 'Items list navigation', 'arteco' ),
            'filter_items_list'     => __( 'Filter items list', 'arteco' ),
        );
        $args = array(
            'label'                 => __( 'Slider Arteco', 'arteco' ),
            'description'           => __( 'Slider para arteco', 'arteco' ),
            'labels'                => $labels,
            'supports'              => array( 'title', 'custom-fields' ),
            'hierarchical'          => false,
            'public'                => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'menu_position'         => 5,
            'menu_icon'             => 'dashicons-images-alt',
            'show_in_admin_bar'     => true,
            'show_in_nav_menus'     => true,
            'can_export'            => true,
            'has_archive'           => true,
            'exclude_from_search'   => false,
            'publicly_queryable'    => true,
            'capability_type'       => 'page',
        );
        register_post_type( 'slider_arteco', $args );

    }
    add_action( 'init', 'slider_arteco', 0 );

}