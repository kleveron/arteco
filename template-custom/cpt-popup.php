<?php
// Register Custom Post Type
function popup_arteco() {

    $labels = array(
        'name'                  => _x( 'Popups Arteco', 'Post Type General Name', 'arteco' ),
        'singular_name'         => _x( 'Popup Arteco', 'Post Type Singular Name', 'arteco' ),
        'menu_name'             => __( 'Popups Arteco', 'arteco' ),
        'name_admin_bar'        => __( 'Popups Arteco', 'arteco' ),
        'archives'              => __( 'Popups Archive', 'arteco' ),
        'attributes'            => __( 'Item Attributes', 'arteco' ),
        'parent_item_colon'     => __( 'Parent Item:', 'arteco' ),
        'all_items'             => __( 'Todo los popups', 'arteco' ),
        'add_new_item'          => __( 'Agregar nuevo popup', 'arteco' ),
        'add_new'               => __( 'Agregar Popup', 'arteco' ),
        'new_item'              => __( 'Nuevo Popup', 'arteco' ),
        'edit_item'             => __( 'Editar Popup', 'arteco' ),
        'update_item'           => __( 'Actualizar Popup', 'arteco' ),
        'view_item'             => __( 'Ver Popup', 'arteco' ),
        'view_items'            => __( 'Ver Popups', 'arteco' ),
        'search_items'          => __( 'Search Item', 'arteco' ),
        'not_found'             => __( 'Not found', 'arteco' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'arteco' ),
        'featured_image'        => __( 'Featured Image', 'arteco' ),
        'set_featured_image'    => __( 'Set featured image', 'arteco' ),
        'remove_featured_image' => __( 'Remove featured image', 'arteco' ),
        'use_featured_image'    => __( 'Use as featured image', 'arteco' ),
        'insert_into_item'      => __( 'Insert into item', 'arteco' ),
        'uploaded_to_this_item' => __( 'Uploaded to this item', 'arteco' ),
        'items_list'            => __( 'Items list', 'arteco' ),
        'items_list_navigation' => __( 'Items list navigation', 'arteco' ),
        'filter_items_list'     => __( 'Filter items list', 'arteco' ),
    );
    $args = array(
        'label'                 => __( 'popup', 'arteco' ),
        'description'           => __( 'Popups para arteco', 'arteco' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'custom-fields' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 10,
        'menu_icon'             => 'dashicons-align-full-width',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => false,
        'exclude_from_search'   => true,
        'publicly_queryable'    => true,
        'capability_type'       => 'page',
    );
    register_post_type( 'popup_arteco', $args );

}
add_action( 'init', 'popup_arteco', 0 );