<?php /* Template Name: Template Home */ ?>

<?php get_header();?>

<?php get_template_part( 'template-parts/home', 'slider' ); ?>
<?php get_template_part( 'template-parts/proyectos', 'widget-ejecucion-carrusel' ); ?>
<?php get_template_part( 'template-parts/proyectos', 'widget-entregados' ); ?>
<?php get_template_part( 'template-parts/testimonios', 'widget' ); ?>
<?php get_template_part( 'template-parts/noticias', 'widget' ); ?>


<?php get_footer();
