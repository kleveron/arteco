<?php
  require __DIR__.'/vendor/autoload.php';
  use Spipu\Html2Pdf\Html2Pdf;
  // init HTML2PDF
  $html2pdf = new HTML2PDF('P', array(220,470), 'en', true, 'UTF-8', array(0, 0, 0, 0));

  // display the full page
  $html2pdf->pdf->SetDisplayMode('fullpage');

  $content = '';
    $content .= '<style type="text/css">';
      $content .= 'table.page_table {width: 100%; border: none;border-collapse: collapse;}';
      $content .= '.page_border-bottom {border-bottom : 2px solid #000000!important;}';
      $content .= '.page_border-top {border-top : 2px solid #000000!important;}';
      $content .= '.page_border-right {border-right : 2px solid #000000!important;}';
      $content .= '.page_border-left {border-left : 2px solid #000000!important;}';
      $content .= 'div.note {padding: 2mm;  width: 100%; font-size: 8pt ;line-height:9.6pt;}';
    $content .= '</style>';
    $content .= '<page backtop="14mm" backbottom="14mm" backleft="10mm" backright="10mm" style="font-size: 12pt">';
        $content .= '<page_header></page_header>';
        $content .= '<page_footer> <img src="./img/footer-proforma-arteco-lucid.png" alt="Logo Acordeco" style="width: 100%"> </page_footer>';

        $content .= '<table class="page_table">';
            $content .= '<tr>';
                $content .= '<td style="width: 50%; text-align: left;">';

                  $content .= '<table class="page_table page_border" >';
                    $content .= '<tr>';
                      $content .= '<td class="page_border-left page_border-bottom page_border-top" style="width: 28mm; text-align: left;font-size:12pt;padding:2.5mm 3mm;"><strong>Proforma:</strong></td>';
                      $content .= '<td class="page_border-right page_border-bottom page_border-top" style="width: 35mm; text-align: right;font-size:12pt;color:#706F6F;padding:2.5mm 3mm;"><span>10AIP162411200001</span></td>';
                    $content .= '</tr>';
                    $content .= '<tr>';
                      $content .= '<td class="page_border-left page_border-bottom page_border-top" style="width: 28mm; text-align: left;font-size:12pt;padding:2.5mm 3mm;"><strong>Fecha y hora:</strong></td>';
                      $content .= '<td class="page_border-right page_border-bottom page_border-top" style="width: 35mm; text-align: right;font-size:12pt;color:#706F6F;padding:2.5mm 3mm;"><span>26/11/2020</span></td>';
                    $content .= '</tr>';
                  $content .= '</table>';

                $content .= '</td>';
                $content .= '<td style="width: 50%; text-align: right">';
                  $content .= '<img src="./img/logo-acordeco-pdf.jpg" alt="Logo Acordeco" style="width:53.5673 mm;height:15.527mm">';
                $content .= '</td>';
            $content .= '</tr>';
        $content .= '</table>';
        $content .= '<br><br>';

        $content .= '<h2>Cliente:</h2>';
        $content .= '<table class="page_table">';
            $content .= '<tr>';
                $content .= '<td style="width: 50%; text-align: left;">';

                  $content .= '<table class="page_table page_border" style="width:100%">';
                    $content .= '<tr>';
                      $content .= '<td style="width: 20mm; text-align: left;font-size:12pt;padding:2.5mm 3mm;padding-left:0;"><strong>Estimado(a)</strong></td>';
                      $content .= '<td style="width: 30mm; text-align: left;font-size:12pt;color:#706F6F;padding:2.5mm 3mm;"><span>Mateo Costa</span></td>';
                    $content .= '</tr>';
                    $content .= '<tr>';
                      $content .= '<td style="width: 20mm; text-align: left;font-size:12pt;padding:2.5mm 3mm;padding-left:0;"><strong>DNI:</strong></td>';
                      $content .= '<td style="width: 30mm; text-align: left;font-size:12pt;color:#706F6F;padding:2.5mm 3mm;"><span>60785698</span></td>';
                    $content .= '</tr>';
                  $content .= '</table>';

                $content .= '</td>';
                $content .= '<td style="width: 50%; text-align: left">';
                  $content .= '<table class="page_table page_border" style="width:100%">';
                    $content .= '<tr>';
                      $content .= '<td style="width: 20mm; text-align: left;font-size:12pt;padding:2.5mm 3mm;padding-left:0;"><strong>Teléfono</strong></td>';
                      $content .= '<td style="width: 30mm; text-align: left;font-size:12pt;color:#706F6F;padding:2.5mm 3mm;"><span>987 654 321</span></td>';
                    $content .= '</tr>';
                    $content .= '<tr>';
                      $content .= '<td style="width: 20mm; text-align: left;font-size:12pt;padding:2.5mm 3mm;padding-left:0;"><strong>E-mail</strong></td>';
                      $content .= '<td style="width: 35mm; text-align: left;font-size:12pt;color:#706F6F;padding:2.5mm 3mm;"><span>mateo@gmail.com</span></td>';
                    $content .= '</tr>';
                  $content .= '</table>';

                $content .= '</td>';
            $content .= '</tr>';
        $content .= '</table>';

        $content .= '<br><br>';

        $content .= '<h2>Proyecto:</h2>';
        $content .= '<table class="page_table">';
          $content .= '<tr>';
              $content .= '<td style="width: 80%; text-align: left;">';

                $content .= '<table class="page_table page_border" style="width:120mm">';
                  $content .= '<tr>';
                    $content .= '<td style="width: 25mm; text-align: left;font-size:12pt;padding:2.5mm 3mm;padding-left:0;"><strong>Nombre</strong></td>';
                    $content .= '<td style="width: 80mm; text-align: left;font-size:12pt;color:#706F6F;padding:2.5mm 3mm;"><span>Lucid Depa Smart</span></td>';
                  $content .= '</tr>';
                  $content .= '<tr>';
                    $content .= '<td style="width: 25mm; text-align: left;font-size:12pt;padding:2.5mm 3mm;padding-left:0;"><strong>Dirección</strong></td>';
                    $content .= '<td style="width: 80mm; text-align: left;font-size:12pt;color:#706F6F;padding:2.5mm 3mm;"><span>Calle Amapolas N° 120, Lince</span></td>';
                  $content .= '</tr>';
                  $content .= '<tr>';
                    $content .= '<td style="width: 25mm; text-align: left;font-size:12pt;padding:2.5mm 3mm;padding-left:0;"><strong>Etapa</strong></td>';
                    $content .= '<td style="width: 80mm; text-align: left;font-size:12pt;color:#706F6F;padding:2.5mm 3mm;"><span>Lanzamiento</span></td>';
                  $content .= '</tr>';
                $content .= '</table>';

              $content .= '</td>';
          $content .= '</tr>';
        $content .= '</table>';

        $content .= '<br><br>';

        $content .= '<h2>Ejecutivo:</h2>';
        $content .= '<table class="page_table">';
          $content .= '<tr>';
              $content .= '<td style="width: 80%; text-align: left;">';

                $content .= '<table class="page_table page_border" style="width:120mm">';
                  $content .= '<tr>';
                    $content .= '<td style="width: 25mm; text-align: left;font-size:12pt;padding:2.5mm 3mm;padding-left:0;"><strong>Nombre</strong></td>';
                    $content .= '<td style="width: 80mm; text-align: left;font-size:12pt;color:#706F6F;padding:2.5mm 3mm;"><span>Susana Delgado</span></td>';
                  $content .= '</tr>';
                  $content .= '<tr>';
                    $content .= '<td style="width: 25mm; text-align: left;font-size:12pt;padding:2.5mm 3mm;padding-left:0;"><strong>Celular</strong></td>';
                    $content .= '<td style="width: 80mm; text-align: left;font-size:12pt;color:#706F6F;padding:2.5mm 3mm;"><span>989 031 286</span></td>';
                  $content .= '</tr>';
                  $content .= '<tr>';
                    $content .= '<td style="width: 25mm; text-align: left;font-size:12pt;padding:2.5mm 3mm;padding-left:0;"><strong>Correo</strong></td>';
                    $content .= '<td style="width: 80mm; text-align: left;font-size:12pt;color:#706F6F;padding:2.5mm 3mm;"><span>susana.delgado@arteco.pe</span></td>';
                  $content .= '</tr>';
                $content .= '</table>';

              $content .= '</td>';
          $content .= '</tr>';
        $content .= '</table>';

        $content .= '<br><br>';
        $content .= '<h2>Datos de la propiedad:</h2>';
        $content .= '<table class="page_table">';
          $content .= '<tr>';
              $content .= '<td style="width: 50%; text-align: left;">';

                $content .= '<table class="page_table page_border" style="width:50%">';
                  $content .= '<tr>';
                    $content .= '<td style="width: 30mm; text-align: left;font-size:12pt;padding:2.5mm 3mm;padding-left:0;"><strong>N° Dpto. Torre</strong></td>';
                    $content .= '<td style="width: 30mm; text-align: left;font-size:12pt;color:#706F6F;padding:2.5mm 3mm;"><span>204</span></td>';
                  $content .= '</tr>';
                  $content .= '<tr>';
                    $content .= '<td style="width: 30mm; text-align: left;font-size:12pt;padding:2.5mm 3mm;padding-left:0;"><strong>Vista:</strong></td>';
                    $content .= '<td style="width: 30mm; text-align: left;font-size:12pt;color:#706F6F;padding:2.5mm 3mm;"><span>Interior</span></td>';
                  $content .= '</tr>';
                  $content .= '<tr>';
                    $content .= '<td style="width: 30mm; text-align: left;font-size:12pt;padding:2.5mm 3mm;padding-left:0;"><strong>Precio:</strong></td>';
                    $content .= '<td style="width: 30mm; text-align: left;font-size:12pt;color:#706F6F;padding:2.5mm 3mm;"><span>S/ 291, 900</span></td>';
                  $content .= '</tr>';
                $content .= '</table>';

              $content .= '</td>';
              $content .= '<td style="width: 50%; text-align: left;">';

              $content .= '<table class="page_table page_border" style="width:50%">';
                $content .= '<tr>';
                  $content .= '<td style="width: 30mm; text-align: left;font-size:12pt;padding:2.5mm 3mm;padding-left:0;"><strong>Piso</strong></td>';
                  $content .= '<td style="width: 80mm; text-align: left;font-size:12pt;color:#706F6F;padding:2.5mm 3mm;"><span>2</span></td>';
                $content .= '</tr>';
                $content .= '<tr>';
                  $content .= '<td style="width: 30mm; text-align: left;font-size:12pt;padding:2.5mm 3mm;padding-left:0;"><strong>Metraje</strong></td>';
                  $content .= '<td style="width: 80mm; text-align: left;font-size:12pt;color:#706F6F;padding:2.5mm 3mm;"><span>41.84 mtrs2</span></td>';
                $content .= '</tr>';
              $content .= '</table>';

            $content .= '</td>';
          $content .= '</tr>';
        $content .= '</table>';

        $content .= '<br><br>';

        $content .= '<h2>Plano del departamneto:</h2>';
        //$content .= '<br><br><br><br><br>';
        $content .= '<div style="width:220mm;height:2px;"></div>';
        $content .= '<div style="text-align: center; width: 100%;">';
          $content .= '<br>';
            $content .= '<img src="./img/plano-muestra.png" alt="plano" style="width: 150mm;">';
          $content .= '<br>';
        $content .= '</div>';
        $content .= '<div style="width:220mm;height:2px;"></div>';
        $content .= '<br><br>';
        $content .= '<div class="note">';
            $content .= 'Horario de atención: De lunes a domingo de 10 am. a 7 pm. (horario corrido)<br><br>
            La presente cotización tiene una vigencia de 72 horas desde la fecha de emisión del documento. No constituye reserva de
            compra ni una fijación en el precio.<br><br>
            A través de la presente nos autoriza el uso de sus datos personales para tratamientos que supongan desarrollo del objetoo social
            de ARTECO y/o de otras empresas del Grupo. Garantizamos la confidencialidad y seguridad de los datos proporcionados por
            cada usuario, cumpliendo con lo dispuesto en la Ley N°29733, ley de Protección de Datos Personales y su reglamento.';
        $content .= '</div>';
        $content .= '<br><br><br><br><br>';
    $content .= '</page>';

  // convert
  $html2pdf->writeHTML($content);

  // send the PDF
  $html2pdf->output();

  //$html2pdf = new Html2Pdf();
  //$html2pdf->writeHTML('<h1>HelloWorld</h1>This is my first test');
  //$html2pdf->output();
?>
