<?php
  require __DIR__ . '/vendor/autoload.php';

  function ar_wp_insert_google( $spreadsheetId, $range , $data ){

    $client = new \Google_Client();

    $client->setApplicationName('Google Sheets and PHP');

    $client->setScopes([\Google_Service_Sheets::SPREADSHEETS]);

    $client->setAccessType('offline');

    $client->setAuthConfig(__DIR__ . '/credentials.json');

    $service = new Google_Service_Sheets($client);

    $body = new Google_Service_Sheets_ValueRange([
      'values' => array($data)
    ]);

    $params = [
      //'valueInputOption' => 'RAW'
      'valueInputOption' => 'USER_ENTERED'
    ];

    $result = $service->spreadsheets_values->append($spreadsheetId, $range, $body, $params);
    return $result;
  }
