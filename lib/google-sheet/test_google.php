<?php
  require __DIR__ . '/vendor/autoload.php';

  $client = new \Google_Client();

  $client->setApplicationName('Google Sheets and PHP');

  $client->setScopes([\Google_Service_Sheets::SPREADSHEETS]);

  $client->setAccessType('offline');

  $client->setAuthConfig(__DIR__ . '/credentials.json');

  $service = new Google_Service_Sheets($client);

  $spreadsheetId = "1PrfNP4GVbtreJ7ub0hf31VOIJC6wgEjLcpfjECzrfjo"; //It is present in your URL

  $range =  "leads Quintanas";

  //APPEND DATA 
  $values = [
    [
      '11 de January de 2021'	,
      '11:50 AM'	,
      '10AIP161101202100001'	,
      'Juan	',
      'Chancafe'	,
      '47017266'	,
      '123456789',
      'chankafito@gmail.com'	,
      'Lucid Depa Smart',
      'Lanzamiento',
      'Susana Delgado'	,
      '204',
      '',
      2	,
      1	,
      'Interior',
      '291900' ,
      '41.85'
    ],
  ];
  $body = new Google_Service_Sheets_ValueRange([
    'values' => $values
  ]);

  $params = [
    'valueInputOption' => 'RAW'
  ];

  $insert = [
    'insertDataOption' => 'INSERT_ROWS'
  ];
  
  //$result = $service->spreadsheets_values->append($spreadsheetId, $range, $body, $params);
  //printf("%d cells appended.", $result->getUpdates()->getUpdatedCells());
  