<?php
global $post;
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Arteco
 */

  if( have_rows('footer', 'option') ):
      while( have_rows('footer', 'option') ): the_row();
      /*Icono 1*/
            if( have_rows('icono_1', 'option') ):
                while( have_rows('icono_1', 'option') ): the_row();
                    $icono1 = get_sub_field('imagen');
                    $texto1 = get_sub_field('texto');
                    $link1 = get_sub_field('link');
               endwhile;
            endif;
      /*Icono 2*/
      if( have_rows('icono_2', 'option') ):
          while( have_rows('icono_2', 'option') ): the_row();
              $icono2 = get_sub_field('imagen');
              $texto2 = get_sub_field('texto');
              $link2 = get_sub_field('link');
         endwhile;
      endif;

      /*Icono 3*/
      if( have_rows('icono_3', 'option') ):
          while( have_rows('icono_3', 'option') ): the_row();
              $icono3 = get_sub_field('imagen');
              $texto3 = get_sub_field('texto');
              $link3 = get_sub_field('link');
         endwhile;
      endif;


      endwhile;
    endif;

?>

	<!--<footer id="colophon" class="site-footer">
        <?php get_template_part( 'template-parts/footer', 'top' ); ?>
        <?php get_template_part( 'template-parts/', 'footer-content' ); ?>
        <?php get_template_part( 'template-parts/', 'footer-bottom' ); ?>
	</footer> -->

<!-- Footer -->

<footer class="footer">
    <div class="container">
        <div class="footer__header">
            <div class="row">

                <div class="col-lg-3">
                    <img src="<?php echo get_template_directory_uri(); ?>/src/img/logo-white.png" alt="">
                </div>

                <div class="col-lg-3">

                    <div class="footer__item">
                        <img src="<?php if ($icono1) { echo $icono1; } ?>" alt="">
                        <span><?php if ($texto1) { echo $texto1;} ?></span>
                    </div>

                </div>
                <div class="col-lg-3">
                    <div class="footer__item">
                          <img src="<?php if ($icono2) { echo $icono2; } ?>" alt="">
                        <span><?php if ($texto2) { echo $texto2;} ?></span>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="footer__item">
                        <img src="<?php if ($icono3) { echo $icono3; } ?>" alt="">
                      <span><?php if ($texto3) { echo $texto3;} ?></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="footer__inner">
            <div class="row">
                <div class="col-lg-3">
                    <div class="footer__title">
                        Proyectos
                    </div>
                    <div class="footer__list">
                        <?php
                        wp_nav_menu( array(
                           'theme_location' => 'menu-footer1',
                          'menu_class' => 'menu-menu-1-container',
                       ) );
                         ?>


                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="footer__title">
                        Secciones
                    </div>
                    <div class="footer__list">
                      <?php
                      wp_nav_menu( array(
                        'theme_location' => 'menu-footer2',
                        'menu_class' => 'menu-menu-2-container',
                     ) );
                       ?>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="footer__title">
                        Otros
                    </div>
                    <div class="footer__list">
                      <?php
                      wp_nav_menu( array(
                        'theme_location' => 'menu-footer3',
                        'menu_class' => 'menu-menu-3-container',
                     ) );
                       ?>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="footer__title">
                      <?php _e('Métodos de pago','arteco'); ?>
                    </div>
                    <div class="footer__list">
                        <ul>
                            <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/src/img/ico-mastercard.png" alt=""></a></li>
                            <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/src/img/ico-visa.png" alt=""></a></li>
                        </ul>
                    </div>
                    <div class="footer__title pt-5">
                      <?php _e('Síguenos en','arteco'); ?>
                    </div>
                    <div class="footer__list footer__list-socials">
                        <ul>
                            <li>
                              <?php if( have_rows('redes_sociales', 'option') ):
                                    while( have_rows('redes_sociales', 'option') ): the_row(); ?>
                                <a target="_blank" href="<?php echo get_sub_field('link'); ?>"><img src="<?php echo get_sub_field('red'); ?>" alt=""></a>
                              <?php endwhile; endif; ?>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="footer__bottom">
            Copyright 2021. Todos los derechos reservados Arteco  | Central: Av. Manuel Olguín 501 - Of. 506, Surco - Lima  | <a href="https://malcolm.la/" target="_blank">Desarrollo por Malcolm</a> .
        </div>
    </div>
</footer>

</div><!-- #page -->



<div id='toTop'><img src="<?php echo get_template_directory_uri(); ?>/src/img/arrow-top.png" alt=""></div>

<?php $frontpage = get_option( 'page_on_front' ); ?>

<?php if( get_field('activar_popup', $frontpage ) && get_field('selecionar_popup', $frontpage ) && is_front_page() ): ?>
  <?php
    $popup =  get_field('selecionar_popup', $frontpage )->ID;
    $img = get_field('imagen', $popup );
    $url = get_field('link', $popup );
  ?>
  <div class="modal fade modal-transparent" id="modalPromo" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-body">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <img src="<?php echo get_template_directory_uri(); ?>/src/img/modal-close.png" alt="">
          </button>
          <?php if($url): ?>
            <a href="<?= $url ?>">
          <?php endif; ?>
            <img class="img-fluid" src="<?= $img; ?>" alt="">
          <?php if($url): ?>
            </a>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
<?php else: ?>
  <?php if( get_field('activar_popup' ) && get_field('selecionar_popup' )): ?>
    <?php
      $popup =  get_field('selecionar_popup' )->ID;
      $img = get_field('imagen' , $popup);
      $url = get_field('link', $popup);
    ?>
    <div class="modal fade modal-transparent" id="modalPromo" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <img src="<?php echo get_template_directory_uri(); ?>/src/img/modal-close.png" alt="">
            </button>
            <?php if($url): ?>
              <a href="<?= $url ?>">
            <?php endif; ?>
              <img class="img-fluid" src="<?= $img; ?>" alt="">
            <?php if($url): ?>
              </a>
            <?php endif; ?>
          </div>
        </div>
      </div>
    </div>
  <?php endif; ?>
<?php endif; ?>

<!-- Imagen Modal -->
<!--
<div class="modal fade modal-transparent" id="modalPromo" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <img src="<?php echo get_template_directory_uri(); ?>/src/img/modal-close.png" alt="">
                </button>
                <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/src/img/cotiza.png" alt="">
            </div>
        </div>
    </div>
</div>  -->

<!-- Coockies -->
 <?php if ( is_home()|| is_front_page() ){ ?>
<div class="coockies">
    <div class="container">
        <div class="text-lg-center">
        Este sitio utiliza cookies propias y de terceros. Si continúa navegando consideramos que acepta el uso de estas. <a href="https://www.arteco.pe/politica-de-cookies/" target="_blank" rel="noopener noreferrer">Política de cookies</a>
        </div>
        <div class="text-center pt-4">
            <button class="butn butn-1 mx-3 coockies-close">Acepto</button>
            <button class="butn butn-2 mx-3">No acepto</button>
        </div>
    </div>
</div>
<?php } ?>

<!-- Modal Video -->
<div class="modal fade modal-video" id="modalVideo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <!-- 16:9 aspect ratio -->
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item" src="" id="video"  allowscriptaccess="always" allow="autoplay"></iframe>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Zoom -->
<div class="modal fade modal-zoom" id="modalZoom" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <!-- 16:9 aspect ratio -->
                <div class="text-center">
                    <img class="img-fluid" src="" id="zoom" />
                </div>
            </div>
        </div>
    </div>
</div>


<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<!-- <script src="../src/js/owl.carousel.min.js"></script>
<script src="../src/js/jquery.mousewheel.min.js"></script> -->
<!-- <script src="https://unpkg.co/gsap@3/dist/gsap.min.js"></script> -->
<!-- <script src="https://unpkg.com/gsap@3/dist/ScrollTrigger.min.js"></script> -->
<script src="<?php echo get_template_directory_uri(); ?>/src/libraries/timelinejs/timeline.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA_x185qTPg9r1MTgbYVm_7x3JEvxwAZW4&libraries=&v=weekly" ></script>
<script src="<?php echo get_template_directory_uri(); ?>/src/js/effects.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/src/js/fakescroll.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/src/js/custom-select.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/src/js/app.js?v=<?= time(); ?>"></script>
<script src="https://unpkg.com/isotope-layout@3/dist/isotope.pkgd.js"></script>

<?php wp_footer(); ?>

</body>
</html>
