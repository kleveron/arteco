jQuery(document).ready(function(){
  jQuery('#contact').DataTable({
    language:{
      url: '//cdn.datatables.net/plug-ins/1.10.22/i18n/Spanish.json'
    },
    dom: 'Bfrtip',
    buttons: [
      'copy', 'csv', 'excel', 'pdf', 'print'
    ]
  });
  document.title = 'Contact ';
});
