'use strict';

(function($) {

  var ar_is_blocked = function( $node ) {
    return $node.is( '.processing' ) || $node.parents( '.processing' ).length;
  };

  var ar_block = function( $node ) {
    if ( ! ar_is_blocked( $node ) ) {
      $node.addClass( 'processing' ).block( {
        message: null,
        overlayCSS: {
          background: '#fff',
          opacity: 0.6
        }
      } );
    }
  };

  var ar_unblock = function( $node ) {
    $node.removeClass( 'processing' ).unblock();
  };

  function ar_wc_show_modal( $html ){
    $('body').append( $html );
    setTimeout(function(){
      $('#modalThank').modal('show');
    }, 500 );
    $('#modalThank').on('show.bs.modal', (evt) => {
      $(document.body).addClass('overlay-open');
    });
    $('#modalThank').on('hidden.bs.modal', (evt) => {
      $(document.body).removeClass('overlay-open');
    });
  }

  $(document).on('click','.ar-send-contact' , function( e ){
    e.preventDefault();
    e.stopPropagation();
    var $this   = $(this),
        $width  = $this.innerWidth(),
        $form   = $this.closest('form'),
        $text   = $this.html(),
        $data   = $form.serializeArray()  || 0 ;
    if( $form[0].checkValidity()){
      $this
        .css({ width : $width })
        .html('<span class="ar-spinner spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>')
        .addClass('disabled')
        .prop('disabled', true );
      ar_block( $form );
      $.post(ajax_script.ajax_url, $data, function( response ){
        console.log(response);
        $this
          .css({ width : 'auto' })
          .html($text)
          .removeClass('disabled')
          .prop('disabled', false );
        if(response.status == true ){
          $form[0].reset();
          ar_unblock( $form );
		      $form.removeClass('was-validated');
        }

        if(response.status == false ){
          ar_unblock( $form );
        }

        if( response.fragments ){
          ar_wc_show_modal(response.fragments);
        }

        if( response.hubspot ){
          hubspot( response.hubspot )
        }

        if( response.code ){
          $('body').append('<iframe width="1" height="1" border="0" style="display: none;" src="https://track.notonlymedia.com/pixel/script?txid='+response.code+'"></iframe>');
        }

      }, 'JSON');

    }else{
      $form.addClass('was-validated');
    }
    return false;
  });

  function hubspot( $data ){
    var _url = $data['url'];

    $.ajax({
        url: _url,
        type: "POST",
        contentType: "application/json",
        dataType: "json",
        headers: {
            "accept": "application/json",
            "Access-Control-Allow-Origin":"*"
        },
        data: JSON.stringify({
          fields : [
              {
                'name'  : 'firstname',
                'value' : $data['firstname']
              },
              {
                'name': 'lastname',
                'value': $data['lastname']
              },
              {
                'name': 'phone',
                'value': $data['phone']
              },
              {
                'name': 'dni',
                'value': $data['dni']
              },
              {
                'name': 'email',
                'value': $data['email']
              },
              {
                'name' : 'cant__de_dorm',
                'value' : $data['bedrooms']
              },
              {
                'name' : 'piso',
                'value' : $data['flat']
              }
           ]
        }),

        success: function(result) {
            console.log(result);
        },
        error: function(xhr,status,error) {
            console.log(xhr);
            console.log(status);
            console.log(error);
        }
    });

  }

  if($('.ar-select2-subject').length>0){
    $('.ar-select2-subject').select2({
      placeholder: 'Asunto',
    });
  }

  if($('.ar-select2-proyects').length>0){
    $('.ar-select2-proyects').each(function(){
      var $placeholder = $(this).attr('placeholder');
      $(this).select2({
        placeholder: $placeholder,
      });
    });

    function resetSelect( $select ){
      var $placeholder = $select.attr('placeholder');
      $select.select2('destroy').select2({
        placeholder: $placeholder,
      });
    }

    $(document).on('change', '.ar-select2-proyects', function( e ){
      e.preventDefault();
      e.stopPropagation();
      var $this =  $(this),
        $form        = $this.closest('form'),
        $serialize   = $form.serializeArray(),
        $bedrooms    = ar_form_get_data('bedrooms', $serialize),
        $flat        = ar_form_get_data('flat', $serialize),
        $view        = ar_form_get_data('view', $serialize),
        $departament = ar_form_get_data('departament', $serialize),
        $data = $form.data('json');
      var filter = new Object();
      if( $(e.currentTarget).hasClass('ar-select2-departament') ){
        if($departament) filter.depa = $departament;
      }else{
        if($bedrooms) filter.dormitorios = $bedrooms;
        if($flat) filter.pisos = $flat;
        if($view) filter.tipo_de_vista = $view;
      }

      let list = $data.filter(function (item) {
        for (var key in filter) {
          if (item[key] === undefined || item[key] != filter[key])
          return false;
        }
        return true;
      });

      var $_bedrooms = ar_list_data('dormitorios', list),
          $_flat = ar_list_data('pisos', list),
          $_view = ar_list_data('tipo_de_vista', list),
          $_departament = ar_list_data('depa', list);

      var $b = $('.ar-select2-bedrooms');
      var $f = $('.ar-select2-flat');
      var $v = $('.ar-select2-view');
      var $d = $('.ar-select2-departament');

      if($(e.currentTarget).hasClass('ar-select2-departament')){
        if(list.length == 1 ){
          $b.val(list[0]['dormitorios']);
          resetSelect( $b );
          $f.val(list[0]['pisos']);
          resetSelect( $f );
          $v.val(list[0]['tipo_de_vista']);
          resetSelect( $v );
        }
      }else{
        $d.val('');
        $d.find('option').prop('disabled', true);
        $.each($_departament, function(i, e){
          $d.find('option[value="'+e+'"]').prop('disabled', false);
        });
        resetSelect( $d );
      }


    })
  }

  /* $(document).on('change', '.ar-change-input', function( e ){
    e.preventDefault();
    e.stopPropagation();

    var $this =  $(this),
        $form        = $this.closest('form'),
        $serialize   = $form.serializeArray(),
        $bedrooms    = ar_form_get_data('bedrooms', $serialize),
        $flat        = ar_form_get_data('flat', $serialize),
        $view        = ar_form_get_data('view', $serialize),
        $departament = ar_form_get_data('departament', $serialize),
        $data = $form.data('json');

    var filter = new Object();
    if($bedrooms) filter.dormitorios = $bedrooms;
    if($flat) filter.pisos = $flat;
    if($view) filter.tipo_de_vista = $view;
    if($departament) filter.depa = $departament;
    let list = $data.filter(function (item) {
      for (var key in filter) {
        if (item[key] === undefined || item[key] != filter[key])
        return false;
      }
      return true;
    });
    var $_bedrooms = ar_list_data('dormitorios', list),
        $_flat = ar_list_data('pisos', list),
        $_view = ar_list_data('tipo_de_vista', list),
        $_departament = ar_list_data('depa', list);

    $form.find('.select-box__list label:not(.not-disabled)').addClass('disabled');

    $form.find('input[name="bedrooms"]').prop('disabled', true);
    $.each($_bedrooms, function(i, e){
      var $input = $form.find('input[name="bedrooms"][value="'+e+'"]'),
          $label = $form.find('label[for="'+$input.attr('id')+'"]');
      $input.prop('disabled', false);
      $label.removeClass('disabled');
    });

    $form.find('input[name="flat"]').prop('disabled', true);
    $.each($_flat, function(i, e){
      var $input = $form.find('input[name="flat"][value="'+e+'"]'),
          $label = $form.find('label[for="'+$input.attr('id')+'"]');
      $input.prop('disabled', false);
      $label.removeClass('disabled');
    });

    $form.find('input[name="view"]').prop('disabled', true);
    $.each($_view, function(i, e){
      var $input = $form.find('input[name="view"][value="'+e+'"]'),
          $label = $form.find('label[for="'+$input.attr('id')+'"]');
      $input.prop('disabled', false);
      $label.removeClass('disabled');
    });

    $form.find('input[name="departament"]').prop('disabled', true);
    $.each($_departament, function(i, e){
      var $input = $form.find('input[name="departament"][value="'+e+'"]'),
          $label = $form.find('label[for="'+$input.attr('id')+'"]');
      $input.prop('disabled', false);
      $label.removeClass('disabled');
    });
    $form.find('input.ar-input-init').prop('disabled', false);
    $label.removeClass('disabled');
  }); */

  function ar_form_get_data( $name , $data ){
    var $return ;
    for(i = 0; i < $data.length; i ++ ) {
      if ($data[i].name === $name) {
        $return = $data[i].value;
      }
    }
    return $return;
  }

  function ar_list_data( $data, $list ){
    var $array = [];
    $.each($list, function( i, e){
      $array.push(e[$data]);
    });
    return $array;
  }

  if($('.ar-select2-proyect').length>0){
    $('.ar-select2-proyect').select2({
      placeholder: 'Elegir proyecto',
    });

    $('.ar-select2-proyect').on('change', function() {
      var $this = $(this),
          $value = $this.val(),
          $select = $('.ar-select2-departament'),
          $placeholder = 'N° de departamento';
      var $data = {
        action : 'ar_ajax_departament',
        post_id : $value
      };
      $select.prop('disabled', true);
      $.post(ajax_script.ajax_url, $data , function( response ){
        enabledList( response );
      }, 'JSON');

    });

    function enabledList( $data ){
      var $_data  = $data;
      var $dep = $('.ar-select2-departament'),
          $tower = $('.ar-select2-tower'),
          $_dep = [] , $_tower = [];
      $tower.prop('disabled', true);
      $tower.html('');
      $dep.html('');
      $dep.prop('disabled', true);
      if($_data){
        $.each($_data, function(i, e){
          if(e['depa'] != ''){
            $_dep.push(e['depa']);
          }
          if(e['torre'] != ''){
            $_tower.push(e['torre']);
          }
        });
        //TORRE
        var $placeTorre = 'Torre';

        $tower.prop('required', true);
        if( $_tower.length >0 ){
          $tower.prop('disabled', false);
          $tower.append('<option value="" hidden> Torre </option>');
          $.each($_tower, function(i, e){
            $tower.append('<option value="'+e+'">'+e+'</option>');
          });
        }else{
          $tower.append('<option value="" hidden> Edifico sin torres </option>');
          $placeTorre = 'Edifico sin torres';
          $tower.prop('required', false);
        }
        $tower.select2('destroy').select2({
          placeholder: $placeTorre,
        });

        //DEPARTAMENTO
        var $placeDep = 'N° de departamento';

        if( $_dep.length >0 ){
          $dep.prop('disabled', false);
          $dep.append('<option value="" hidden> N° de departamento </option>');
          $.each($_dep, function(i, e){
            $dep.append('<option value="'+e+'">Dpto. '+e+'</option>');
          });
        }else{
          $dep.append('<option value="" hidden> No disponible </option>');
          $placeDep = 'No disponible';
        }

        $dep.select2('destroy').select2({
          placeholder: $placeDep,
        });

        $tower.on('change', function(){
          var $this = $(this), $val = $this.val();
          var $list = $_data.filter(function(item){ return item.torre === $val ;} );
          $dep.find('option').prop('disabled', true);
          $.each($list, function(i, e){
            console.log(e.depa);
            $dep.find('option[value="'+e.depa+'"]').prop('disabled', false);
          });
          $dep.select2('destroy').select2({
            placeholder: 'N° de departamento',
          });
        });

      }else{
        $tower.append('<option value="" hidden> Torre </option>');
        $dep.append('<option value="" hidden> N° de departamento </option>');
        $dep.select2('destroy').select2({
          placeholder: 'No disponible',
        });
        $tower.select2('destroy').select2({
          placeholder: 'No disponible',
        });
      }

    }
  }

  if($('.ar-select2-tower').length>0){
    $('.ar-select2-tower').select2({
      placeholder: 'Torre',
    });
  }

  if($('.ar-select2-departament').length>0){
    $('.ar-select2-departament').select2({
      placeholder: 'N° de departamento',
    });
  }

  $(document).on('click touch', '.ar_navbar-toggler', function( evt ){
    evt.preventDefault();
    if($(this).hasClass('active')){
      $(this).removeClass('active');
      $('.ar_navigation').removeClass('active');
      closeOverlay();
    }else{
      $(this).addClass('active');
      $('.ar_navigation').addClass('active');
      openOverlay();
    }
  })

  var openOverlay = () => {
    $(document.body).addClass('overlay-open');
    $(document.body).append('<div class="overlay"></div>');
    setTimeout(() => {
      $('.overlay').addClass('show');
    }, 100);
  };

  var closeOverlay = () => {
    $(document.body).removeClass('overlay-open');
    $('.overlay').removeClass('show');
    setTimeout(() => {
      $('.overlay').remove();
    }, 500);
  };

  //DATA PICKER
  if($('.ar-picker').length>0){
    var $date = $('.ar-picker');
    var $container = $('.ar-picker-container');
    var $trigger = $('.ar-picker-trigger');
    var $wrap = $('.ar-date-piker');
    var $dt = new Date();
    $dt.setMonth( $dt.getMonth() + 2 );
    var options = {
      autoHide : true,
      inline: true,
      container : $container,
      language: 'es-ES',
      startDate : new Date(),
      endDate : $dt,
      format: 'dd/mm/yyyy',
      template: '<div class="datepicker-container">' +
                  '<div class="datepicker-panel" data-view="years picker">' +
                    '<ul class="picker-days">' +
                      '<li class="prev" data-view="years prev"></li>' +
                      '<li data-view="years current"></li>' +
                      '<li class="next" data-view="years next"></li>' +
                    '</ul>' +
                    '<ul data-view="years"></ul>' +
                  '</div>' +
                  '<div class="datepicker-panel" data-view="months picker">' +
                    '<ul class="picker-days">' +
                      '<li class="prev" data-view="year prev"></li>' +
                      '<li data-view="year current"></li>' +
                      '<li class="next" data-view="year next"></li>' +
                    '</ul>' +
                    '<ul data-view="months"></ul>' +
                  '</div>' +
                  '<div class="datepicker-panel" data-view="days picker">' +
                    '<ul class="picker-days">' +
                      '<li class="prev"data-view="month prev"></li>' +
                      '<li data-view="month current"></li>' +
                      '<li class="next" data-view="month next"></li>' +
                    '</ul>' +
                    '<ul data-view="week"></ul>' +
                    '<ul data-view="days"></ul>' +
                  '</div>' +
                '</div>',
      show: function (e) {
        //console.log(e.type, e.namespace);
      },
      hide: function (e) {
        //console.log(e.type, e.namespace);
      },
      pick: function (e) {
        console.log(e.type, e.namespace, e.view);
        if($wrap.hasClass('actived')){
          $container.slideUp();
          $wrap.removeClass('actived');
        }else{
          $container.slideDown();
          $wrap.addClass('actived');
        }
      },
      update: function(e){
        console.log(e.type, e.namespace, e.view);
      }
    };

    $date.datepicker(options);

    $trigger.on('click', function (e) {
      if($wrap.hasClass('actived')){
        $container.slideUp();
        $wrap.removeClass('actived');
      }else{
        $container.slideDown();
        $wrap.addClass('actived');
      }
    });

    $date.on('click', function (e) {
      if($wrap.hasClass('actived')){
        $container.slideUp();
        $wrap.removeClass('actived');
      }else{
        $container.slideDown();
        $wrap.addClass('actived');
      }
    });
  }

  //SCROLL
  $(document).on('click touch', '.ar-scroll',function(e){
    e.preventDefault();
    var me = $(this),
        target = me.attr('href');
    if($(target).length>0 ){
      $("html, body").animate({
        scrollTop: $(target).offset().top
      }, 1000);
    }
    return false;
  });

  //CUSTOM FILTER PROYECT INTERNA
  if($("#filtros-dep-internas").length > 0) {

    $('#filtros-dep-internas .ar-select2').each(function(){
      var $placeholder = $(this).attr('placeholder');
      $(this).select2({
        placeholder: $placeholder,
      });
    });

    var dataJson = [];
    $('.departamentos-interna-slider__slick .planos-itm').each(function( index , elem ){
      var dept = new Object();
      dept.tipo = $(elem).attr('data-type');
      dept.tipo_de_vista = $(elem).attr('data-view');
      dept.pisos = $(elem).attr('data-flat');
      dept.dormitorios = $(elem).attr('data-bedrooms');
      dept.html = elem,
	  dept.plant = $(elem).attr('data-plant');
      dataJson.push(dept);
    });
    var dt = $('.departamentos-interna-slider__slick .planos-itm').length > 4 ? true : false;

    slickBlueprintsInit(dt);
    var $select = '#filtros-dep-internas .ar-select2';

    $(document).on('change', $select, function( e ){
      e.preventDefault();
      e.stopPropagation();
      var $this =  $(this),
        $form        = $this.closest('form'),
        $serialize   = $form.serializeArray(),
        $bedrooms    = ar_form_get_data('bedrooms', $serialize),
        $flat        = ar_form_get_data('flat', $serialize),
        $view        = ar_form_get_data('view', $serialize),
        $type        = ar_form_get_data('type', $serialize);

      var filter = new Object();
      if($bedrooms != '' && $bedrooms != 'all') filter.dormitorios = parseInt($bedrooms);
      if($flat != '' && $flat != 'all' ) filter.pisos = $flat;
      if($view != '' && $view != 'all' ) filter.tipo_de_vista = $view;
      if($type != '' && $type != 'all' ) filter.tipo = $type;

      let list = dataJson.filter(function (item) {
        for (var key in filter) {
          if (item[key] === undefined || item[key] != filter[key])
          return false;
        }
        return true;
      });
      slickBlueprints(list);
      slickFilter(filter);
	  imagePreview(list);
    });

	function imagePreview( $list ){
      $('.room-distribution').attr('style', '');
      if($list.length > 0 ){
        if($list.length > 1 ){
          const result = [];
          $list.forEach((item)=>{
            if(!result.includes(item.tipo)){
              result.push(item.tipo);
            }
          });
          if( result.length == 1){
            $('#image-preview').html($list[0].plant);
          }else{
            $('#image-preview').html(`<div class="h3 text-center text-uppercase w-100"> no hay planta disponible </div>`);
            $('.room-distribution').css({ height : '100%', display : 'flex', justifyContent : 'center' })
          }
        }else{
          $('#image-preview').html($list[0].plant);
        }
      }else{
        $('#image-preview').html(`<div class="h3 text-center text-uppercase w-100"> no hay planta disponible </div>`);
        $('.room-distribution').css({ height : '100%', display : 'flex', justifyContent : 'center' })
      }
    }

    function slickBlueprintsInit( $dots = true){
      $(".departamentos-interna-slider__slick").slick({
        slidesToShow: 4,
        slidesToScroll: 4,
        centerMode: false,
        arrows: true,
        prevArrow: '<div class="slick-prevArrow"><</div>',
        nextArrow: '<div class="slick-nextArrow">></div>',
        dots: $dots,
        autoplay: false,
        autoplaySpeed: 2000,
        responsive:[
          {
            breakpoint: 992,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2,
              centerMode: false,
              dots: false
            }
          }
        ]
      });
    }

    function slickBlueprintsDestroy(){
      if ($(".departamentos-interna-slider__slick").hasClass('slick-initialized')) {
        $(".departamentos-interna-slider__slick").slick('destroy');
      }
    }

    function slickBlueprints( $list ){
      slickBlueprintsDestroy();
      var $el  = $(".departamentos-interna-slider__slick");
      $el.html('');
      if( $list.length > 0 ){
        $.each($list, function(index, elem ){
          $el.append(elem.html);
        });
        var dots = $list.length > 4 ? true :false;
        slickBlueprintsInit(dots);
      }else{
        var notItems =  `<div class="w-100 position-relative py-2">
                            <p>No se encontraron items</p>
                        </div>`;
        $el.append(notItems);

      }
    }

    function slickFilter( obj ){
      var filters = $('#filtros-lista');
      var $icon = filters.attr('icon');
      var form  = $('#filtros-dep-internas form');
      var $type = form.find('select[name="type"] option:selected').text();
      var $bedrooms = form.find('select[name="bedrooms"] option:selected').text();
      var $view = form.find('select[name="view"] option:selected').text();
      var $flat = form.find('select[name="flat"] option:selected').text();

      filters.html('');
      if( obj && Object.keys(obj).length > 0 ){
        $.each(obj, function(index, elem ){
          var btn = '';
          switch (index) {
            case 'dormitorios':
              btn = `<div class="filter-resultado-el w-auto" data-select="bedrooms">
                      <span class="filter-resultado-el__dsc pr-2"> ${ $bedrooms } </span>
                      <span class="filter-resultado-el__img">
                        <img src="${$icon}" alt="icon-arteco">
                      </span>
                    </div>`;
              break;
            case 'pisos':
              btn = `<div class="filter-resultado-el w-auto" data-select="flat">
                      <span class="filter-resultado-el__dsc pr-2"> ${ $flat } </span>
                      <span class="filter-resultado-el__img">
                        <img src="${$icon}" alt="icon-arteco">
                      </span>
                    </div>`;
              break;
            case 'tipo_de_vista':
              btn = `<div class="filter-resultado-el w-auto" data-select="view">
                      <span class="filter-resultado-el__dsc pr-2"> ${ $view } </span>
                      <span class="filter-resultado-el__img">
                        <img src="${$icon}" alt="icon-arteco">
                      </span>
                    </div>`;
              break;
            case 'tipo':
              btn = `<div class="filter-resultado-el w-auto" data-select="type">
                      <span class="filter-resultado-el__dsc pr-2"> ${ $type } </span>
                      <span class="filter-resultado-el__img">
                        <img src="${$icon}" alt="icon-arteco">
                      </span>
                    </div>`;
              break;
          }
          filters.append(btn);
        });
        var clear =   `<div class="filter-resultado-delete">
                        <a href="#clear" class="">Borrar filtros</a>
                      </div>`;
        filters.append(clear);
      }
    }

    $(document).on('click touch', '.filter-resultado-delete', function( e ){
      e.preventDefault();
      e.stopPropagation();
      $('#filtros-dep-internas .ar-select2').val('').trigger('change');
      return false;
    });

    $(document).on('click touch', '.filter-resultado-el .filter-resultado-el__img', function( e ){
      e.preventDefault();
      e.stopPropagation();
      var closest = $(this).closest('.filter-resultado-el'),
          select = closest.attr('data-select');
      $('#filtros-dep-internas .ar-select2[name="'+select+'"]').val('').trigger('change');
      return false;
    });

    $(document).on('click touch', '.ar-close-modal-plans', function(e){
      e.preventDefault();
      e.stopPropagation();
      var $btn = $(this),
          $bedrooms = $btn.attr('bedrooms'),
          $flat = $btn.attr('flat'),
          $view = $btn.attr('view'),
          $departament = $btn.attr('depto'),
          $form = $('.contacto-el-form');

      $('.modal-card').modal('hide');
      if($form.length > 0){
        var $scroll = $form.offset().top- $('header .menu').height();
        $form.find('select[name="bedrooms"]').val( $bedrooms );
        resetSelectAr( $form.find('select[name="bedrooms"]') );
        $form.find('select[name="flat"] ').val( $flat );
        resetSelectAr( $form.find('select[name="flat"]') );
        $form.find('select[name="view"] ').val( $view );
        resetSelectAr( $form.find('select[name="view"]') );
        $form.find('select[name="departament"]').val( $departament );
        resetSelectAr( $form.find('select[name="departament"]') );
        $('html, body').animate({ scrollTop: $scroll }, 'slow');

      }
    })
  }

  function resetSelectAr( $select ){
    var $placeholder = $select.attr('placeholder');
    $select.select2('destroy').select2({
      placeholder: $placeholder,
    });
  }

  //recaptcha
  $(document).on('click','.ajax-claim-send', function( e ){
    e.preventDefault();
    e.stopPropagation();
    var $this   = $(this),
        $width  = $this.innerWidth(),
        $form   = $this.closest('form'),
        $text   = $this.html(),
        $data   = $form.serializeArray()  || 0 ,
        gRecapt = grecaptcha.getResponse();
    if( $form[0].checkValidity() && gRecapt != ''){
      $this
        .css({ width : $width })
        .html('<span class="ar-spinner spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>')
        .addClass('disabled')
        .prop('disabled', true );
      $data.push({name: 'gcaptcha', value: gRecapt });

      ar_block( $form );
      $.post(ajax_script.ajax_url, $data, function( response ){
        $this
          .css({ width : 'auto' })
          .html($text)
          .removeClass('disabled')
          .prop('disabled', false );
        if(response.status == true ){
          $form[0].reset();
          $form.removeClass('was-validated');
          ar_unblock( $form );
        }

        if(response.status == false ){
          ar_unblock( $form );
        }

        if( response.fragments ){
          ar_wc_show_modal(response.fragments);
        }

      }, 'JSON');
    }else{
      $form.addClass('was-validated');
      var $input = $form.find('input:invalid');
      if($input.eq(0).length > 0){
        var $scroll = $input.eq(0).offset().top- $('header .menu').height() - 20;
        $('html, body').animate({ scrollTop: $scroll }, 'slow');
      }
    }

  })

  //post singles proprect
  $(document).on('click','.ar-send-contact-post' , function( e ){
    e.preventDefault();
    e.stopPropagation();
    var $this   = $(this),
        $width  = $this.innerWidth(),
        $form   = $this.closest('form'),
        $text   = $this.html(),
        $data   = $form.serializeArray()  || 0 ;
    if( $form[0].checkValidity()){
      $this
        .css({ width : $width })
        .html('<span class="ar-spinner spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>')
        .addClass('disabled')
        .prop('disabled', true );
      ar_block( $form );
      $.post(ajax_script.ajax_url, $data, function( response ){
        console.log(response);
        $this
          .css({ width : 'auto' })
          .html($text)
          .removeClass('disabled')
          .prop('disabled', false );

        if(response.status == true ){
          $form[0].reset();
          ar_unblock( $form );
		      $form.removeClass('was-validated');
          $('.contact-post-form').hide()
          $('#gracias_form').show()
        }

        if(response.status == false ){
          ar_unblock( $form );
        }

        if( response.hubspot ){
          var name = response.hubspot.firstname+' '+response.hubspot.lastname;
          $('#gracias_form .name-thank').html(name);
        }

      }, 'JSON');

    }else{
      $form.addClass('was-validated');
    }
    return false;
  });

  //SUSCRIPTION
  $(document).on('click', '.send-suscription', function(e){

    e.preventDefault();
    e.stopPropagation();
    var $this   = $(this),
        $width  = $this.innerWidth(),
        $form   = $this.closest('form'),
        $text   = $this.html(),
        $data   = $form.serializeArray()  || 0 ;
    if( $form[0].checkValidity()){
      $this
        .css({ width : $width })
        .html('<span class="ar-spinner spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>')
        .addClass('disabled')
        .prop('disabled', true );
      ar_block( $form );
      $.post(ajax_script.ajax_url, $data, function( response ){
        console.log(response);
        $this
          .css({ width : 'auto' })
          .html($text)
          .removeClass('disabled')
          .prop('disabled', false );

        if(response.status == true ){
          $form[0].reset();
          ar_unblock( $form );
		      $form.removeClass('was-validated');
        }

        if(response.status == false ){
          ar_unblock( $form );
        }

        if( response.fragments ){
          ar_wc_show_modal(response.fragments);
        }

      }, 'JSON');

    }else{
      $form.addClass('was-validated');
    }
    return false;
  })

})(jQuery);;
