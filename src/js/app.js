
var $ = jQuery.noConflict();


$('[data-toggle="tooltip"]').tooltip()

if($(".banner__slick").length>0){
  $(".banner__slick").slick({
    slidesToShow: 1,
    centerMode: false,
    arrows: false,
    dots: true,
    autoplay: true,
    autoplaySpeed: 3500,
    appendDots:$(".banner__dots")
  });
  $('.banner__slick').on('beforeChange', function(event, slick, currentSlide, nextSlide){
    console.log(nextSlide);
    $(".banner__list-opt").eq(nextSlide).trigger("click");
  });


}

$(".banner__list-opt").on("click", function(e) {
  $(".banner__list-opt").removeClass("active");
  var $idx = $(this).index();
  $(this).addClass("active");
  $(".banner__card").removeClass("active").eq($idx).addClass("active");
  $('.banner__slick').slick('slickGoTo', $idx);
})

if($(".proyectos__slick").length>0){
  $(".proyectos__slick").slick({
    slidesToShow: 3,
    centerMode: false,
    arrows: true,
    prevArrow: '<div class="slick-prevArrow"><</div>',
    nextArrow: '<div class="slick-nextArrow">></div>',
    dots: false,
    autoplay: false,
    autoplaySpeed: 2000,
    responsive:[
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          centerMode: false,
          dots: false
        }
      }
    ]
  });
}

if($(".proyectos__slick-2").length>0){
  $(".proyectos__slick-2").slick({
    slidesToShow: 4,
    centerMode: false,
    arrows: true,
    prevArrow: '<div class="slick-prevArrow"><</div>',
    nextArrow: '<div class="slick-nextArrow">></div>',
    dots: false,
    autoplay: false,
    autoplaySpeed: 2000,
    responsive:[
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          centerMode: false,
          dots: false
        }
      }
    ]
  });
}


if($(".clientes__slick").length>0){
  $(".clientes__slick").slick({
    slidesToShow: 3,
    centerMode: false,
    arrows: true,
    prevArrow: '<div class="slick-prevArrow"><</div>',
    nextArrow: '<div class="slick-nextArrow">></div>',
    dots: false,
    autoplay: false,
    autoplaySpeed: 2000,
    responsive:[
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          centerMode: false,
          dots: false
        }
      }
    ]
  });
}


if($(".noticias__slick").length>0){
  $(".noticias__slick").slick({
    slidesToShow: 3,
    centerMode: false,
    arrows: true,
    prevArrow: '<div class="slick-prevArrow"><</div>',
    nextArrow: '<div class="slick-nextArrow">></div>',
    dots: false,
    autoplay: false,
    autoplaySpeed: 2000,
    responsive:[
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          centerMode: false,
          dots: false
        }
      }
    ]
  });
}


if($(".slider-ss__slick").length>0){
  $(".slider-ss__slick").slick({
    slidesToShow: 1,
    centerMode: false,
    arrows: true,
    prevArrow: '<div class="slick-prevArrow"><</div>',
    nextArrow: '<div class="slick-nextArrow">></div>',
    dots: false,
    autoplay: false,
    autoplaySpeed: 2000,
    responsive:[
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          centerMode: false,
          dots: false
        }
      }
    ]
  });
}


if($(".slider-ss__slick-2").length>0){
  $(".slider-ss__slick-2").each(function () {
    var $this = $(this);
    var id = $this.data("id");
    if(!id)return;
    $this.slick({
      slidesToShow: 1,
      centerMode: false,
      arrows: true,
      prevArrow: '<div class="slick-prevArrow"><</div>',
      nextArrow: '<div class="slick-nextArrow">></div>',
      dots: true,
      autoplay: false,
      autoplaySpeed: 2000,
      appendDots:$(".slider-ss__slick-dots.slider-ss__slick-dots-"+id),
      responsive:[
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            centerMode: false,
            dots: false
          }
        }
      ]
    });
  })
}

if($('.js-timeline').length>0){
  $('.js-timeline').Timeline({
    dotsPosition: 'top'
  });
}

/* if($(".departamentos-interna-slider__slick").length>0){
  $(".departamentos-interna-slider__slick").slick({
    slidesToShow: 4,
    centerMode: false,
    arrows: true,
    prevArrow: '<div class="slick-prevArrow"><</div>',
    nextArrow: '<div class="slick-nextArrow">></div>',
    dots: true,
    autoplay: false,
    autoplaySpeed: 2000,
    responsive:[
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          centerMode: false,
          dots: false
        }
      }
    ]
  });
} */

if($('.jc-astoria-maps').length>0){
  // var _markers = [];
  // var _this = $('.jc-astoria-maps-mobile')
  // if($(window).width()>992){
  //   _this = $('.jc-astoria-maps');
  // }

  // var _number = Math.floor(Math.random() * (100 - 0 + 1) + 0);
  // var _mapID = 'map-'+_number;
  // _this.attr('id', _mapID);
  // var _lat  = _this.attr('lat');
  // var _lng  = _this.attr('lng');
  // var _icon = _this.attr('icon');

  // var _LatLng = { lat: parseFloat(_lat), lng: parseFloat(_lng) };

  // console.log("window.google: ", window.google)
  // var map = new window.google.maps.Map(document.getElementById(_mapID), {
  //   center: _LatLng,
  //   zoom: 16,
  //   disableDefaultUI: true,
  // });

  // var _styles = [
  //   {
  //     featureType: "poi",
  //     stylers: [
  //     { visibility: "off" }
  //     ]
  //   }
  // ];

  // var _styledMap = new google.maps.StyledMapType(_styles, {
  //   name: "Styled Map"
  // });

  // map.mapTypes.set('map_style', _styledMap);
  // map.setMapTypeId('map_style');

  // function jc_clear( _list ){
  //   for (var i = 0; i < _list.length; i++ ) {
  //     _list[i].setMap(null);
  //   }
  //   _list.length = 0;
  // }

  // function jc_markers( _position, _time, _icon, _map, _marker, _index ){
  //   window.setTimeout(function() {
  //     var marker = new google.maps.Marker({
  //       position: _position,
  //       map: _map,
  //       animation: google.maps.Animation.DROP,
  //       icon: _icon,
  //       visible: false,
  //     });

  //     marker.visible = true;
  //     _marker.push(marker);
  //   }, _time );
  // }

  // $(document).on('click', '.jc-maps-btn', function(){
  //   var _this = $(this), _index = _this.attr('index');
  //   $('[list]').slideUp();
  //   var _list = $('[list="'+_index+'"]');
  //   jc_clear(_markers);

  //   _list.children('label').each(function(index, value){
  //     var _me = $(this),
  //         _itemLat = _me.attr('lat'),
  //         _itemLng = _me.attr('lng'),
  //         _itemIcon = _me.attr('icon');
  //     var _pos = new google.maps.LatLng(_itemLat , _itemLng);
  //     console.log(_pos);
  //     jc_markers( _pos , index*300 , _itemIcon, map , _markers , index );
  //   });
  //   setTimeout(function(){
  //     _list.slideDown();
  //   }, 500)

  // })

}


/* if($('#nmap').length>0){

  var _LatLng = { "lat": -12.060528443072426, "lng": -77.04129324476092 }
  var _mapID = "nmap";
  if($('#'+_mapID).length >0){
    _LatLng = { "lat": parseFloat($('#'+_mapID).attr('lat')), "lng": parseFloat($('#'+_mapID).attr('lng')) };
  }
  var map = new window.google.maps.Map(document.getElementById(_mapID), {
      center: _LatLng,
      zoom: 16,
      disableDefaultUI: true,
    });


function addMarker(prop) {
  var marker = new google.maps.Marker({
     position: prop.coordinates, // Passing the coordinates
     map:map, //Map that we need to add
     draggarble: false// If set to true you can drag the marker
  });
  if(prop.iconImage) { marker.setIcon(prop.iconImage); }
  if(prop.content) {
     var information = new google.maps.InfoWindow({
     content: prop.content
     });

     marker.addListener('click', function() {
     information.open(map, marker);
     });
  }
}

  $(".marker").each(function(){
    var $this = $(this);
    var tipo = $this.data("type");
    var img = $this.data("img");
    var desc = $this.data("desc");
    var title = $this.data("title");
    if(tipo == "big"){
      var html = "<div class=big-marker'>"+
        "<div class='big-marker__img'>"+
          "<img src='"+ img +"' />"+
        "</div>"+
        "<div class='big-marker__title'>"+
          title+
        "</div>"+
        "<div class='big-marker__desc'>"+
          desc+
        "</div>"+
      "</div>";
      addMarker({coordinates:{ "lat": -12.060528443072426, "lng": -77.04129324476092 }, content: html});
    }
  });

} */



$(".show-mas").on("click", function(e){
  console.log($(this).parent().hide().next().show())
  $(this).parent()
    .removeClass("d-block")
    .addClass("d-none")
    .next()
    .removeClass("d-none")
    .addClass("d-block");
  e.preventDefault();
});


$(".show-menos").on("click", function(e){
  $(this).parent()
  .removeClass("d-block")
    .addClass("d-none")
    .prev()
    .removeClass("d-none")
    .addClass("d-block");
  e.preventDefault();
});


$(".alianza").on("click", function(e){
  var $this = $(this);
  var index = $this.parent().index();
  $(".alianza").removeClass("active");
  $this.addClass("active");

  $(".alianza-desc").removeClass("active");
  $(".alianza-desc").eq(index).addClass("active");
});


if($(".slider-categorias__slick").length>0){
  $(".slider-categorias__slick").slick({
    slidesToShow: 6,
    centerMode: false,
    arrows: true,
    prevArrow: '<div class="slick-prevArrow"><</div>',
    nextArrow: '<div class="slick-nextArrow">></div>',
    dots: false,
    autoplay: false,
    autoplaySpeed: 2000
  });
}

$(".pregunta-item__mas, .pregunta-item__arrow, .pregunta-item__title").on("click", function(){
  $(this).parent().toggleClass("active");
});

if($('#modalPromo').length>0){
  setTimeout(function(){
    $('#modalPromo').modal('show');
  }, 3*1000)
}

if($(".coockies-close").length>0){
  $(".coockies-close").on("click", function(){
    $(this).closest(".coockies").hide();
  });
}


// MODAL VIDEO


$(document).ready(function() {

  // Gets the video src from the data-src on each button

  var $videoSrc;
  $('.video-btn').click(function() {
      $videoSrc = $(this).data( "src" );
  });
  console.log($videoSrc);


  // when the modal is opened autoplay it
  $('#modalVideo').on('shown.bs.modal', function (e) {

  // set the video src to autoplay and not to show related video. Youtube related video is like a box of chocolates... you never know what you're gonna get
  $("#video").attr('src',$videoSrc + "?autoplay=1&amp;modestbranding=1&amp;showinfo=0" );
  })


  // stop playing the youtube video when I close the modal
  $('#modalVideo').on('hide.bs.modal', function (e) {
      // a poor man's stop video
      $("#video").attr('src',$videoSrc);
  })


  var $zoomSrc;
  $('.zoom-btn').click(function() {
    $zoomSrc = $(this).data( "src" );
  });
  console.log($zoomSrc);
    // when the modal is opened autoplay it
  $('#modalZoom').on('shown.bs.modal', function (e) {

    // set the video src to autoplay and not to show related video. Youtube related video is like a box of chocolates... you never know what you're gonna get
    $("#zoom").attr('src',$zoomSrc);
    })


  // document ready
  });


// ISOTOPE

function getFilterIsotope(filters, distrito){
  var filterIsotope = [];
  if(distrito=="all"){
    filterIsotope = filters.length ==0 ?'*': JSON.parse(JSON.stringify(filters));
  }
  else{
    $.map(filters, function(el, i){
      filterIsotope.push(el+distrito);
    });
    filterIsotope = filters.length==0? [distrito]: filterIsotope;
  }
  return filterIsotope;
}

function getFilterIsotope_dormitorio(filters, dormitorio){
  var filterIsotope_dormitorio = [];
  if(dormitorio=="all"){
    filterIsotope_dormitorio = filters.length ==0 ?'*': JSON.parse(JSON.stringify(filters));
  }
  else{
    $.map(filters, function(el, i){
      filterIsotope_dormitorio.push(el+dormitorio);
    });
    filterIsotope_dormitorio = filters.length==0? [dormitorio]: filterIsotope_dormitorio;
  }
  return filterIsotope_dormitorio;
}

function getFilterIsotope_tipo(filters, tipo){
  var filterIsotope_tipo = [];
  if(tipo=="all"){
    filterIsotope_tipo = filters.length ==0 ?'*': JSON.parse(JSON.stringify(filters));
  }
  else{
    $.map(filters, function(el, i){
      filterIsotope_tipo.push(el+tipo);
    });
    filterIsotope_tipo = filters.length==0? [tipo]: filterIsotope_tipo;
  }
  return filterIsotope_tipo;
}

$(document).ready(function(){

  if($("#proyectos").length>0){
  //  var filters = [];
    var filter = [];
    var distrito = "all";
    $grid = $("#proyectos");
    $(".proyectos__filters-el-btn").on("click", function(){
      var $this = $(this);
      //var filter = $(this).data("filter");
      filter = $(this).data("filter");
      //var index = filters.indexOf(filter);
      distrito = $('input[name="distrito"]:checked').val();

      $(".proyectos__filters-el-btn").removeClass("active");
      $this.addClass("active");

      var filterIsotope = getFilterIsotope(filter?[filter]:[], distrito);
      $grid.isotope({ filter: filterIsotope.join(", ") })
    });

    $('input[type=radio][name=distrito]').on('change', function(){
      distrito = $('input[name="distrito"]:checked').val();
      var filterIsotope = getFilterIsotope(filter?[filter]:[], distrito);
      $grid.isotope({ filter: filterIsotope.join(", ") })
    });
  }

  if($("#planos").length>0){
      var filter = [];
      var dormitorio = "all";
      var tipo = "all";
      $planos = $("#planos .slick-track");

      $('input[type=radio][name=dormitorio]').on('change', function(){
        dormitorio = $('input[name="dormitorio"]:checked').val();
        var filterIsotope_dormitorio = getFilterIsotope_dormitorio(filter?[filter]:[], dormitorio);
        $planos.isotope({ filter: filterIsotope_dormitorio.join(', ') })
      });

      $('input[type=radio][name=tipo]').on('change', function(){
        tipo = $('input[name="tipo"]:checked').val();
        var filterIsotope_tipo = getFilterIsotope_tipo(filter?[filter]:[], tipo);
        $planos.isotope({ filter: filterIsotope_tipo.join(', ') })
      });


      }


})



// Menu

var didScroll;
var lastScrollTop = 0;
var delta = 5;
var navbarHeight = $('.menu').outerHeight();

$(window).scroll(function(event){
    didScroll = true;
});

setInterval(function() {
    if (didScroll) {
        hasScrolled();
        didScroll = false;
    }
}, 250);

function hasScrolled() {
    var st = $(this).scrollTop();

    // Make sure they scroll more than delta
    if(Math.abs(lastScrollTop - st) <= delta)
        return;

    // If they scrolled down and are past the navbar, add class .nav-up.
    // This is necessary so you never see what is "behind" the navbar.
    if (st > lastScrollTop && st > navbarHeight){
        // Scroll Down
        $('.menu').removeClass('nav-down').addClass('nav-up');
    } else {
        // Scroll Up
        if(st + $(window).height() < $(document).height()) {
            $('.menu').removeClass('nav-up').addClass('nav-down');
        }
    }

    lastScrollTop = st;
}

// Galeria
function initGalery($btnGalery, $groups, group_active_class, $sliders){
  $btnGalery.on("click", function(){
    var $this = $(this);
    var group = $this.data("group");
    $btnGalery.removeClass("active");
    $this.addClass("active");
    $groups.addClass("d-none");
    console.log("group: ", group)
    $(group_active_class+group).removeClass("d-none");
    $sliders.slick('setPosition');
  });

}

if($(".galeria-btn__el-js").length>0){
  initGalery($(".galeria-btn__el-js"),
            $(".galeria-body__el-js"),
            ".galeria-body__el-js.group",
            $(".slider-ss__slick"));
}

if($(".departamento-cacteristicas-el-js").length>0){
  var $btnGalery = $(".departamento-cacteristicas-el-js");
  var $groups = $(".departamento-cacteristicas-el-body-js");
  var group_active_class =".departamento-cacteristicas-el-body-js.group";
  var $sliders = $(".slider-ss__slick");

  $btnGalery.on("click", function(){
    var $this = $(this);
    var group = $this.data("group");
    $btnGaleryGroup = $this
              .closest(".departamento-cacteristicas-el-js-wrapper")
              .find(".departamento-cacteristicas-el-js");
    $btnGaleryGroup.removeClass("active");
    $this.addClass("active");
    var $groups = $this
            .closest(".departamento-cacteristicas-el-js-wrapper")
            .next(".departamento-cacteristicas-el-body-js-wrapper")
            .find(".departamento-cacteristicas-el-body-js");
    $groups.addClass("d-none");
    console.log("group: ", group)
    $(group_active_class+group).removeClass("d-none");
    $sliders.slick('setPosition');
  })

}
/* 

if($("#filtros-dep-internas").length > 0) {

  var filters = [];
  var $filtersWrapper = $("#filtros-dep-internas");
  var filterElLeft = '<div class="filter-resultado-el"><span class="filter-resultado-el__dsc">'
  var filterElRight = '</span><span class="filter-resultado-el__img"><img src="http://localhost/arteco/wp-content/themes/arteco/src/img/equis.png" alt=""></span></div>'
  var $filtersList = $("#filtros-lista");
  var filtersClass = ['input[type="radio"][name="tipo"]',
                      'input[type="radio"][name="vista"]',
                      'input[type="radio"][name="dormitorio"]',
                      'input[type="radio"][name="piso"]'];


  $filtersWrapper.find(filtersClass.join(", ")).on("change", function() {
    // console.log("$this: ", $(this));
    // console.log("this: ", this);
    console.log("this.name: ", this.name)
    console.log("this.value: ", this.value)
    console.log("this.dataClass: ", this.dataset.class)

var valor = this.value.replace(/^.*(\d+).*$/i,'$1');

    var filterObj = {
      label: this.value,
      name: this.name,
      dataclass: this.dataset.class,
      datahtml:  filterElLeft+this.name+' '+valor+filterElRight
    }

    var removeIndex = filters.map(item => item.name).indexOf(this.name);
    ~removeIndex && filters.splice(removeIndex, 1);
    if(this.value !='all')filters.push(filterObj);

    $filtersList.find(".filter-resultado-el").remove();
    $.each(filters, function(index, obj) {
      console.log(obj.datahtml)
      $filtersList.prepend(obj.datahtml);
    });

  });

   $filtersList.find(".filter-resultado-delete").on("click", function(){

      $filtersList.find(".filter-resultado-el").remove();

      $("input[type='radio'][value='all']").prop("checked", "checked").val('all').change();

   });



} */

if($(".pasos-timeline").length > 0){
  var $pasosTimeline = $(".pasos-timeline");
  var currentPage = 0;
  var currentOption = 0;

  function updatePasosOpciones(){
    var $options = $(".opcion-pago");
    var $pages = $(".paso");
    var $pageTimelines = $(".pasos-timeline").find("li");

    $pages.each(function () {
      var $optionsInPaso = $(this).find(".opcion-pago");
      $optionsInPaso.removeClass("active");
      $optionsInPaso.eq(currentOption).addClass("active");
    })
    $pages.removeClass("active");
    $pages.eq(currentPage).addClass("active");
    $pageTimelines.eq(currentPage).addClass("active");
  }

  $(".custom-card").on("click", function(){
    currentOption = $(this).closest(".custom-card").parent().index();
    currentPage = 1;
    updatePasosOpciones();
  });

  $(".aceptar-comunicado").on("click", function(){
    currentPage = 2;
    $('#mensajeModal').modal('hide');
    updatePasosOpciones();
  });


  $(".enviar-form").on("click", function(){
    currentPage = 3;
    updatePasosOpciones();
  });

  $(".pagar").on("click", function(){
    currentPage = 4;
    updatePasosOpciones();
  })
}


// Botón para arriba

$(window).scroll(function() {
  if ($(this).scrollTop()) {
      $('#toTop').fadeIn();
  } else {
      $('#toTop').fadeOut();
  }
});

$("#toTop").click(function () {
 //1 second of animation time
 //html works for FFX but not Chrome
 //body works for Chrome but not FFX
 //This strange selector seems to work universally
 $("html, body").animate({scrollTop: 0}, 1000);
});


if($(".foo").length > 0) {
  console.log("hello")
  document.querySelector('.foo').fakeScroll();
}

if($(".select-box__list").length > 0) {
  console.log($(".select-box__list"));
  $(".select-box__list").each(function(){
    console.log($(this)[0])
    var height = "260"
    if($(this).data("height")=="1")height = "400"
    if($(this).find("li").length > 5) {
      $(this).css("height", height)
      $(this)[0].fakeScroll();
    }
  })
}

$(".select-box__current").on("click", function(){
  $(this).toggleClass("active");
})

//////////// MAPS ///////////////
let map, popup, Popup, popup_list=[];

/** Initializes the map and the custom popup. */
function initMap() {
  var lat = parseFloat($("#nmap").data("lat"));
  var lng = parseFloat($("#nmap").data("lng"));
  map = new google.maps.Map(document.getElementById("nmap"), {
    center: { lat, lng},
    zoom: 18,
    //disableDefaultUI: true,
  });
  /**
   * A customized popup on the map.
   */
  class Popup extends google.maps.OverlayView {
    position;
    containerDiv;
    constructor(position, content) {
      super();
      this.position = position;
      content.classList.add("popup-bubble");

      // This zero-height div is positioned at the bottom of the bubble.
      const bubbleAnchor = document.createElement("div");

      bubbleAnchor.classList.add("popup-bubble-anchor");
      bubbleAnchor.appendChild(content);
      // This zero-height div is positioned at the bottom of the tip.
      this.containerDiv = document.createElement("div");
      this.containerDiv.classList.add("popup-container");
      this.containerDiv.appendChild(bubbleAnchor);
      // Optionally stop clicks, etc., from bubbling up to the map.
      Popup.preventMapHitsAndGesturesFrom(this.containerDiv);
    }
    /** Called when the popup is added to the map. */
    onAdd() {
      this.getPanes().floatPane.appendChild(this.containerDiv);
    }
    /** Called when the popup is removed from the map. */
    onRemove() {
      if (this.containerDiv.parentElement) {
        this.containerDiv.parentElement.removeChild(this.containerDiv);
      }
    }
    /** Called each frame when the popup needs to draw itself. */
    draw() {
      const divPosition = this.getProjection().fromLatLngToDivPixel(
        this.position
      );
      // Hide the popup when it is far out of view.
      const display =
        Math.abs(divPosition.x) < 4000 && Math.abs(divPosition.y) < 4000
          ? "block"
          : "none";

      if (display === "block") {
        this.containerDiv.style.left = divPosition.x + "px";
        this.containerDiv.style.top = divPosition.y + "px";
      }

      if (this.containerDiv.style.display !== display) {
        this.containerDiv.style.display = display;
      }
    }
  }

  // popup = new Popup(
  //   new google.maps.LatLng(-12.059899525363713,-77.03822651322541),
  //   document.getElementById("content")
  // );
  // popup.setMap(map);

    function initMarkers(){
      for(var i=0; i<popup_list.length;i++){
        popup_list[i].setMap(null)
      }
      popup_list = [];
      var category = $(".butn-ico.active").data("category");
      $(".marker").addClass("d-none");
      $(".marker."+category).each(function(){
        var $this = $(this).clone();
        $this.removeClass("d-none");
        
        if($this.data("default")=="1"){
          $this.find(".small-marker").hide();
          $this.find(".big-marker").css({"display": "block"});
        }
        popup = new Popup(
          new google.maps.LatLng($this.data("lat"),$this.data("lng")),
          $this[0]
        );
        popup.setMap(map);
        popup_list.push(popup)
      });
    }

    initMarkers();

    $(".butn-ico").on("click", function(){
      var $this = $(this);
      $(".butn-ico").removeClass("active");
      $this.addClass("active");
      initMarkers();
    });
}

if($('#nmap').length>0){
  initMap();

  $('#nmap').on("click", ".small-marker", function(){
    console.log($(this));
    var $this = $(this);
    $(".big-marker").hide(300);
    $this.next(".big-marker").show(300);
    $(".small-marker").show();
    $this.hide();
  })

}

$(".click-close-modal").on("click", function(){
  console.log("click-close-modal: ", $(this).data("modalid"))
  $('#'+$(this).data("modalid")).modal('hide');
})
