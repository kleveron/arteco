<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Arteco
 */

get_header('dark');
?>
<div class="section" style="background-image: url(<?php echo get_template_directory_uri(); ?>/src/img/background-404.png)">
	<div class="container">
		<div class="banner__butn-wrapper d-none d-lg-block">
			<div class="banner__butn-content">
				<a href="#" class="banner__butn-opt banner__butn-chat">
					<img src="<?php echo get_template_directory_uri(); ?>/src/img/ico-chat.png" alt="">
				</a>
				<a href="#" class="banner__butn-opt banner__butn-ws">
					<img src="<?php echo get_template_directory_uri(); ?>/src/img/ico-whatsapp.png" alt="">
				</a>
				<a href="#" class="banner__butn butn butn-1">LLAMA GRATIS</a>
			</div>
		</div>
		<div class="element-404">
			<img src="<?php echo get_template_directory_uri(); ?>/src/img/404.png" alt="">
			<div class="element-404__opts">
				<a href="<?php echo get_home_url(); ?>/proyectos">Proyectos</a>
			<!--	<a href="#">Noticias</a> -->
				<a href="<?php echo get_home_url(); ?>/nosotros">Nosotros</a>
			</div>
			<div class="text-center mt-5">
				<a class="butn butn-1" href="<?php echo get_home_url(); ?>">Volver al inicio</a>
			</div>
		</div>
	</div>
</div>
<?php
get_footer();
