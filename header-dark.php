<?php
/**
 * Header Dark
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Arteco
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <?php wp_head(); ?>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
          integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />
    <!-- <link rel="stylesheet" href="../src/css/owl.carousel.min.css">
      <link rel="stylesheet" href="../src/css/owl.theme.default.min.css"> -->
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" />
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/src/libraries/timelinejs/timeline.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/src/css/vendor.css?v=<?= time(); ?>">

</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>

<div id="page" class="site">
    <a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e( 'Skip to content', 'arteco' ); ?></a>

    <header id="masthead" class="site-header">

        <div class="menu menu-dark" <?php /*if( is_user_logged_in()){?>  style="top: 32px;" <?php } */?> >
            <div class="container">
                <div class="menu__inner">
                    <div class="menu__logo">
                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
                          <?php  if( have_rows('logos', 'option') ):
                                while( have_rows('logos', 'option') ): the_row();
                                  $principal = get_sub_field('principal');
                                  $color = get_sub_field('color');
                                  $blanco = get_sub_field('blanco');

                                ?>
                            <img src="<?php echo $blanco; ?>" alt="">
                            <img src="<?php echo $blanco; ?>" alt="">
                              <?php   endwhile;  endif; ?>
                        </a>
                    </div>
                    <div class="ar_navigation">
                        <div class="menu__opciones">
                            <?php     $menuprincipal =  array(
                                'theme_location' => 'menu-1',
                                'menu'          => '',
                                'container'      => '',
                                'container_class' => false,
                                'container_id' => '',
                                'menu_class'     => 'menu__opciones',
                                'menu_id'        => 'primary-menu',
                                'echo'            => false,
                                'fallback_cb'     => 'wp_page_menu',
                                'before'          => '',
                                'after'           => '',
                                'link_before'     => '<span>',
                                'link_after'      => '</span>',
                                'depth'           => 0,
                                'walker'          => ''
                            );

                            echo str_replace (
                                '<a' , '<a class="menu__opcion" ' ,
                                strip_tags(wp_nav_menu( $menuprincipal ),'<a><span>' ) );

                            ?>

                        </div>
                    </div>
                    <div class="menu__boton">
                        <a href="separa-depa.html" class="butn butn-1">Separa tu depa</a>
                        <button class="ar_navbar-toggler"></button>
                    </div>
                </div>


            </div>
        </div>


    </header> <!-- #masthead -->


    <!-- Menu -->
