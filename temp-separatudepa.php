<?php /* Template Name: Template Separa tu depa*/ ?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <title>Arteco</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
    integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />
  <!-- <link rel="stylesheet" href="../src/css/owl.carousel.min.css">
    <link rel="stylesheet" href="../src/css/owl.theme.default.min.css"> -->
  <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" />
  <link rel="stylesheet" href="../src/css/vendor.css">
</head>

<body>
  <!-- Header -->

  <!-- Menu -->
  <div class="menu menu-dark">
    <div class="container">
      <div class="menu__inner">
        <div class="menu__logo">
          <a href="index.html">
            <img src="../src/img/logo-white.png" alt="">
            <img src="../src/img/logo-white.png" alt="">
          </a>
        </div>
        <div class="ar_navigation">
        <div class="menu__opciones">
          <a class="menu__opcion" href="departamentos.html">
            <span>
              Proyectos
            </span>
          </a>
          <a class="menu__opcion" href="blog.html">
            <span>
              Noticias
            </span>
          </a>
          <a class="menu__opcion" href="nosotros.html">
            <span>
              Nosotros
            </span>
          </a>
          <a class="menu__opcion" href="contacto.html">
            <span>
              Contacto
            </span>
          </a>
        </div>
        </div>
        <div class="menu__boton">
          <a href="separa-depa.html" class="butn butn-1">Separa tu depa</a>
          <button class="ar_navbar-toggler"></button>
        </div>
      </div>


    </div>
  </div>


  <div>
    <div class="pasos-timeline-wrapper">
        <div class="container position-relative">
        <ul class="pasos-timeline">
          <li class="active">
            <img src="../src/img/check.png" alt="">
          </li>
          <li>
            <img src="../src/img/check.png" alt="">
          </li>
          <li>
            <img src="../src/img/check.png" alt="">
          </li>
          <li>
            <img src="../src/img/check.png" alt="">
          </li>
          <li>
            <img src="../src/img/check.png" alt="">
          </li>
        </ul>
      </div>
      <div class="banner__butn-wrapper d-none d-lg-block">
        <div class="banner__butn-content">
          <a href="#" class="banner__butn-opt banner__butn-chat">
            <img src="../src/img/ico-chat.png" alt="">
          </a>
          <a href="#" class="banner__butn-opt banner__butn-ws">
            <img src="../src/img/ico-whatsapp.png" alt="">
          </a>
          <a href="#" class="banner__butn butn butn-1">LLAMA GRATIS</a>
        </div>
      </div>
    </div>
  </div>


  <!-- Elegir proyecto -->

  <div class="section bg-color-11">
    <div class="container">
      <div class="paso-wrapper">
        <div class="paso active">
          <h2 class="h2">
            Elige un proyecto
          </h2>
          <div class="mt-4">
            <div class="row">
              <div class="col-lg-4">
                <div class="custom-card custom-card--2 text-left my-4">
                  <div class="custom-card__img-wrapper">
                    <img class="custom-card__img img-fluid" src="../src/img/elegir-proyecto-1.png" alt="">
                    <h3 class="custom-card__title h3 ln-h3">
                      Distrito
                    </h3>
                  </div>
                  <div>
                    <div class="px-3 py-3">
                      <div class="row align-items-center">
                        <div class="col-lg-12">
                          <h3 class="h4 custom-card__subtitle">
                              La Campiña IV
                          </h3>
                          <div class="mt-1">
                            Prolong. Iquitos 1843, Lince
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-4">
                <div class="custom-card custom-card--2 text-left my-4">
                  <div class="custom-card__img-wrapper">
                    <img class="custom-card__img img-fluid" src="../src/img/elegir-proyecto-2.png" alt="">
                    <h3 class="custom-card__title h3 ln-h3">
                      Distrito
                    </h3>
                  </div>
                  <div>
                    <div class="px-3 py-3">
                      <div class="row align-items-center">
                        <div class="col-lg-12">
                          <h3 class="h4 custom-card__subtitle">
                              La Campiña IV
                          </h3>
                          <div class="mt-1">
                            Prolong. Iquitos 1843, Lince
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-4">
                <div class="custom-card custom-card--2 text-left my-4">
                  <div class="custom-card__img-wrapper">
                    <img class="custom-card__img img-fluid" src="../src/img/elegir-proyecto-3.png" alt="">
                    <h3 class="custom-card__title h3 ln-h3">
                      Distrito
                    </h3>
                  </div>
                  <div>
                    <div class="px-3 py-3">
                      <div class="row align-items-center">
                        <div class="col-lg-12">
                          <h3 class="h4 custom-card__subtitle">
                              La Campiña IV
                          </h3>
                          <div class="mt-1">
                            Prolong. Iquitos 1843, Lince
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-4">
                <div class="custom-card custom-card--2 text-left my-4">
                  <div class="custom-card__img-wrapper">
                    <img class="custom-card__img img-fluid" src="../src/img/elegir-proyecto-1.png" alt="">
                    <h3 class="custom-card__title h3 ln-h3">
                      Distrito
                    </h3>
                  </div>
                  <div>
                    <div class="px-3 py-3">
                      <div class="row align-items-center">
                        <div class="col-lg-12">
                          <h3 class="h4 custom-card__subtitle">
                              La Campiña IV
                          </h3>
                          <div class="mt-1">
                            Prolong. Iquitos 1843, Lince
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-4">
                <div class="custom-card custom-card--2 text-left my-4">
                  <div class="custom-card__img-wrapper">
                    <img class="custom-card__img img-fluid" src="../src/img/elegir-proyecto-2.png" alt="">
                    <h3 class="custom-card__title h3 ln-h3">
                      Distrito
                    </h3>
                  </div>
                  <div>
                    <div class="px-3 py-3">
                      <div class="row align-items-center">
                        <div class="col-lg-12">
                          <h3 class="h4 custom-card__subtitle">
                              La Campiña IV
                          </h3>
                          <div class="mt-1">
                            Prolong. Iquitos 1843, Lince
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-4">
                <div class="custom-card custom-card--2 text-left my-4">
                  <div class="custom-card__img-wrapper">
                    <img class="custom-card__img img-fluid" src="../src/img/elegir-proyecto-3.png" alt="">
                    <h3 class="custom-card__title h3 ln-h3">
                      Distrito
                    </h3>
                  </div>
                  <div>
                    <div class="px-3 py-3">
                      <div class="row align-items-center">
                        <div class="col-lg-12">
                          <h3 class="h4 custom-card__subtitle">
                              La Campiña IV
                          </h3>
                          <div class="mt-1">
                            Prolong. Iquitos 1843, Lince
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="paso">
          <div class="opcion-pago active">
            <h2 class="h2">
              Elige una opción de pago
            </h2>
            <div class="mt-4">
              <div class="row">
                <div class="col-lg-6">
                  <div>
                    <div class="proyectos__slick-el">
                      <div class="proyectos__slick-el-img-wrapper">
                        <img class="proyectos__slick-el-img" src="../src/img/opcion-pago.png" alt="">
                        <h3 class="proyectos__slick-el-title">
                          Lince
                        </h3>
                      </div>
                      <div class="proyectos__slick-el-body">
                        <div class="proyectos__slick-el-subtitle">
                          Lucid
                        </div>
                        <div class="proyectos__slick-el-address">
                          Prolong. Iquitos 1843, Lince
                        </div>
                        <div class="proyectos__slick-el-item-wrapper">
                          <div class="row">
                            <div class="col">
                              <div class="proyectos__slick-el-item">
                                <img src="../src/img/ico-metraje.png" alt="">
                                <span>Desde 50m2</span>
                              </div>
                            </div>
                            <div class="col">
                              <div class="proyectos__slick-el-item">
                                <img src="../src/img/ico-habitaciones.png" alt="">
                                1 a 3 dorm.
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="proyectos__slick-el-price">
                          <span>Desde:</span>
                          <span class="proyectos__slick-el-price-number">
                            S/.40.000
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="separa-btn" data-toggle="modal" data-target="#mensajeModal">
                    <div class="row">
                      <div class="col-lg-4 d-flex justify-content-center align-items-center">
                        <div>
                          <img src="../src/img/ico-brochure.png" alt="">
                        </div>
                      </div>
                      <div class="col-lg-8">
                        <div>
                          <strong>
                            Separa tu departamento
                          </strong>
                        </div>
                        <div class="color-10">
                          Separa hoy tu depa y obten beneficios especiales
                        </div>
                        <hr class="bg-color-10">
                        <h4 class="h4">
                          S/1,000
                        </h4>
                      </div>
                    </div>
                  </div>
                  <div class="separa-btn" data-toggle="modal" data-target="#mensajeModal">
                    <div class="row">
                      <div class="col-lg-4 d-flex justify-content-center align-items-center">
                        <div>
                          <img src="../src/img/ico-brochure.png" alt="">
                        </div>
                      </div>
                      <div class="col-lg-8">
                        <div>
                          <strong>
                            Paga tu inicial
                          </strong>
                        </div>
                        <div class="color-10">
                          Paga por nuestra plataforma directamente
                        </div>
                        <hr class="bg-color-10">
                        <h4 class="h4">
                          S/9,000
                        </h4>
                      </div>
                    </div>
                  </div>
                  <div class="color-10 mt-4">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit*
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="opcion-pago">
            <h2 class="h2">
              Elige una opción de pago
            </h2>
            <div class="mt-4">
              <div class="row">
                <div class="col-lg-6">
                  <div>
                    <div class="proyectos__slick-el">
                      <div class="proyectos__slick-el-img-wrapper">
                        <img class="proyectos__slick-el-img" src="../src/img/opcion-pago.png" alt="">
                        <h3 class="proyectos__slick-el-title">
                          Lince
                        </h3>
                      </div>
                      <div class="proyectos__slick-el-body">
                        <div class="proyectos__slick-el-subtitle">
                          Lucid
                        </div>
                        <div class="proyectos__slick-el-address">
                          Prolong. Iquitos 1843, Lince
                        </div>
                        <div class="proyectos__slick-el-item-wrapper">
                          <div class="row">
                            <div class="col">
                              <div class="proyectos__slick-el-item">
                                <img src="../src/img/ico-metraje.png" alt="">
                                <span>Desde 50m2</span>
                              </div>
                            </div>
                            <div class="col">
                              <div class="proyectos__slick-el-item">
                                <img src="../src/img/ico-habitaciones.png" alt="">
                                1 a 3 dorm.
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="proyectos__slick-el-price">
                          <span>Desde:</span>
                          <span class="proyectos__slick-el-price-number">
                            S/.40.000
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="separa-btn" data-toggle="modal" data-target="#mensajeModal">
                    <div class="row">
                      <div class="col-lg-4 d-flex justify-content-center align-items-center">
                        <div>
                          <img src="../src/img/ico-brochure.png" alt="">
                        </div>
                      </div>
                      <div class="col-lg-8">
                        <div>
                          <strong>
                            Separa tu departamento
                          </strong>
                        </div>
                        <div class="color-10">
                          Separa hoy tu depa y obten beneficios especiales
                        </div>
                        <hr class="bg-color-10">
                        <h4 class="h4">
                          S/1,000
                        </h4>
                      </div>
                    </div>
                  </div>
                  <div class="separa-btn" data-toggle="modal" data-target="#mensajeModal">
                    <div class="row">
                      <div class="col-lg-4 d-flex justify-content-center align-items-center">
                        <div>
                          <img src="../src/img/ico-brochure.png" alt="">
                        </div>
                      </div>
                      <div class="col-lg-8">
                        <div>
                          <strong>
                            Paga tu inicial
                          </strong>
                        </div>
                        <div class="color-10">
                          Paga por nuestra plataforma directamente
                        </div>
                        <hr class="bg-color-10">
                        <h4 class="h4">
                          S/9,000
                        </h4>
                      </div>
                    </div>
                  </div>
                  <div class="color-10 mt-4">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit*
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="opcion-pago">
            <h2 class="h2">
              Elige una opción de pago
            </h2>
            <div class="mt-4">
              <div class="row">
                <div class="col-lg-6">
                  <div>
                    <div class="proyectos__slick-el">
                      <div class="proyectos__slick-el-img-wrapper">
                        <img class="proyectos__slick-el-img" src="../src/img/opcion-pago.png" alt="">
                        <h3 class="proyectos__slick-el-title">
                          Lince
                        </h3>
                      </div>
                      <div class="proyectos__slick-el-body">
                        <div class="proyectos__slick-el-subtitle">
                          Lucid
                        </div>
                        <div class="proyectos__slick-el-address">
                          Prolong. Iquitos 1843, Lince
                        </div>
                        <div class="proyectos__slick-el-item-wrapper">
                          <div class="row">
                            <div class="col">
                              <div class="proyectos__slick-el-item">
                                <img src="../src/img/ico-metraje.png" alt="">
                                <span>Desde 50m2</span>
                              </div>
                            </div>
                            <div class="col">
                              <div class="proyectos__slick-el-item">
                                <img src="../src/img/ico-habitaciones.png" alt="">
                                1 a 3 dorm.
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="proyectos__slick-el-price">
                          <span>Desde:</span>
                          <span class="proyectos__slick-el-price-number">
                            S/.40.000
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="separa-btn" data-toggle="modal" data-target="#mensajeModal">
                    <div class="row">
                      <div class="col-lg-4 d-flex justify-content-center align-items-center">
                        <div>
                          <img src="../src/img/ico-brochure.png" alt="">
                        </div>
                      </div>
                      <div class="col-lg-8">
                        <div>
                          <strong>
                            Separa tu departamento
                          </strong>
                        </div>
                        <div class="color-10">
                          Separa hoy tu depa y obten beneficios especiales
                        </div>
                        <hr class="bg-color-10">
                        <h4 class="h4">
                          S/1,000
                        </h4>
                      </div>
                    </div>
                  </div>
                  <div class="separa-btn" data-toggle="modal" data-target="#mensajeModal">
                    <div class="row">
                      <div class="col-lg-4 d-flex justify-content-center align-items-center">
                        <div>
                          <img src="../src/img/ico-brochure.png" alt="">
                        </div>
                      </div>
                      <div class="col-lg-8">
                        <div>
                          <strong>
                            Paga tu inicial
                          </strong>
                        </div>
                        <div class="color-10">
                          Paga por nuestra plataforma directamente
                        </div>
                        <hr class="bg-color-10">
                        <h4 class="h4">
                          S/9,000
                        </h4>
                      </div>
                    </div>
                  </div>
                  <div class="color-10 mt-4">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit*
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="opcion-pago">
            <h2 class="h2">
              Elige una opción de pago
            </h2>
            <div class="mt-4">
              <div class="row">
                <div class="col-lg-6">
                  <div>
                    <div class="proyectos__slick-el">
                      <div class="proyectos__slick-el-img-wrapper">
                        <img class="proyectos__slick-el-img" src="../src/img/opcion-pago.png" alt="">
                        <h3 class="proyectos__slick-el-title">
                          Lince
                        </h3>
                      </div>
                      <div class="proyectos__slick-el-body">
                        <div class="proyectos__slick-el-subtitle">
                          Lucid
                        </div>
                        <div class="proyectos__slick-el-address">
                          Prolong. Iquitos 1843, Lince
                        </div>
                        <div class="proyectos__slick-el-item-wrapper">
                          <div class="row">
                            <div class="col">
                              <div class="proyectos__slick-el-item">
                                <img src="../src/img/ico-metraje.png" alt="">
                                <span>Desde 50m2</span>
                              </div>
                            </div>
                            <div class="col">
                              <div class="proyectos__slick-el-item">
                                <img src="../src/img/ico-habitaciones.png" alt="">
                                1 a 3 dorm.
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="proyectos__slick-el-price">
                          <span>Desde:</span>
                          <span class="proyectos__slick-el-price-number">
                            S/.40.000
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="separa-btn" data-toggle="modal" data-target="#mensajeModal">
                    <div class="row">
                      <div class="col-lg-4 d-flex justify-content-center align-items-center">
                        <div>
                          <img src="../src/img/ico-brochure.png" alt="">
                        </div>
                      </div>
                      <div class="col-lg-8">
                        <div>
                          <strong>
                            Separa tu departamento
                          </strong>
                        </div>
                        <div class="color-10">
                          Separa hoy tu depa y obten beneficios especiales
                        </div>
                        <hr class="bg-color-10">
                        <h4 class="h4">
                          S/1,000
                        </h4>
                      </div>
                    </div>
                  </div>
                  <div class="separa-btn" data-toggle="modal" data-target="#mensajeModal">
                    <div class="row">
                      <div class="col-lg-4 d-flex justify-content-center align-items-center">
                        <div>
                          <img src="../src/img/ico-brochure.png" alt="">
                        </div>
                      </div>
                      <div class="col-lg-8">
                        <div>
                          <strong>
                            Paga tu inicial
                          </strong>
                        </div>
                        <div class="color-10">
                          Paga por nuestra plataforma directamente
                        </div>
                        <hr class="bg-color-10">
                        <h4 class="h4">
                          S/9,000
                        </h4>
                      </div>
                    </div>
                  </div>
                  <div class="color-10 mt-4">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit*
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="opcion-pago">
            <h2 class="h2">
              Elige una opción de pago
            </h2>
            <div class="mt-4">
              <div class="row">
                <div class="col-lg-6">
                  <div>
                    <div class="proyectos__slick-el">
                      <div class="proyectos__slick-el-img-wrapper">
                        <img class="proyectos__slick-el-img" src="../src/img/opcion-pago.png" alt="">
                        <h3 class="proyectos__slick-el-title">
                          Lince
                        </h3>
                      </div>
                      <div class="proyectos__slick-el-body">
                        <div class="proyectos__slick-el-subtitle">
                          Lucid
                        </div>
                        <div class="proyectos__slick-el-address">
                          Prolong. Iquitos 1843, Lince
                        </div>
                        <div class="proyectos__slick-el-item-wrapper">
                          <div class="row">
                            <div class="col">
                              <div class="proyectos__slick-el-item">
                                <img src="../src/img/ico-metraje.png" alt="">
                                <span>Desde 50m2</span>
                              </div>
                            </div>
                            <div class="col">
                              <div class="proyectos__slick-el-item">
                                <img src="../src/img/ico-habitaciones.png" alt="">
                                1 a 3 dorm.
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="proyectos__slick-el-price">
                          <span>Desde:</span>
                          <span class="proyectos__slick-el-price-number">
                            S/.40.000
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="separa-btn" data-toggle="modal" data-target="#mensajeModal">
                    <div class="row">
                      <div class="col-lg-4 d-flex justify-content-center align-items-center">
                        <div>
                          <img src="../src/img/ico-brochure.png" alt="">
                        </div>
                      </div>
                      <div class="col-lg-8">
                        <div>
                          <strong>
                            Separa tu departamento
                          </strong>
                        </div>
                        <div class="color-10">
                          Separa hoy tu depa y obten beneficios especiales
                        </div>
                        <hr class="bg-color-10">
                        <h4 class="h4">
                          S/1,000
                        </h4>
                      </div>
                    </div>
                  </div>
                  <div class="separa-btn" data-toggle="modal" data-target="#mensajeModal">
                    <div class="row">
                      <div class="col-lg-4 d-flex justify-content-center align-items-center">
                        <div>
                          <img src="../src/img/ico-brochure.png" alt="">
                        </div>
                      </div>
                      <div class="col-lg-8">
                        <div>
                          <strong>
                            Paga tu inicial
                          </strong>
                        </div>
                        <div class="color-10">
                          Paga por nuestra plataforma directamente
                        </div>
                        <hr class="bg-color-10">
                        <h4 class="h4">
                          S/9,000
                        </h4>
                      </div>
                    </div>
                  </div>
                  <div class="color-10 mt-4">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit*
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="opcion-pago">
            <h2 class="h2">
              Elige una opción de pago
            </h2>
            <div class="mt-4">
              <div class="row">
                <div class="col-lg-6">
                  <div>
                    <div class="proyectos__slick-el">
                      <div class="proyectos__slick-el-img-wrapper">
                        <img class="proyectos__slick-el-img" src="../src/img/opcion-pago.png" alt="">
                        <h3 class="proyectos__slick-el-title">
                          Lince
                        </h3>
                      </div>
                      <div class="proyectos__slick-el-body">
                        <div class="proyectos__slick-el-subtitle">
                          Lucid
                        </div>
                        <div class="proyectos__slick-el-address">
                          Prolong. Iquitos 1843, Lince
                        </div>
                        <div class="proyectos__slick-el-item-wrapper">
                          <div class="row">
                            <div class="col">
                              <div class="proyectos__slick-el-item">
                                <img src="../src/img/ico-metraje.png" alt="">
                                <span>Desde 50m2</span>
                              </div>
                            </div>
                            <div class="col">
                              <div class="proyectos__slick-el-item">
                                <img src="../src/img/ico-habitaciones.png" alt="">
                                1 a 3 dorm.
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="proyectos__slick-el-price">
                          <span>Desde:</span>
                          <span class="proyectos__slick-el-price-number">
                            S/.40.000
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="separa-btn" data-toggle="modal" data-target="#mensajeModal">
                    <div class="row">
                      <div class="col-lg-4 d-flex justify-content-center align-items-center">
                        <div>
                          <img src="../src/img/ico-brochure.png" alt="">
                        </div>
                      </div>
                      <div class="col-lg-8">
                        <div>
                          <strong>
                            Separa tu departamento
                          </strong>
                        </div>
                        <div class="color-10">
                          Separa hoy tu depa y obten beneficios especiales
                        </div>
                        <hr class="bg-color-10">
                        <h4 class="h4">
                          S/1,000
                        </h4>
                      </div>
                    </div>
                  </div>
                  <div class="separa-btn" data-toggle="modal" data-target="#mensajeModal">
                    <div class="row">
                      <div class="col-lg-4 d-flex justify-content-center align-items-center">
                        <div>
                          <img src="../src/img/ico-brochure.png" alt="">
                        </div>
                      </div>
                      <div class="col-lg-8">
                        <div>
                          <strong>
                            Paga tu inicial
                          </strong>
                        </div>
                        <div class="color-10">
                          Paga por nuestra plataforma directamente
                        </div>
                        <hr class="bg-color-10">
                        <h4 class="h4">
                          S/9,000
                        </h4>
                      </div>
                    </div>
                  </div>
                  <div class="color-10 mt-4">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit*
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="paso">
          <div class="opcion-pago active">
            <h2 class="h2">
              Ingresa tus datos
            </h2>
            <div class="mt-4">
              <div class="row">
                <div class="col-lg-6">
                  <div>
                    <div class="proyectos__slick-el">
                      <div class="proyectos__slick-el-img-wrapper">
                        <img class="proyectos__slick-el-img" src="../src/img/opcion-pago.png" alt="">
                        <h3 class="proyectos__slick-el-title">
                          Lince
                        </h3>
                      </div>
                      <div class="proyectos__slick-el-body">
                        <div class="proyectos__slick-el-subtitle">
                          Lucid
                        </div>
                        <div class="proyectos__slick-el-address">
                          Prolong. Iquitos 1843, Lince
                        </div>
                        <div class="proyectos__slick-el-item-wrapper">
                          <div class="row">
                            <div class="col">
                              <div class="proyectos__slick-el-item">
                                <span>
                                  1 domitorio / 1 baño
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="proyectos__slick-el-price">
                          <span>
                            Área común 1 / Área común 2 <br>
                            Área común 3 / Área común 4
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="row">
                    <div class="col-lg-6">
                      <input placeholder="Nombre *" type="text" class="form-control">
                    </div>
                    <div class="col-lg-6">
                      <input placeholder="Apellido *" type="text" class="form-control">
                    </div>
                    <div class="col-lg-6">
                      <input placeholder="DNI" type="text" class="form-control">
                    </div>
                    <div class="col-lg-6">
                      <input placeholder="Teléfono" type="text" class="form-control">
                    </div>
                    <div class="col-lg-6">
                      <input placeholder="Correo" type="text" class="form-control">
                    </div>
                    <div class="col-lg-6">
                      <div class="select-box">
                        <div class="select-box__current" tabindex="1">
                            <div class="select-box__value"><input class="select-box__input" type="radio" id="cita00" value="1" name="cita0" checked />
                                <p class="select-box__input-text">N° de departamentos </p>
                            </div>
                            <div class="select-box__value"><input class="select-box__input" type="radio" id="cita01" value="2" name="cita0" />
                                <p class="select-box__input-text">cita 1</p>
                            </div>
                            <div class="select-box__value"><input class="select-box__input" type="radio" id="cita02" value="3" name="cita0" />
                                <p class="select-box__input-text">cita 2</p>
                            </div>
                            <div class="select-box__value"><input class="select-box__input" type="radio" id="cita03" value="4" name="cita0" />
                                <p class="select-box__input-text">cita 3</p>
                            </div>
                            <div class="select-box__value"><input class="select-box__input" type="radio" id="cita04" value="5" name="cita0" />
                                <p class="select-box__input-text">cita 4</p>
                            </div><img class="select-box__icon" src="../src/img/select-arrow.png" alt="Arrow Icon" aria-hidden="true" />
                        </div>
                        <ul class="select-box__list">
                            <li><label class="select-box__option" for="cita00" aria-hidden="aria-hidden">Todos</label></li>
                            <li><label class="select-box__option" for="cita01" aria-hidden="aria-hidden">cita 1</label></li>
                            <li><label class="select-box__option" for="cita02" aria-hidden="aria-hidden">cita 2</label></li>
                            <li><label class="select-box__option" for="cita03" aria-hidden="aria-hidden">cita 3</label></li>
                            <li><label class="select-box__option" for="cita04" aria-hidden="aria-hidden">cita 4</label></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-12">
                      <div class="d-block text-left my-2 custom-control custom-checkbox ">
                        <input type="checkbox" class="custom-control-input" id="cbox-terminos">
                        <label for="cbox-terminos" class="custom-control-label ">Aceptar términos y condiciones</label>
                      </div>
                    </div>
                    <div class="col-lg-12">
                      <div class="d-block text-left my-2 custom-control custom-checkbox ">
                        <input type="checkbox" class="custom-control-input" id="cbox-autorizo">
                        <label for="cbox-autorizo" class="custom-control-label ">Autorizo a Arteco para que relice las actividades descritas en la <a class="color-3" href="#">Política de Privacidad</a></label>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-12 text-center pt-4">
                      <button class="butn butn-1 enviar-form">Siguiente</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="opcion-pago">
            <h2 class="h2">
              Ingresa tus datos
            </h2>
            <div class="mt-4">
              <div class="row">
                <div class="col-lg-6">
                  <div>
                    <div class="proyectos__slick-el">
                      <div class="proyectos__slick-el-img-wrapper">
                        <img class="proyectos__slick-el-img" src="../src/img/opcion-pago.png" alt="">
                        <h3 class="proyectos__slick-el-title">
                          Lince
                        </h3>
                      </div>
                      <div class="proyectos__slick-el-body">
                        <div class="proyectos__slick-el-subtitle">
                          Lucid
                        </div>
                        <div class="proyectos__slick-el-address">
                          Prolong. Iquitos 1843, Lince
                        </div>
                        <div class="proyectos__slick-el-item-wrapper">
                          <div class="row">
                            <div class="col">
                              <div class="proyectos__slick-el-item">
                                <span>
                                  1 domitorio / 1 baño
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="proyectos__slick-el-price">
                          <span>
                            Área común 1 / Área común 2 <br>
                            Área común 3 / Área común 4
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="row">
                    <div class="col-lg-6">
                      <input placeholder="Nombre *" type="text" class="form-control">
                    </div>
                    <div class="col-lg-6">
                      <input placeholder="Apellido *" type="text" class="form-control">
                    </div>
                    <div class="col-lg-6">
                      <input placeholder="DNI" type="text" class="form-control">
                    </div>
                    <div class="col-lg-6">
                      <input placeholder="Teléfono" type="text" class="form-control">
                    </div>
                    <div class="col-lg-6">
                      <input placeholder="Correo" type="text" class="form-control">
                    </div>
                    <div class="col-lg-6">
                      <div class="select-box">
                        <div class="select-box__current" tabindex="1">
                            <div class="select-box__value"><input class="select-box__input" type="radio" id="cita10" value="1" name="cita1" checked />
                                <p class="select-box__input-text">N° de departamentos </p>
                            </div>
                            <div class="select-box__value"><input class="select-box__input" type="radio" id="cita11" value="2" name="cita1" />
                                <p class="select-box__input-text">cita 1</p>
                            </div>
                            <div class="select-box__value"><input class="select-box__input" type="radio" id="cita12" value="3" name="cita1" />
                                <p class="select-box__input-text">cita 2</p>
                            </div>
                            <div class="select-box__value"><input class="select-box__input" type="radio" id="cita13" value="4" name="cita1" />
                                <p class="select-box__input-text">cita 3</p>
                            </div>
                            <div class="select-box__value"><input class="select-box__input" type="radio" id="cita14" value="5" name="cita1" />
                                <p class="select-box__input-text">cita 4</p>
                            </div><img class="select-box__icon" src="../src/img/select-arrow.png" alt="Arrow Icon" aria-hidden="true" />
                        </div>
                        <ul class="select-box__list">
                            <li><label class="select-box__option" for="cita10" aria-hidden="aria-hidden">Todos</label></li>
                            <li><label class="select-box__option" for="cita11" aria-hidden="aria-hidden">cita 1</label></li>
                            <li><label class="select-box__option" for="cita12" aria-hidden="aria-hidden">cita 2</label></li>
                            <li><label class="select-box__option" for="cita13" aria-hidden="aria-hidden">cita 3</label></li>
                            <li><label class="select-box__option" for="cita14" aria-hidden="aria-hidden">cita 4</label></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-12">
                      <div class="d-block text-left my-2 custom-control custom-checkbox ">
                        <input type="checkbox" class="custom-control-input" id="cbox-terminos">
                        <label for="cbox-terminos" class="custom-control-label ">Aceptar términos y condiciones</label>
                      </div>
                    </div>
                    <div class="col-lg-12">
                      <div class="d-block text-left my-2 custom-control custom-checkbox ">
                        <input type="checkbox" class="custom-control-input" id="cbox-autorizo">
                        <label for="cbox-autorizo" class="custom-control-label ">Autorizo a Arteco para que relice las actividades descritas en la <a class="color-3" href="#">Política de Privacidad</a></label>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-12 text-center pt-4">
                      <button class="butn butn-1 enviar-form">Siguiente</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="opcion-pago">
            <h2 class="h2">
              Ingresa tus datos
            </h2>
            <div class="mt-4">
              <div class="row">
                <div class="col-lg-6">
                  <div>
                    <div class="proyectos__slick-el">
                      <div class="proyectos__slick-el-img-wrapper">
                        <img class="proyectos__slick-el-img" src="../src/img/opcion-pago.png" alt="">
                        <h3 class="proyectos__slick-el-title">
                          Lince
                        </h3>
                      </div>
                      <div class="proyectos__slick-el-body">
                        <div class="proyectos__slick-el-subtitle">
                          Lucid
                        </div>
                        <div class="proyectos__slick-el-address">
                          Prolong. Iquitos 1843, Lince
                        </div>
                        <div class="proyectos__slick-el-item-wrapper">
                          <div class="row">
                            <div class="col">
                              <div class="proyectos__slick-el-item">
                                <span>
                                  1 domitorio / 1 baño
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="proyectos__slick-el-price">
                          <span>
                            Área común 1 / Área común 2 <br>
                            Área común 3 / Área común 4
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="row">
                    <div class="col-lg-6">
                      <input placeholder="Nombre *" type="text" class="form-control">
                    </div>
                    <div class="col-lg-6">
                      <input placeholder="Apellido *" type="text" class="form-control">
                    </div>
                    <div class="col-lg-6">
                      <input placeholder="DNI" type="text" class="form-control">
                    </div>
                    <div class="col-lg-6">
                      <input placeholder="Teléfono" type="text" class="form-control">
                    </div>
                    <div class="col-lg-6">
                      <input placeholder="Correo" type="text" class="form-control">
                    </div>
                    <div class="col-lg-6">
                      <div class="select-box">
                        <div class="select-box__current" tabindex="1">
                            <div class="select-box__value"><input class="select-box__input" type="radio" id="cita20" value="1" name="cita2" checked />
                                <p class="select-box__input-text">N° de departamentos </p>
                            </div>
                            <div class="select-box__value"><input class="select-box__input" type="radio" id="cita21" value="2" name="cita2" />
                                <p class="select-box__input-text">cita 1</p>
                            </div>
                            <div class="select-box__value"><input class="select-box__input" type="radio" id="cita22" value="3" name="cita2" />
                                <p class="select-box__input-text">cita 2</p>
                            </div>
                            <div class="select-box__value"><input class="select-box__input" type="radio" id="cita23" value="4" name="cita2" />
                                <p class="select-box__input-text">cita 3</p>
                            </div>
                            <div class="select-box__value"><input class="select-box__input" type="radio" id="cita24" value="5" name="cita2" />
                                <p class="select-box__input-text">cita 4</p>
                            </div><img class="select-box__icon" src="../src/img/select-arrow.png" alt="Arrow Icon" aria-hidden="true" />
                        </div>
                        <ul class="select-box__list">
                            <li><label class="select-box__option" for="cita0" aria-hidden="aria-hidden">Todos</label></li>
                            <li><label class="select-box__option" for="cita1" aria-hidden="aria-hidden">cita 1</label></li>
                            <li><label class="select-box__option" for="cita2" aria-hidden="aria-hidden">cita 2</label></li>
                            <li><label class="select-box__option" for="cita3" aria-hidden="aria-hidden">cita 3</label></li>
                            <li><label class="select-box__option" for="cita4" aria-hidden="aria-hidden">cita 4</label></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-12">
                      <div class="d-block text-left my-2 custom-control custom-checkbox ">
                        <input type="checkbox" class="custom-control-input" id="cbox-terminos">
                        <label for="cbox-terminos" class="custom-control-label ">Aceptar términos y condiciones</label>
                      </div>
                    </div>
                    <div class="col-lg-12">
                      <div class="d-block text-left my-2 custom-control custom-checkbox ">
                        <input type="checkbox" class="custom-control-input" id="cbox-autorizo">
                        <label for="cbox-autorizo" class="custom-control-label ">Autorizo a Arteco para que relice las actividades descritas en la <a class="color-3" href="#">Política de Privacidad</a></label>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-12 text-center pt-4">
                      <button class="butn butn-1 enviar-form">Siguiente</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="opcion-pago">
            <h2 class="h2">
              Ingresa tus datos
            </h2>
            <div class="mt-4">
              <div class="row">
                <div class="col-lg-6">
                  <div>
                    <div class="proyectos__slick-el">
                      <div class="proyectos__slick-el-img-wrapper">
                        <img class="proyectos__slick-el-img" src="../src/img/opcion-pago.png" alt="">
                        <h3 class="proyectos__slick-el-title">
                          Lince
                        </h3>
                      </div>
                      <div class="proyectos__slick-el-body">
                        <div class="proyectos__slick-el-subtitle">
                          Lucid
                        </div>
                        <div class="proyectos__slick-el-address">
                          Prolong. Iquitos 1843, Lince
                        </div>
                        <div class="proyectos__slick-el-item-wrapper">
                          <div class="row">
                            <div class="col">
                              <div class="proyectos__slick-el-item">
                                <span>
                                  1 domitorio / 1 baño
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="proyectos__slick-el-price">
                          <span>
                            Área común 1 / Área común 2 <br>
                            Área común 3 / Área común 4
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="row">
                    <div class="col-lg-6">
                      <input placeholder="Nombre *" type="text" class="form-control">
                    </div>
                    <div class="col-lg-6">
                      <input placeholder="Apellido *" type="text" class="form-control">
                    </div>
                    <div class="col-lg-6">
                      <input placeholder="DNI" type="text" class="form-control">
                    </div>
                    <div class="col-lg-6">
                      <input placeholder="Teléfono" type="text" class="form-control">
                    </div>
                    <div class="col-lg-6">
                      <input placeholder="Correo" type="text" class="form-control">
                    </div>
                    <div class="col-lg-6">
                      <div class="select-box">
                        <div class="select-box__current" tabindex="1">
                            <div class="select-box__value"><input class="select-box__input" type="radio" id="cita30" value="1" name="cita3" checked />
                                <p class="select-box__input-text">N° de departamentos </p>
                            </div>
                            <div class="select-box__value"><input class="select-box__input" type="radio" id="cita31" value="2" name="cita3" />
                                <p class="select-box__input-text">cita 1</p>
                            </div>
                            <div class="select-box__value"><input class="select-box__input" type="radio" id="cita32" value="3" name="cita3" />
                                <p class="select-box__input-text">cita 2</p>
                            </div>
                            <div class="select-box__value"><input class="select-box__input" type="radio" id="cita33" value="4" name="cita3" />
                                <p class="select-box__input-text">cita 3</p>
                            </div>
                            <div class="select-box__value"><input class="select-box__input" type="radio" id="cita34" value="5" name="cita3" />
                                <p class="select-box__input-text">cita 4</p>
                            </div><img class="select-box__icon" src="../src/img/select-arrow.png" alt="Arrow Icon" aria-hidden="true" />
                        </div>
                        <ul class="select-box__list">
                            <li><label class="select-box__option" for="cita30" aria-hidden="aria-hidden">Todos</label></li>
                            <li><label class="select-box__option" for="cita31" aria-hidden="aria-hidden">cita 1</label></li>
                            <li><label class="select-box__option" for="cita32" aria-hidden="aria-hidden">cita 2</label></li>
                            <li><label class="select-box__option" for="cita33" aria-hidden="aria-hidden">cita 3</label></li>
                            <li><label class="select-box__option" for="cita34" aria-hidden="aria-hidden">cita 4</label></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-12">
                      <div class="d-block text-left my-2 custom-control custom-checkbox ">
                        <input type="checkbox" class="custom-control-input" id="cbox-terminos">
                        <label for="cbox-terminos" class="custom-control-label ">Aceptar términos y condiciones</label>
                      </div>
                    </div>
                    <div class="col-lg-12">
                      <div class="d-block text-left my-2 custom-control custom-checkbox ">
                        <input type="checkbox" class="custom-control-input" id="cbox-autorizo">
                        <label for="cbox-autorizo" class="custom-control-label ">Autorizo a Arteco para que relice las actividades descritas en la <a class="color-3" href="#">Política de Privacidad</a></label>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-12 text-center pt-4">
                      <button class="butn butn-1 enviar-form">Siguiente</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="opcion-pago">
            <h2 class="h2">
              Ingresa tus datos
            </h2>
            <div class="mt-4">
              <div class="row">
                <div class="col-lg-6">
                  <div>
                    <div class="proyectos__slick-el">
                      <div class="proyectos__slick-el-img-wrapper">
                        <img class="proyectos__slick-el-img" src="../src/img/opcion-pago.png" alt="">
                        <h3 class="proyectos__slick-el-title">
                          Lince
                        </h3>
                      </div>
                      <div class="proyectos__slick-el-body">
                        <div class="proyectos__slick-el-subtitle">
                          Lucid
                        </div>
                        <div class="proyectos__slick-el-address">
                          Prolong. Iquitos 1843, Lince
                        </div>
                        <div class="proyectos__slick-el-item-wrapper">
                          <div class="row">
                            <div class="col">
                              <div class="proyectos__slick-el-item">
                                <span>
                                  1 domitorio / 1 baño
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="proyectos__slick-el-price">
                          <span>
                            Área común 1 / Área común 2 <br>
                            Área común 3 / Área común 4
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="row">
                    <div class="col-lg-6">
                      <input placeholder="Nombre *" type="text" class="form-control">
                    </div>
                    <div class="col-lg-6">
                      <input placeholder="Apellido *" type="text" class="form-control">
                    </div>
                    <div class="col-lg-6">
                      <input placeholder="DNI" type="text" class="form-control">
                    </div>
                    <div class="col-lg-6">
                      <input placeholder="Teléfono" type="text" class="form-control">
                    </div>
                    <div class="col-lg-6">
                      <input placeholder="Correo" type="text" class="form-control">
                    </div>
                    <div class="col-lg-6">
                      <div class="select-box">
                        <div class="select-box__current" tabindex="1">
                            <div class="select-box__value"><input class="select-box__input" type="radio" id="cita40" value="1" name="cita4" checked />
                                <p class="select-box__input-text">N° de departamentos </p>
                            </div>
                            <div class="select-box__value"><input class="select-box__input" type="radio" id="cita41" value="2" name="cita4" />
                                <p class="select-box__input-text">cita 1</p>
                            </div>
                            <div class="select-box__value"><input class="select-box__input" type="radio" id="cita42" value="3" name="cita4" />
                                <p class="select-box__input-text">cita 2</p>
                            </div>
                            <div class="select-box__value"><input class="select-box__input" type="radio" id="cita43" value="4" name="cita4" />
                                <p class="select-box__input-text">cita 3</p>
                            </div>
                            <div class="select-box__value"><input class="select-box__input" type="radio" id="cita44" value="5" name="cita4" />
                                <p class="select-box__input-text">cita 4</p>
                            </div><img class="select-box__icon" src="../src/img/select-arrow.png" alt="Arrow Icon" aria-hidden="true" />
                        </div>
                        <ul class="select-box__list">
                            <li><label class="select-box__option" for="cita40" aria-hidden="aria-hidden">Todos</label></li>
                            <li><label class="select-box__option" for="cita41" aria-hidden="aria-hidden">cita 1</label></li>
                            <li><label class="select-box__option" for="cita42" aria-hidden="aria-hidden">cita 2</label></li>
                            <li><label class="select-box__option" for="cita43" aria-hidden="aria-hidden">cita 3</label></li>
                            <li><label class="select-box__option" for="cita44" aria-hidden="aria-hidden">cita 4</label></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-12">
                      <div class="d-block text-left my-2 custom-control custom-checkbox ">
                        <input type="checkbox" class="custom-control-input" id="cbox-terminos">
                        <label for="cbox-terminos" class="custom-control-label ">Aceptar términos y condiciones</label>
                      </div>
                    </div>
                    <div class="col-lg-12">
                      <div class="d-block text-left my-2 custom-control custom-checkbox ">
                        <input type="checkbox" class="custom-control-input" id="cbox-autorizo">
                        <label for="cbox-autorizo" class="custom-control-label ">Autorizo a Arteco para que relice las actividades descritas en la <a class="color-3" href="#">Política de Privacidad</a></label>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-12 text-center pt-4">
                      <button class="butn butn-1 enviar-form">Siguiente</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="opcion-pago">
            <h2 class="h2">
              Ingresa tus datos
            </h2>
            <div class="mt-4">
              <div class="row">
                <div class="col-lg-6">
                  <div>
                    <div class="proyectos__slick-el">
                      <div class="proyectos__slick-el-img-wrapper">
                        <img class="proyectos__slick-el-img" src="../src/img/opcion-pago.png" alt="">
                        <h3 class="proyectos__slick-el-title">
                          Lince
                        </h3>
                      </div>
                      <div class="proyectos__slick-el-body">
                        <div class="proyectos__slick-el-subtitle">
                          Lucid
                        </div>
                        <div class="proyectos__slick-el-address">
                          Prolong. Iquitos 1843, Lince
                        </div>
                        <div class="proyectos__slick-el-item-wrapper">
                          <div class="row">
                            <div class="col">
                              <div class="proyectos__slick-el-item">
                                <span>
                                  1 domitorio / 1 baño
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="proyectos__slick-el-price">
                          <span>
                            Área común 1 / Área común 2 <br>
                            Área común 3 / Área común 4
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="row">
                    <div class="col-lg-6">
                      <input placeholder="Nombre *" type="text" class="form-control">
                    </div>
                    <div class="col-lg-6">
                      <input placeholder="Apellido *" type="text" class="form-control">
                    </div>
                    <div class="col-lg-6">
                      <input placeholder="DNI" type="text" class="form-control">
                    </div>
                    <div class="col-lg-6">
                      <input placeholder="Teléfono" type="text" class="form-control">
                    </div>
                    <div class="col-lg-6">
                      <input placeholder="Correo" type="text" class="form-control">
                    </div>
                    <div class="col-lg-6">
                      <div class="select-box">
                        <div class="select-box__current" tabindex="1">
                            <div class="select-box__value"><input class="select-box__input" type="radio" id="cita50" value="1" name="cita5" checked />
                                <p class="select-box__input-text">N° de departamentos </p>
                            </div>
                            <div class="select-box__value"><input class="select-box__input" type="radio" id="cita51" value="2" name="cita5" />
                                <p class="select-box__input-text">cita 1</p>
                            </div>
                            <div class="select-box__value"><input class="select-box__input" type="radio" id="cita52" value="3" name="cita5" />
                                <p class="select-box__input-text">cita 2</p>
                            </div>
                            <div class="select-box__value"><input class="select-box__input" type="radio" id="cita53" value="4" name="cita5" />
                                <p class="select-box__input-text">cita 3</p>
                            </div>
                            <div class="select-box__value"><input class="select-box__input" type="radio" id="cita54" value="5" name="cita5" />
                                <p class="select-box__input-text">cita 4</p>
                            </div><img class="select-box__icon" src="../src/img/select-arrow.png" alt="Arrow Icon" aria-hidden="true" />
                        </div>
                        <ul class="select-box__list">
                            <li><label class="select-box__option" for="cita50" aria-hidden="aria-hidden">Todos</label></li>
                            <li><label class="select-box__option" for="cita51" aria-hidden="aria-hidden">cita 1</label></li>
                            <li><label class="select-box__option" for="cita52" aria-hidden="aria-hidden">cita 2</label></li>
                            <li><label class="select-box__option" for="cita53" aria-hidden="aria-hidden">cita 3</label></li>
                            <li><label class="select-box__option" for="cita54" aria-hidden="aria-hidden">cita 4</label></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-12">
                      <div class="d-block text-left my-2 custom-control custom-checkbox ">
                        <input type="checkbox" class="custom-control-input" id="cbox-terminos">
                        <label for="cbox-terminos" class="custom-control-label ">Aceptar términos y condiciones</label>
                      </div>
                    </div>
                    <div class="col-lg-12">
                      <div class="d-block text-left my-2 custom-control custom-checkbox ">
                        <input type="checkbox" class="custom-control-input" id="cbox-autorizo">
                        <label for="cbox-autorizo" class="custom-control-label ">Autorizo a Arteco para que relice las actividades descritas en la <a class="color-3" href="#">Política de Privacidad</a></label>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-12 text-center pt-4">
                      <button class="butn butn-1 enviar-form">Siguiente</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="paso">
          <div class="opcion-pago active">
            <h2 class="h2">
              Realiza el pago
            </h2>
            <div class="mt-4">
              <div class="row">
                <div class="col-lg-6">
                  <div>
                    <div class="proyectos__slick-el">
                      <div class="proyectos__slick-el-img-wrapper">
                        <img class="proyectos__slick-el-img" src="../src/img/opcion-pago.png" alt="">
                        <h3 class="proyectos__slick-el-title">
                          Lince
                        </h3>
                      </div>
                      <div class="proyectos__slick-el-body">
                        <div class="proyectos__slick-el-subtitle">
                          Lucid
                        </div>
                        <div class="proyectos__slick-el-address">
                          Prolong. Iquitos 1843, Lince
                        </div>
                        <div class="proyectos__slick-el-item-wrapper">
                          <div class="row">
                            <div class="col">
                              <div class="proyectos__slick-el-item">
                                <span>
                                  1 domitorio / 1 baño
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="proyectos__slick-el-price">
                          <span>
                            Área común 1 / Área común 2 <br>
                            Área común 3 / Área común 4
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="color-3">
                    <div>
                      <strong>
                        Orden N°
                      </strong>
                      <span>
                        000 00 000
                      </span>
                    </div>
                    <div class="mt-4">
                      <strong>
                        Información del cliente
                      </strong>
                    </div>

                    <div class="mt-3">
                      <div class="mb-2">
                        Nombre <strong></strong>
                      </div>
                      <div class="mb-2">
                        Apellido <strong></strong>
                      </div>
                      <div class="mb-2">
                        Correo <strong></strong>
                      </div>
                      <div class="mb-2">
                        Teléfono <strong></strong>
                      </div>
                      <div class="mb-2">
                        DNI <strong></strong>
                      </div>
                      <div class="mb-2">
                        Dirección <strong></strong>
                      </div>
                      <div class="mb-2">
                        Domitorios <strong></strong>
                      </div>
                      <div class="mb-2">
                        Piso <strong></strong>
                      </div>

                      <div class="row">
                        <div class="col-lg-6">
                          <div class="mt-4">
                            <strong>
                              Item
                            </strong>
                          </div>
                          <div class="mt-2">
                            Nombre del proyecto
                          </div>
                        </div>
                        <div class="col-lg-6 text-right">
                          <div class="mt-4">
                            <strong>
                              Importe
                            </strong>
                          </div>
                          <div class="color-b mt-2">
                            <strong>
                              s/1,000
                            </strong>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-12 text-center pt-4">
                      <button class="butn butn-1 pagar">Pagar</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="opcion-pago">
            <h2 class="h2">
              Realiza el pago
            </h2>
            <div class="mt-4">
              <div class="row">
                <div class="col-lg-6">
                  <div>
                    <div class="proyectos__slick-el">
                      <div class="proyectos__slick-el-img-wrapper">
                        <img class="proyectos__slick-el-img" src="../src/img/opcion-pago.png" alt="">
                        <h3 class="proyectos__slick-el-title">
                          Lince
                        </h3>
                      </div>
                      <div class="proyectos__slick-el-body">
                        <div class="proyectos__slick-el-subtitle">
                          Lucid
                        </div>
                        <div class="proyectos__slick-el-address">
                          Prolong. Iquitos 1843, Lince
                        </div>
                        <div class="proyectos__slick-el-item-wrapper">
                          <div class="row">
                            <div class="col">
                              <div class="proyectos__slick-el-item">
                                <span>
                                  1 domitorio / 1 baño
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="proyectos__slick-el-price">
                          <span>
                            Área común 1 / Área común 2 <br>
                            Área común 3 / Área común 4
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="color-3">
                    <div>
                      <strong>
                        Orden N°
                      </strong>
                      <span>
                        000 00 000
                      </span>
                    </div>
                    <div class="mt-4">
                      <strong>
                        Información del cliente
                      </strong>
                    </div>

                    <div class="mt-3">
                      <div class="mb-2">
                        Nombre <strong></strong>
                      </div>
                      <div class="mb-2">
                        Apellido <strong></strong>
                      </div>
                      <div class="mb-2">
                        Correo <strong></strong>
                      </div>
                      <div class="mb-2">
                        Teléfono <strong></strong>
                      </div>
                      <div class="mb-2">
                        DNI <strong></strong>
                      </div>
                      <div class="mb-2">
                        Dirección <strong></strong>
                      </div>
                      <div class="mb-2">
                        Domitorios <strong></strong>
                      </div>
                      <div class="mb-2">
                        Piso <strong></strong>
                      </div>

                      <div class="row">
                        <div class="col-lg-6">
                          <div class="mt-4">
                            <strong>
                              Item
                            </strong>
                          </div>
                          <div class="mt-2">
                            Nombre del proyecto
                          </div>
                        </div>
                        <div class="col-lg-6 text-right">
                          <div class="mt-4">
                            <strong>
                              Importe
                            </strong>
                          </div>
                          <div class="color-b mt-2">
                            <strong>
                              s/1,000
                            </strong>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-12 text-center pt-4">
                      <button class="butn butn-1 pagar">Pagar</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="opcion-pago">
            <h2 class="h2">
              Realiza el pago
            </h2>
            <div class="mt-4">
              <div class="row">
                <div class="col-lg-6">
                  <div>
                    <div class="proyectos__slick-el">
                      <div class="proyectos__slick-el-img-wrapper">
                        <img class="proyectos__slick-el-img" src="../src/img/opcion-pago.png" alt="">
                        <h3 class="proyectos__slick-el-title">
                          Lince
                        </h3>
                      </div>
                      <div class="proyectos__slick-el-body">
                        <div class="proyectos__slick-el-subtitle">
                          Lucid
                        </div>
                        <div class="proyectos__slick-el-address">
                          Prolong. Iquitos 1843, Lince
                        </div>
                        <div class="proyectos__slick-el-item-wrapper">
                          <div class="row">
                            <div class="col">
                              <div class="proyectos__slick-el-item">
                                <span>
                                  1 domitorio / 1 baño
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="proyectos__slick-el-price">
                          <span>
                            Área común 1 / Área común 2 <br>
                            Área común 3 / Área común 4
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="color-3">
                    <div>
                      <strong>
                        Orden N°
                      </strong>
                      <span>
                        000 00 000
                      </span>
                    </div>
                    <div class="mt-4">
                      <strong>
                        Información del cliente
                      </strong>
                    </div>

                    <div class="mt-3">
                      <div class="mb-2">
                        Nombre <strong></strong>
                      </div>
                      <div class="mb-2">
                        Apellido <strong></strong>
                      </div>
                      <div class="mb-2">
                        Correo <strong></strong>
                      </div>
                      <div class="mb-2">
                        Teléfono <strong></strong>
                      </div>
                      <div class="mb-2">
                        DNI <strong></strong>
                      </div>
                      <div class="mb-2">
                        Dirección <strong></strong>
                      </div>
                      <div class="mb-2">
                        Domitorios <strong></strong>
                      </div>
                      <div class="mb-2">
                        Piso <strong></strong>
                      </div>

                      <div class="row">
                        <div class="col-lg-6">
                          <div class="mt-4">
                            <strong>
                              Item
                            </strong>
                          </div>
                          <div class="mt-2">
                            Nombre del proyecto
                          </div>
                        </div>
                        <div class="col-lg-6 text-right">
                          <div class="mt-4">
                            <strong>
                              Importe
                            </strong>
                          </div>
                          <div class="color-b mt-2">
                            <strong>
                              s/1,000
                            </strong>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-12 text-center pt-4">
                      <button class="butn butn-1 pagar">Pagar</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="opcion-pago">
            <h2 class="h2">
              Realiza el pago
            </h2>
            <div class="mt-4">
              <div class="row">
                <div class="col-lg-6">
                  <div>
                    <div class="proyectos__slick-el">
                      <div class="proyectos__slick-el-img-wrapper">
                        <img class="proyectos__slick-el-img" src="../src/img/opcion-pago.png" alt="">
                        <h3 class="proyectos__slick-el-title">
                          Lince
                        </h3>
                      </div>
                      <div class="proyectos__slick-el-body">
                        <div class="proyectos__slick-el-subtitle">
                          Lucid
                        </div>
                        <div class="proyectos__slick-el-address">
                          Prolong. Iquitos 1843, Lince
                        </div>
                        <div class="proyectos__slick-el-item-wrapper">
                          <div class="row">
                            <div class="col">
                              <div class="proyectos__slick-el-item">
                                <span>
                                  1 domitorio / 1 baño
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="proyectos__slick-el-price">
                          <span>
                            Área común 1 / Área común 2 <br>
                            Área común 3 / Área común 4
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="color-3">
                    <div>
                      <strong>
                        Orden N°
                      </strong>
                      <span>
                        000 00 000
                      </span>
                    </div>
                    <div class="mt-4">
                      <strong>
                        Información del cliente
                      </strong>
                    </div>

                    <div class="mt-3">
                      <div class="mb-2">
                        Nombre <strong></strong>
                      </div>
                      <div class="mb-2">
                        Apellido <strong></strong>
                      </div>
                      <div class="mb-2">
                        Correo <strong></strong>
                      </div>
                      <div class="mb-2">
                        Teléfono <strong></strong>
                      </div>
                      <div class="mb-2">
                        DNI <strong></strong>
                      </div>
                      <div class="mb-2">
                        Dirección <strong></strong>
                      </div>
                      <div class="mb-2">
                        Domitorios <strong></strong>
                      </div>
                      <div class="mb-2">
                        Piso <strong></strong>
                      </div>

                      <div class="row">
                        <div class="col-lg-6">
                          <div class="mt-4">
                            <strong>
                              Item
                            </strong>
                          </div>
                          <div class="mt-2">
                            Nombre del proyecto
                          </div>
                        </div>
                        <div class="col-lg-6 text-right">
                          <div class="mt-4">
                            <strong>
                              Importe
                            </strong>
                          </div>
                          <div class="color-b mt-2">
                            <strong>
                              s/1,000
                            </strong>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-12 text-center pt-4">
                      <button class="butn butn-1 pagar">Pagar</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="opcion-pago">
            <h2 class="h2">
              Realiza el pago
            </h2>
            <div class="mt-4">
              <div class="row">
                <div class="col-lg-6">
                  <div>
                    <div class="proyectos__slick-el">
                      <div class="proyectos__slick-el-img-wrapper">
                        <img class="proyectos__slick-el-img" src="../src/img/opcion-pago.png" alt="">
                        <h3 class="proyectos__slick-el-title">
                          Lince
                        </h3>
                      </div>
                      <div class="proyectos__slick-el-body">
                        <div class="proyectos__slick-el-subtitle">
                          Lucid
                        </div>
                        <div class="proyectos__slick-el-address">
                          Prolong. Iquitos 1843, Lince
                        </div>
                        <div class="proyectos__slick-el-item-wrapper">
                          <div class="row">
                            <div class="col">
                              <div class="proyectos__slick-el-item">
                                <span>
                                  1 domitorio / 1 baño
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="proyectos__slick-el-price">
                          <span>
                            Área común 1 / Área común 2 <br>
                            Área común 3 / Área común 4
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="color-3">
                    <div>
                      <strong>
                        Orden N°
                      </strong>
                      <span>
                        000 00 000
                      </span>
                    </div>
                    <div class="mt-4">
                      <strong>
                        Información del cliente
                      </strong>
                    </div>

                    <div class="mt-3">
                      <div class="mb-2">
                        Nombre <strong></strong>
                      </div>
                      <div class="mb-2">
                        Apellido <strong></strong>
                      </div>
                      <div class="mb-2">
                        Correo <strong></strong>
                      </div>
                      <div class="mb-2">
                        Teléfono <strong></strong>
                      </div>
                      <div class="mb-2">
                        DNI <strong></strong>
                      </div>
                      <div class="mb-2">
                        Dirección <strong></strong>
                      </div>
                      <div class="mb-2">
                        Domitorios <strong></strong>
                      </div>
                      <div class="mb-2">
                        Piso <strong></strong>
                      </div>

                      <div class="row">
                        <div class="col-lg-6">
                          <div class="mt-4">
                            <strong>
                              Item
                            </strong>
                          </div>
                          <div class="mt-2">
                            Nombre del proyecto
                          </div>
                        </div>
                        <div class="col-lg-6 text-right">
                          <div class="mt-4">
                            <strong>
                              Importe
                            </strong>
                          </div>
                          <div class="color-b mt-2">
                            <strong>
                              s/1,000
                            </strong>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-12 text-center pt-4">
                      <button class="butn butn-1 pagar">Pagar</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="opcion-pago">
            <h2 class="h2">
              Realiza el pago
            </h2>
            <div class="mt-4">
              <div class="row">
                <div class="col-lg-6">
                  <div>
                    <div class="proyectos__slick-el">
                      <div class="proyectos__slick-el-img-wrapper">
                        <img class="proyectos__slick-el-img" src="../src/img/opcion-pago.png" alt="">
                        <h3 class="proyectos__slick-el-title">
                          Lince
                        </h3>
                      </div>
                      <div class="proyectos__slick-el-body">
                        <div class="proyectos__slick-el-subtitle">
                          Lucid
                        </div>
                        <div class="proyectos__slick-el-address">
                          Prolong. Iquitos 1843, Lince
                        </div>
                        <div class="proyectos__slick-el-item-wrapper">
                          <div class="row">
                            <div class="col">
                              <div class="proyectos__slick-el-item">
                                <span>
                                  1 domitorio / 1 baño
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="proyectos__slick-el-price">
                          <span>
                            Área común 1 / Área común 2 <br>
                            Área común 3 / Área común 4
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="color-3">
                    <div>
                      <strong>
                        Orden N°
                      </strong>
                      <span>
                        000 00 000
                      </span>
                    </div>
                    <div class="mt-4">
                      <strong>
                        Información del cliente
                      </strong>
                    </div>

                    <div class="mt-3">
                      <div class="mb-2">
                        Nombre <strong></strong>
                      </div>
                      <div class="mb-2">
                        Apellido <strong></strong>
                      </div>
                      <div class="mb-2">
                        Correo <strong></strong>
                      </div>
                      <div class="mb-2">
                        Teléfono <strong></strong>
                      </div>
                      <div class="mb-2">
                        DNI <strong></strong>
                      </div>
                      <div class="mb-2">
                        Dirección <strong></strong>
                      </div>
                      <div class="mb-2">
                        Domitorios <strong></strong>
                      </div>
                      <div class="mb-2">
                        Piso <strong></strong>
                      </div>

                      <div class="row">
                        <div class="col-lg-6">
                          <div class="mt-4">
                            <strong>
                              Item
                            </strong>
                          </div>
                          <div class="mt-2">
                            Nombre del proyecto
                          </div>
                        </div>
                        <div class="col-lg-6 text-right">
                          <div class="mt-4">
                            <strong>
                              Importe
                            </strong>
                          </div>
                          <div class="color-b mt-2">
                            <strong>
                              s/1,000
                            </strong>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-12 text-center pt-4">
                      <button class="butn butn-1 pagar">Pagar</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="paso">
          <div class="opcion-pago active">
            <h2 class="h2">
              Inicial pagada
            </h2>
            <div class="mt-4">
              <div class="row">
                <div class="col-lg-6">
                  <div>
                    <div class="proyectos__slick-el">
                      <div class="proyectos__slick-el-img-wrapper">
                        <img class="proyectos__slick-el-img" src="../src/img/opcion-pago.png" alt="">
                        <h3 class="proyectos__slick-el-title">
                          Lince
                        </h3>
                      </div>
                      <div class="proyectos__slick-el-body">
                        <div class="proyectos__slick-el-subtitle">
                          Lucid
                        </div>
                        <div class="proyectos__slick-el-address">
                          Prolong. Iquitos 1843, Lince
                        </div>
                        <div class="proyectos__slick-el-item-wrapper">
                          <div class="row">
                            <div class="col">
                              <div class="proyectos__slick-el-item">
                                <span>
                                  1 domitorio / 1 baño
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="proyectos__slick-el-price">
                          <span>
                            Área común 1 / Área común 2 <br>
                            Área común 3 / Área común 4
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-6">
                  <h3 class="h2">
                    ¡Tu pago se realizó con éxito!
                  </h3>
                  <div class="py-2">
                    Te enviamos toda la información a tu correo electrónico para que puedas descargarla, o hazlo directamente aquí.
                  </div>
                  <div class="row py-4">
                    <div class="col-lg-12">
                      <div class="contacto__butn">
                        <a href="#" class="contacto__butn-el">
                          <img src="../src/img/ico-brochure.png" alt="">
                          <span>Descargar PDF</span>
                        </a>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-12 text-center pt-4">
                      <button class="butn butn-1">Ir al inicio</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="opcion-pago">
            <h2 class="h2">
              Inicial pagada
            </h2>
            <div class="mt-4">
              <div class="row">
                <div class="col-lg-6">
                  <div>
                    <div class="proyectos__slick-el">
                      <div class="proyectos__slick-el-img-wrapper">
                        <img class="proyectos__slick-el-img" src="../src/img/opcion-pago.png" alt="">
                        <h3 class="proyectos__slick-el-title">
                          Lince
                        </h3>
                      </div>
                      <div class="proyectos__slick-el-body">
                        <div class="proyectos__slick-el-subtitle">
                          Lucid
                        </div>
                        <div class="proyectos__slick-el-address">
                          Prolong. Iquitos 1843, Lince
                        </div>
                        <div class="proyectos__slick-el-item-wrapper">
                          <div class="row">
                            <div class="col">
                              <div class="proyectos__slick-el-item">
                                <span>
                                  1 domitorio / 1 baño
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="proyectos__slick-el-price">
                          <span>
                            Área común 1 / Área común 2 <br>
                            Área común 3 / Área común 4
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-6">
                  <h3 class="h2">
                    ¡Tu pago se realizó con éxito!
                  </h3>
                  <div class="py-2">
                    Te enviamos toda la información a tu correo electrónico para que puedas descargarla, o hazlo directamente aquí.
                  </div>
                  <div class="row py-4">
                    <div class="col-lg-12">
                      <div class="contacto__butn">
                        <a href="#" class="contacto__butn-el">
                          <img src="../src/img/ico-brochure.png" alt="">
                          <span>Descargar PDF</span>
                        </a>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-12 text-center pt-4">
                      <button class="butn butn-1">Ir al inicio</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="opcion-pago">
            <h2 class="h2">
              Inicial pagada
            </h2>
            <div class="mt-4">
              <div class="row">
                <div class="col-lg-6">
                  <div>
                    <div class="proyectos__slick-el">
                      <div class="proyectos__slick-el-img-wrapper">
                        <img class="proyectos__slick-el-img" src="../src/img/opcion-pago.png" alt="">
                        <h3 class="proyectos__slick-el-title">
                          Lince
                        </h3>
                      </div>
                      <div class="proyectos__slick-el-body">
                        <div class="proyectos__slick-el-subtitle">
                          Lucid
                        </div>
                        <div class="proyectos__slick-el-address">
                          Prolong. Iquitos 1843, Lince
                        </div>
                        <div class="proyectos__slick-el-item-wrapper">
                          <div class="row">
                            <div class="col">
                              <div class="proyectos__slick-el-item">
                                <span>
                                  1 domitorio / 1 baño
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="proyectos__slick-el-price">
                          <span>
                            Área común 1 / Área común 2 <br>
                            Área común 3 / Área común 4
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-6">
                  <h3 class="h2">
                    ¡Tu pago se realizó con éxito!
                  </h3>
                  <div class="py-2">
                    Te enviamos toda la información a tu correo electrónico para que puedas descargarla, o hazlo directamente aquí.
                  </div>
                  <div class="row py-4">
                    <div class="col-lg-12">
                      <div class="contacto__butn">
                        <a href="#" class="contacto__butn-el">
                          <img src="../src/img/ico-brochure.png" alt="">
                          <span>Descargar PDF</span>
                        </a>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-12 text-center pt-4">
                      <button class="butn butn-1">Ir al inicio</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="opcion-pago">
            <h2 class="h2">
              Inicial pagada
            </h2>
            <div class="mt-4">
              <div class="row">
                <div class="col-lg-6">
                  <div>
                    <div class="proyectos__slick-el">
                      <div class="proyectos__slick-el-img-wrapper">
                        <img class="proyectos__slick-el-img" src="../src/img/opcion-pago.png" alt="">
                        <h3 class="proyectos__slick-el-title">
                          Lince
                        </h3>
                      </div>
                      <div class="proyectos__slick-el-body">
                        <div class="proyectos__slick-el-subtitle">
                          Lucid
                        </div>
                        <div class="proyectos__slick-el-address">
                          Prolong. Iquitos 1843, Lince
                        </div>
                        <div class="proyectos__slick-el-item-wrapper">
                          <div class="row">
                            <div class="col">
                              <div class="proyectos__slick-el-item">
                                <span>
                                  1 domitorio / 1 baño
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="proyectos__slick-el-price">
                          <span>
                            Área común 1 / Área común 2 <br>
                            Área común 3 / Área común 4
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-6">
                  <h3 class="h2">
                    ¡Tu pago se realizó con éxito!
                  </h3>
                  <div class="py-2">
                    Te enviamos toda la información a tu correo electrónico para que puedas descargarla, o hazlo directamente aquí.
                  </div>
                  <div class="row py-4">
                    <div class="col-lg-12">
                      <div class="contacto__butn">
                        <a href="#" class="contacto__butn-el">
                          <img src="../src/img/ico-brochure.png" alt="">
                          <span>Descargar PDF</span>
                        </a>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-12 text-center pt-4">
                      <button class="butn butn-1">Ir al inicio</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="opcion-pago">
            <h2 class="h2">
              Inicial pagada
            </h2>
            <div class="mt-4">
              <div class="row">
                <div class="col-lg-6">
                  <div>
                    <div class="proyectos__slick-el">
                      <div class="proyectos__slick-el-img-wrapper">
                        <img class="proyectos__slick-el-img" src="../src/img/opcion-pago.png" alt="">
                        <h3 class="proyectos__slick-el-title">
                          Lince
                        </h3>
                      </div>
                      <div class="proyectos__slick-el-body">
                        <div class="proyectos__slick-el-subtitle">
                          Lucid
                        </div>
                        <div class="proyectos__slick-el-address">
                          Prolong. Iquitos 1843, Lince
                        </div>
                        <div class="proyectos__slick-el-item-wrapper">
                          <div class="row">
                            <div class="col">
                              <div class="proyectos__slick-el-item">
                                <span>
                                  1 domitorio / 1 baño
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="proyectos__slick-el-price">
                          <span>
                            Área común 1 / Área común 2 <br>
                            Área común 3 / Área común 4
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-6">
                  <h3 class="h2">
                    ¡Tu pago se realizó con éxito!
                  </h3>
                  <div class="py-2">
                    Te enviamos toda la información a tu correo electrónico para que puedas descargarla, o hazlo directamente aquí.
                  </div>
                  <div class="row py-4">
                    <div class="col-lg-12">
                      <div class="contacto__butn">
                        <a href="#" class="contacto__butn-el">
                          <img src="../src/img/ico-brochure.png" alt="">
                          <span>Descargar PDF</span>
                        </a>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-12 text-center pt-4">
                      <button class="butn butn-1">Ir al inicio</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="opcion-pago">
            <h2 class="h2">
              Inicial pagada
            </h2>
            <div class="mt-4">
              <div class="row">
                <div class="col-lg-6">
                  <div>
                    <div class="proyectos__slick-el">
                      <div class="proyectos__slick-el-img-wrapper">
                        <img class="proyectos__slick-el-img" src="../src/img/opcion-pago.png" alt="">
                        <h3 class="proyectos__slick-el-title">
                          Lince
                        </h3>
                      </div>
                      <div class="proyectos__slick-el-body">
                        <div class="proyectos__slick-el-subtitle">
                          Lucid
                        </div>
                        <div class="proyectos__slick-el-address">
                          Prolong. Iquitos 1843, Lince
                        </div>
                        <div class="proyectos__slick-el-item-wrapper">
                          <div class="row">
                            <div class="col">
                              <div class="proyectos__slick-el-item">
                                <span>
                                  1 domitorio / 1 baño
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="proyectos__slick-el-price">
                          <span>
                            Área común 1 / Área común 2 <br>
                            Área común 3 / Área común 4
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-6">
                  <h3 class="h2">
                    ¡Tu pago se realizó con éxito!
                  </h3>
                  <div class="py-2">
                    Te enviamos toda la información a tu correo electrónico para que puedas descargarla, o hazlo directamente aquí.
                  </div>
                  <div class="row py-4">
                    <div class="col-lg-12">
                      <div class="contacto__butn">
                        <a href="#" class="contacto__butn-el">
                          <img src="../src/img/ico-brochure.png" alt="">
                          <span>Descargar PDF</span>
                        </a>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-12 text-center pt-4">
                      <button class="butn butn-1">Ir al inicio</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


  <!-- Mensaje Modal -->

  <div class="modal fade modal-mensaje" id="mensajeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <img src="../src/img/equis.png" alt="">
        </button>
        <div class="modal-body">
          <div class="modal-mensaje__body">
            <h3 class="h3 mb-4">
              Futuro propietario
            </h3>
            <div class="foo fakeScroll fakeScroll--hasBar">
              <div>
                <p>
                  Felicitaciones por dar el primer paso para separar su departamento propio. Antes de continuar
                  eso, agradeceríamos de lectura a lo siguiente y nos brinde su conformidad:
                </p>
                <p>
                  Declaro conocer que el presente formulario consiste en una solicitud de separación; no
                  na reserva de compra ni genera derecho alguno sobre la unidad elegida.
                </p>
                <p>
                  Declaro conocer que la disponibilidad de la unidad inmobiliaria elegida se encuentra a
                  sujeta a confirmación por parte de la Inmobiliaria.
                </p>
                <p>
                  Declaro conocer que el plazo máximo para que la Inmobiliaria proceda con la
                  confirmación será de cuarenta y ocho (48) horas contadas desde que el pago haya sido
                  procesado.
                </p>
                <p>
                  Declaro conocer que el presente formulario consiste en una solicitud de separación; no
                  e una reserva de compra ni genera derecho alguno sobre la unidad elegida.
                </p>
                <p>
                  Declaro conocer que la disponibilidad de la unidad inmobiliaria elegida se encuentra a
                  sujeta a confirmación por parte de la Inmobiliaria.
                </p>
                <p>
                  Declaro conocer que el plazo máximo para que la Inmobiliaria proceda con la
                  confirmación será de cuarenta y ocho (48) horas contadas desde que el pago haya sido
                  procesado.
                </p>
              </div>
          </div>

          </div>
          <div class="text-center pt-4">
            <button class="butn butn-1 mr-2 aceptar-comunicado">Acepto</button>
            <button class="butn butn-2 ml-2" data-dismiss="modal">No acepto</button>
          </div>
        </div>
      </div>
    </div>
  </div>


  <!-- Footer -->

  <footer class="footer">
    <div class="container">
      <div class="footer__header">
        <div class="row">
        <div class="col-lg-3">
          <img src="../src/img/logo-white.png" alt="">
        </div>
        <div class="col-lg-3">
          <div class="footer__item">
            <img src="../src/img/ico-phone-orange.png" alt="">
            <span>715 8080</span>
          </div>
        </div>
        <div class="col-lg-3">
          <div class="footer__item">
            <img src="../src/img/ico-cellphone-orange.png" alt="">
            <span>924389800</span>
          </div>
        </div>
        <div class="col-lg-3">
          <div class="footer__item">
            <img src="../src/img/ico-envelope-orange.png" alt="">
            <span>informeventas@arteco.pe</span>
          </div>
        </div>
      </div>
    </div>
    </div>
    <div class="container">
      <div class="footer__inner">
        <div class="row">
          <div class="col-lg-3">
            <div class="footer__title">
              Proyectos
            </div>
            <div class="footer__list">
              <ul>
                <li><a href="departamentos-interna.html">Depa Smart Lince</a></li>
                <li><a href="departamentos-interna.html">Lucid Depa Smart</a></li>
                <li><a href="departamentos-interna.html">Depa Smart Lince</a></li>
                <li><a href="departamentos-interna.html">Depa Smart Breña</a></li>
                <li><a href="departamentos-interna.html">Depa Smart Chorrillos</a></li>
                <li><a href="departamentos-interna.html">Paseo La Campiña IV</a></li>
              </ul>
            </div>
          </div>
          <div class="col-lg-3">
            <div class="footer__title">
              Secciones
            </div>
            <div class="footer__list">
              <ul>
                <li><a href="departamentos.html">Proyecto</a></li>
                <li><a href="blog.html">Noticias</a></li>
                <li><a href="nosotros.html">Nosotros</a></li>
                <li><a href="contacto.html">Contacto</a></li>
              </ul>
            </div>
          </div>
          <div class="col-lg-3">
            <div class="footer__title">
              Otros
            </div>
            <div class="footer__list">
              <ul>
                <li><a href="terminos-condiciones.html">Términos y condiciones</a></li>
                <li><a href="#">Proveedores</a></li>
                <li><a href="libro-reclamaciones.html"><img src="../src/img/ico-reclamaciones-white.png" alt=""> Libro de reclamaciones</a></li>
              </ul>
            </div>
          </div>
          <div class="col-lg-3">
            <div class="footer__title">
              Métodos de pago
            </div>
            <div class="footer__list">
              <ul>
                <li><a href="#"><img src="../src/img/ico-mastercard.png" alt=""></a></li>
                <li><a href="#"><img src="../src/img/ico-visa.png" alt=""></a></li>
              </ul>
            </div>
            <div class="footer__title pt-5">
              Síguenos en
            </div>
            <div class="footer__list footer__list-socials">
              <ul>
                <li>
                  <a href="#"><img src="../src/img/ico-facebook.svg" alt=""></a>
                  <a href="#"><img src="../src/img/ico-linkedin.svg" alt=""></a>
                  <a href="#"><img src="../src/img/ico-instagram.svg" alt=""></a>
                  <a href="#"><img src="../src/img/ico-youtube.svg" alt=""></a>
                  <a href="#"><img src="../src/img/ico-twitter.svg" alt=""></a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="container">
      <div class="footer__bottom">
        Copyright 2021. Todos los derechos reservados Arteco  | Central: Av. Manuel Olguín 501 - Of. 506, Surco - Lima  | <a href="#">Desarrollo por Malcolm</a> .
      </div>
    </div>
  </footer>



  <script src="https://code.jquery.com/jquery-3.5.1.min.js"
    integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx"
    crossorigin="anonymous"></script>
  <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
  <!-- <script src="../src/js/owl.carousel.min.js"></script>
  <script src="../src/js/jquery.mousewheel.min.js"></script> -->
  <!-- <script src="https://unpkg.co/gsap@3/dist/gsap.min.js"></script> -->
  <!-- <script src="https://unpkg.com/gsap@3/dist/ScrollTrigger.min.js"></script> -->
  <!-- Async script executes immediately and must be after any DOM elements used in callback. -->
  <script
  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA_x185qTPg9r1MTgbYVm_7x3JEvxwAZW4&libraries=&v=weekly"
></script>
  <script src="../src/js/effects.js"></script>
<script src="../src/js/fakescroll.min.js"></script>
  <script src="../src/js/custom-select.js"></script>
  <script src="../src/js/app.js"></script>

</body>

</html>
