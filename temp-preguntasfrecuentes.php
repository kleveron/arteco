<?php /* Template Name: Template Preguntas Frecuentes*/ ?>

<?php get_header('dark'); ?>
 <!-- Preguntas frecuentes -->
<?php
 if( have_rows('grupo') ):
   while( have_rows('grupo') ) : the_row();
   $i=$i+1;
 endwhile;
   $total = 12/$i;
endif; ?>
 <div class="section">
     <?php get_template_part( 'template-parts/banner', 'top' ); ?>

  <?php
   $grupo = $_GET['grupo'];
   if($grupo==null){   ?>
   <div class="container">
     <div class="text-center mb-5">
      <h2 class="h2">
        <?php _e('Preguntas frecuentes','arteco') ?>
      </h2>
     </div>
     <div class="row text-center mb-5 pt-3">
       <?php
       if( have_rows('grupo') ):
         while( have_rows('grupo') ) : the_row();
           $categoria = get_sub_field('categoria');
           $icono = get_sub_field('icono');
           $slugcat =   slugify($categoria);

        ?>
       <div class="col-lg-<?php echo $total; ?>">
         <a href="?grupo=<?php echo $slugcat; ?>" class="pregunta-cat">
           <div class="imgcat"><img class="pregunta-cat__img" src="<?php echo $icono; ?>" alt=""></div>
           <div class="pregunta-cat__txt">
             <?php echo $categoria; ?>
           </div>
          </a>
       </div>
       <?php endwhile;
      endif; ?>
     </div>
   </div>
  <?php }   if ($grupo!=null) {   ?>
    <div class="container">
   <div class="text-center mb-5">
    <h2 class="h2">
      <?php _e('Preguntas frecuentes','arteco') ?>
     </h2>
   </div>
   <div class="mb-5 pt-3">
    <?php  if( have_rows('grupo') ):
       while( have_rows('grupo') ) : the_row();
           $categoria = get_sub_field('categoria');
             $slugcat =   slugify($categoria);
           ?>
     <h4 class="h4 my-4" id="categoria-1">
       <?php echo $categoria; ?>
      </h4>
      <?php   if( have_rows('preguntas') ):
               while( have_rows('preguntas') ) : the_row();
                   $titulo = get_sub_field('titulo');
                   $pregunta = get_sub_field('pregunta'); ?>
                   <div class="pregunta-item <?php if($grupo == $slugcat){ echo 'active'; } ?>" >
                     <div class="pregunta-item__mas"></div>
                      <div class="pregunta-item__arrow"></div>
                      <div class="pregunta-item__title">
                        <strong><?php echo $titulo; ?></strong>
                      </div>
                     <div class="pregunta-item__inner">
                        <div> <?php echo $pregunta; ?></div>
                     </div>
                   </div>
          <?php    endwhile; endif; ?>

    <?php endwhile;   endif; ?>
   </div>
 </div>
   <?php } ?>
 </div>


 <?php get_footer(); ?>
