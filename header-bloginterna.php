<?php
/**
 * Header Blog
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Arteco
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<?php wp_head(); ?>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
    integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />
    <!-- <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/src/css/owl.carousel.min.css">
      <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/src/css/owl.theme.default.min.css"> -->
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" />
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/src/css/vendor.css?v=<?= time(); ?>">
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>

<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e( 'Skip to content', 'arteco' ); ?></a>

	<header id="masthead" class="site-header">

        <div class="menu" <?php /*if( is_user_logged_in()){?>  style="top: 32px;" <?php } */?> >
            <div class="container">
                <div class="menu__inner">
                    <div class="menu__logo">
                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>">

                            <img src="<?php echo get_template_directory_uri(); ?>/src/img/logo.png" alt="">
                            <img src="<?php echo get_template_directory_uri(); ?>/src/img/logo-2.png" alt="">
                        </a>
                        <div class="menu__desc">   Blog </div>
                    </div>
                    <div class="menu__opciones">
                        <div class="input-search input-search-menu">
													  <form id="searchform" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
                            <input type="text" class="search-field form-control" name="s" placeholder="Escribe tu busqueda..." value="<?php echo get_search_query(); ?>">
                            <input type="hidden" name="post_type" value="post" />
														<input type="submit" value=" " style="position: absolute;width: 76px;right: 0;top: 0px;border: 0;background: url(http://localhost/arteco/wp-content/themes/arteco/src/img/search.png);background-repeat: no-repeat;background-position: center center;">
														</form>
                        </div>
                      </div>
                      <div class="menu__boton">
                        <a href="#suscribirse" class="butn butn-1">Suscribirse</a>
                      </div>
                </div>


            </div>
        </div>

	</header> <!-- #masthead -->


	<?php get_template_part( 'template-parts/header', 'blogcategorias' ); ?>
