<?php if ( get_field( 'activar_timeline' ) == 1 ) :
$contador = 0;
  ?>

<!-- Timeline -->

<div class="bg-color-11 section overflow-hidden">
  <div class="container">
    <div class="d-lg-flex d-block align-items-center">
      <div class="color-1 big-text mr-4">
       <?php the_field( 'superyear' ); ?>
      </div>
      <h2 class="h1">
        <?php the_field( 'supertitulo' ); ?>
      </h2>
    </div>
  </div>


  <div class="container pt-5 mt-5 container--timeline">
    <div class="timeline-container timeline-theme-1 pt-3">
      <div class="timeline js-timeline">
        <?php if( have_rows('timeline_item') ):
              while( have_rows('timeline_item') ) : the_row();
                   $contador = $contador + 1;
                   $year = get_sub_field('ano');
                   $titulo = get_sub_field('titulo');
                   $distrito = get_sub_field('distrito');
                   $direccion = get_sub_field('direccion');
                   $descripcion = get_sub_field('descripcion');
                   $titulo_atributos = get_sub_field('titulo_atributos');?>
                   <div data-time="<?php echo $year; ?>">
                          <div class="">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <h3 class="h2 ln-h3"><?php echo $titulo; ?></h3>
                                        <div class="mt-4"><strong><?php echo $distrito; ?></strong></div>
                                        <div class="mt-1"> <?php echo $direccion; ?>  </div>
                                        <div class="color-3 mt-3"><?php echo $descripcion; ?></div>
                                        <h4 class="h4 mt-5 mb-4"> <?php echo $titulo_atributos; ?> </h4>
                                          <div class="row">
                                              <?php   /*Atributos Repeater*/
                                                    if( have_rows('atributos') ){
                                                          while( have_rows('atributos') ){ the_row();
                                                                  $logos_atributos = get_sub_field('logo'); ?>
                                                                  <div class="col-lg-4">
                                                                      <img src="<?php echo $logos_atributos; ?>" alt="">
                                                                  </div>

                                                   <?php } } ?>
                                           </div>
                                    </div>
                                    <div class="col-lg-6">
                                          <div class="slider-ss__slick-2" data-id="<?php echo $contador; ?>">
                                              <?php   /*Carrusel Repeater*/
                                                      if( have_rows('carrusel') ){
                                                            while( have_rows('carrusel') ){ the_row();
                                                                  $titulo_carrusel = get_sub_field('titulo');
                                                                  $subtitulo_carrusel = get_sub_field('subtitulo');
                                                                  $video_carrusel = get_sub_field('video');
                                                                  $imagen_carrusel = get_sub_field('imagen'); ?>
                                                  <div>
                                                      <div class="slider-ss__el slider-ss__el--2">
                                                              <div class="slider-ss__el-img">
                                                                <img src="<?php echo $imagen_carrusel; ?>" alt="">
                                                              </div>
                                                              <?php  if ( get_sub_field( 'activar_video' ) == 1 ) { ?>
                                                                <div class="slider-ss__el-play video-btn" data-toggle="modal" data-src="<?php echo $video_carrusel; ?>" data-target="#modalVideo">
                                                                  <img src="<?php echo get_template_directory_uri(); ?>/src/img/play.png" alt="">
                                                                </div>
                                                                <?php } ?>
                                                                <div class="slider-ss__msg">
                                                                    <div class="slider-ss__msg-title"><?php echo $titulo_carrusel; ?></div>
                                                                    <div class="slider-ss__msg-dsc"><?php echo $subtitulo_carrusel; ?></div>
                                                                  </div>
                                                       </div>
                                                   </div>   <?php  } }   ?>

                                                </div>
                                              <div class="slider-ss__slick-dots slider-ss__slick-dots-<?php echo $contador; ?>"></div>
                                      </div>
                                    </div>
                                  </div>
                  </div>
              <?php  endwhile;  endif; ?>
                 </div>
                </div>
               </div>
 </div>

<?php endif; ?>
