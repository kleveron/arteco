<?php if ( get_field( 'activar_proyecto_entregado' ) == 1 ) :

  if ( have_rows( 'banner_entregado' ) ) :
         while ( have_rows( 'banner_entregado' ) ) : the_row();
             if ( get_sub_field( 'fondo' ) ) :
                 $fondo = get_sub_field( 'fondo' );
             endif;
                 $titulobanner = get_sub_field( 'titulo' );
         endwhile;
  endif;


   ?>


  <!-- Banner Entregados-->

    <div class="banner banner-departamentos">

   <?php get_template_part( 'template-parts/banner', 'top' ); ?>

      <div class="banner__images">
        <div>
          <div>
            <img class="banner__img" src="<?php echo $fondo; ?>" alt="">
          </div>
        </div>
      </div>
      <div class="banner__title-wrapper">
        <div class="banner__title">
           <?php echo $titulobanner; ?>
        </div>
      </div>
    </div>


<?php else : ?>



  <?php
   if ( have_rows( 'banner' ) ) :
          while ( have_rows( 'banner' ) ) : the_row();
              if ( get_sub_field( 'fondo' ) ) :
                  $fondo = get_sub_field( 'fondo' );
              endif;
                  $estado = get_sub_field( 'estado' );
                  $titulobanner = get_sub_field( 'titulo' );
          endwhile;
   endif;

    if ( have_rows( 'previo' ) ) :
          while ( have_rows( 'previo' ) ) : the_row();
           $areadesde = get_sub_field( 'area_desde' );
           $areahasta = get_sub_field( 'area_hasta' );

           endwhile;
    endif;
  ?>

  <!-- Banner -->
  <div class="banner banner-departamentos-interna">
      <?php get_template_part( 'template-parts/banner', 'top' ); ?>
   <div class="banner__images">
          <div>
              <div>
                  <img class="banner__img" src="<?php echo $fondo; ?>" alt="">
              </div>
          </div>
      </div>
      <div class="banner__content">
          <div class="container">
              <div class="banner__card-wrapper">
                  <div class="banner__card active">
                      <div class="banner__card-1">
                          <div class="banner__card-supratitle">
                             <?php echo $estado?>
                          </div>
                          <div class="banner__card-title">
                             <?php echo $titulobanner ?>
                          </div>
                          <div class="banner__card-br"></div>
                          <div class="banner__card-bottom">
                              <div class="banner__card-bottom-metraje">
                                  <img src="<?php echo get_template_directory_uri(); ?>/src/img/ico-metraje-black.png" alt="">
                                  <span>Desde <?php echo $areadesde; ?>m2 hasta <?php echo $areahasta; ?>m2</span>
                              </div>
                          </div>
                          <div>
                              <a href="#form-contacto" class="butn butn-1 ar-scroll">Cotiza aquí</a>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>


  </div>



<?php endif; ?>
