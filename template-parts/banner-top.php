<?php if ( get_field( 'activar_banner_top' ) == 1 ) : ?>

<div class="banner__butn-wrapper d-none d-lg-block">
  <div class="banner__butn-content">

    <?php  if( have_rows('banner_flotante', 'option') ):
          while( have_rows('banner_flotante', 'option') ): the_row();
                $texto_en = get_sub_field('texto_de_llamada');
                $link_en = get_sub_field('link_en_llamada');
                $whatsapp = get_sub_field('whatsapp');
                $chat = get_sub_field('chat');

           ?>
    <?php if($chat): ?>
      <a href="<?= $chat; ?>" class="banner__butn-opt banner__butn-chat">
        <img src="<?php echo get_template_directory_uri(); ?>/src/img/ico-chat.png" alt="">
      </a>
    <?php endif; ?>
    <a href="<?php echo $whatsapp; ?>" class="banner__butn-opt banner__butn-ws">
      <img src="<?php echo get_template_directory_uri(); ?>/src/img/ico-whatsapp.png" alt="">
    </a>
    <?php if ($texto_en): ?>
      <a href="#" class="banner__butn butn butn-1"><?php echo $texto_en; ?></a>
    <?php endif; ?>


    <?php   endwhile;  endif; ?>

  </div>
</div>

<?php endif; ?>
