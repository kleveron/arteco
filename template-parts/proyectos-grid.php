<?php

// WP_Query arguments
$args = array(
    'post_type'              => array( 'proyecto_arteco' ),
    //'order'                  => 'ASC',
);

// The Query
$proyectos = new WP_Query( $args );
if ( $proyectos->have_posts() ) { ?>



<div class="text-center">
  <div class="row proyectos__no-slick" id="proyectos" data-isotope='{ "itemSelector": ".grid-itm", "layoutMode": "fitRows" }'>
    <?php  while ( $proyectos->have_posts() ) {
      $proyectos->the_post();
      $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full');
      $categorias = get_the_terms( $post->ID, 'proyecto_categoria' );
      $distritos = get_the_terms( $post->ID, 'proyecto_distrito' );
      $inactivo = get_field( 'activar' );

      if(!empty($categorias)){
          $cat = array_shift($categorias);
          $catproyecto = $cat->name;
          $catproyectoslug = $cat->slug;
       }

       if(!empty($distritos)){
           $dist = array_shift($distritos);
           $distproyecto = $dist->name;
           $distproyectoslug = $dist->slug;
        }


    ?>

    <div class="col-lg-4 grid-itm <?php echo $distproyectoslug.' '.$catproyectoslug.' '.$inactivo; ?>">
      <div class="proyectos__slick-el">
        <a href="<?php echo get_permalink(); ?>">
        <div class="proyectos__slick-el-img-wrapper">
          <div class="proyectos__slick-el-img-wrapper-inner">
            <img class="proyectos__slick-el-img" src="<?php the_post_thumbnail_url(); ?>" alt="">
          </div>
          <h3 class="proyectos__slick-el-title">
            <?php echo $distproyecto; ?>
          </h3>
          <div class="proyectos__slick-el-sticker"><?php echo $catproyecto; ?></div>
        </div>
        </a>
        <div class="proyectos__slick-el-body">
          <div class="proyectos__slick-el-subtitle">
           <?php the_title(); ?>
          </div>
          <?php if ( have_rows( 'previo' ) ) : ?>
          <?php while ( have_rows( 'previo' ) ) : the_row(); ?>
           <div class="proyectos__slick-el-address">
            <?php the_sub_field( 'ubicacion' ); ?>
          </div>
          <div class="proyectos__slick-el-item-wrapper">
            <div class="row">
              <div class="col">
                <div class="proyectos__slick-el-item">
                  <img src="<?php echo get_template_directory_uri(); ?>/src/img/ico-metraje.png" alt="">
                  <span>Desde   <?php the_sub_field( 'area_desde' ); ?></span>
                </div>
              </div>
              <div class="col">
                <div class="proyectos__slick-el-item">
                  <img src="<?php echo get_template_directory_uri(); ?>/src/img/ico-habitaciones.png" alt="">
                  <span> <?php the_sub_field( 'dormitorios' ); ?> dorm.</span>
                </div>
              </div>
            </div>
          </div>
          <div class="proyectos__slick-el-price">
            <span>Desde:</span>
            <span class="proyectos__slick-el-price-number"> S/ <?php the_sub_field( 'precio_desde' ); ?> </span>
          </div>
          <?php endwhile; ?>
          <?php endif; ?>

          <div class="text-center">
            <a href="<?php echo get_permalink(); ?>" class="butn butn-1">Ver más</a>
          </div>
        </div>
      </div>
    </div>



      <?php } } else { _e('No se econtraron resultados','arteco');}
       wp_reset_postdata(); ?>

  </div>
</div>
</div>
</div>
