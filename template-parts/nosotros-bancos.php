    <!-- Bancos -->
<?php if ( get_field( 'activar_bancos' ) == 1 ) : ?>
    <div class="section">
      <div class="container">
        <div class="supratitle text-center">
        <?php the_field( 'pretitulo_bancos' ); ?>
        </div>
        <h2 class="h3 text-center">
        <?php the_field( 'titulo_bancos' ); ?>
        </h2>
        <div class="my-4">
            <?php the_field( 'descripcion_bancos' ); ?>
        </div>
      </div>
      <div class="container">
        <?php if ( have_rows( 'logos_bancos' ) ) : ?>
          <div class="row">
           <?php while ( have_rows( 'logos_bancos' ) ) : the_row();
                 $imagen = get_sub_field('imagen');
           ?>
                <div class="col-md text-center mt-4">
                  <img src="<?php echo $imagen; ;?>" alt="">
                </div>
          <?php endwhile; ?>
          </div>
          <?php endif; ?>
      </div>
    </div>
<?php endif; ?>
