

 <?php if ( get_field( 'activar_caracteristicas_2' ) == 1 ) : ?>
  <!-- Características lista -->
  <?php if ( have_rows( 'caracteristicas_2' ) ) : ?>
  	<?php while ( have_rows( 'caracteristicas_2' ) ) : the_row(); ?>

  <div class="departamento-lista section">
    <div class="container">
      <h2 class="h3 pb-4">
       	<?php the_sub_field( 'caracteristicas_2_titulo' ); ?>
      </h2>
    </div>
      <div class="container">
        <div class="row">
          <div class="col-lg-4">
            <ul class="lista-check">
              <?php
              if( have_rows('bloque_1') ):   while( have_rows('bloque_1') ) : the_row();
                  $caracteristica = get_sub_field('caracteristica'); ?>
                 <li>
                <?php echo $caracteristica; ?>
              </li>
                <?php  endwhile;  endif; ?>

            </ul>
          </div>
          <div class="col-lg-4">
            <ul class="lista-check">
              <?php
              if( have_rows('bloque_2') ):   while( have_rows('bloque_2') ) : the_row();
                  $caracteristica = get_sub_field('caracteristica'); ?>
                 <li>
                <?php echo $caracteristica; ?>
              </li>
                <?php  endwhile;  endif; ?>

            </ul>
          </div>
          <div class="col-lg-4">
            <ul class="lista-check">
              <?php
              if( have_rows('bloque_3') ):   while( have_rows('bloque_3') ) : the_row();
                  $caracteristica = get_sub_field('caracteristica'); ?>
                 <li>
                <?php echo $caracteristica; ?>
              </li>
                <?php  endwhile;  endif; ?>

            </ul>
          </div>
        </div>
      </div>
  </div>
<?php endwhile; ?>
<?php endif; ?>
<?php endif; ?>
