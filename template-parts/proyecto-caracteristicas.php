<?php

?>
<!-- Características -->
<?php if ( get_field( 'activar_caracteristicas' ) == 1 ) : ?>

    <div class="departamento-cacteristicas section <?php if ( get_field( 'activar_proyecto_entregado' ) == 0 ) : ?>pt-0<?php  endif;?>">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <h2 class="h3 pb-4">
                        Características del proyecto
                    </h2>
                    <div class="departamento-cacteristicas-1">
                        <div class="row">
                          <?php  if( have_rows('caracteristica') ):
                            while( have_rows('caracteristica') ) : the_row(); ?>
                                <?php $iconocaracteristica = get_sub_field('icono'); ?>
                                <?php $textocaracteristica = get_sub_field('titulo'); ?>
                            <div class="col-lg-6">
                                <div class="departamento-cacteristicas-el">
                                    <img src="<?php echo $iconocaracteristica; ?>" alt="">
                                    <div class="departamento-cacteristicas-el-title">
                                        <?php echo $textocaracteristica; ?>
                                    </div>
                                </div>
                            </div>
                            <?php endwhile; endif;?>


                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="slider-ss__slick">
                        <?php if( have_rows('video_caracteristica') ):
                                while( have_rows('video_caracteristica') ) : the_row();
                                    $imagenprevia = get_sub_field('imagen_previa');
                                    $videolink = get_sub_field('video_link'); ?>
                        <div>
                            <div class="slider-ss__el">
                                <div class="slider-ss__el-img">
                                    <img src="<?php echo $imagenprevia; ?>" alt="">
                                </div>
                                <div class="slider-ss__el-play video-btn" data-toggle="modal" data-src="<?php echo $videolink; ?>" data-target="#modalVideo">
                                    <img src="<?php echo get_template_directory_uri(); ?>/src/img/play.png" alt="">
                                </div>
                                <div class="slider-ss__el-zoom zoom-btn" data-toggle="modal" data-src="<?php echo $imagenprevia; ?>" data-target="#modalZoom">
                                    <img src="<?php echo get_template_directory_uri(); ?>/src/img/two-arrows.png" alt="">
                                </div>
                            </div>
                        </div>
                          <?php endwhile;  endif;       ?>

                    </div>
                </div>
            </div>
        </div>
    </div>

<?php else : ?>

<?php endif; ?>
