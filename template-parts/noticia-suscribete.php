
     <!-- Suscribete -->
<?php if ( get_field( 'activar_suscribete' ) == 1 ) : ?>
  <div id="suscribirse" class="section bg-color-11">
    <div class="container">
      <div class="row">
        <div class="col-lg-6">
          <h2 class="h2">
            ¡Suscríbete y disfruta de nuestro contenido!
          </h2>
          <div class="color-3 pt-3 d-none">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit...
          </div>
        </div>
        <div class="col-lg-6">
          <form class="" method="POST">
            <input type="hidden" name="action" value="ar_ajax_newsletter">
            <input type="hidden" name="post" value="<?= $post->ID; ?>">
            <div class="row">
              <div class="col-lg-6">
                <input placeholder="Nombre *" name="firstname" type="text" class="form-control" required>
              </div>
              <div class="col-lg-6">
                <input placeholder="Apellido *" name="lastname"  type="text" class="form-control" required>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12">
                <input placeholder="Correo electrónico *" name="email" type="email" class="form-control" required>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12">
                <div class="d-block text-left my-2 custom-control custom-checkbox ">
                  <?php
                    $page = '';
                    $page2 = '';
                    if( have_rows('admin_terms', 'option') ):
                      while( have_rows('admin_terms', 'option') ): the_row();
                        $page  = get_sub_field('admin_necessary_data_policies');
                        $page2 = get_sub_field('admin_additional_data_policies');
                      endwhile;
                    endif;
                    $terms = 'He leido y acepto los términos y condiciones de la "Politicas de datos personales para fines necesarios".';
                    if($page):
                      $terms = 'He leido y acepto los términos y condiciones de la <a href="'.get_the_permalink($page->ID).'" target="_blank" rel="noopener noreferrer">"Politicas de datos personales para fines necesarios".</a>';
                    endif;
                  ?>
                  <input type="checkbox" class="custom-control-input" id="cbox-terminos-post" name="terms" value="on" required checked>
                  <label for="cbox-terminos-post" class="custom-control-label form-text"><?= $terms; ?></label>
                </div>
              </div>
              <div class="col-lg-12">
                <?php

                  $terms_ext = 'He leido y acepto los términos y condiciones de la "Politicas de datos personales para fines adicionales".';
                  if($page2):
                    $terms_ext = 'He leido y acepto los términos y condiciones de la <a href="'.get_the_permalink($page2->ID).'" target="_blank" rel="noopener noreferrer">"Politicas de datos personales para fines adicionales".</a>';
                  endif;
                ?>
                <div class="d-block text-left my-2 custom-control custom-checkbox ">
                  <input type="checkbox" class="custom-control-input" id="cbox-autorizo-post" name="terms_extra" value="on" >
                  <label for="cbox-autorizo-post" class="custom-control-label form-text"><?= $terms_ext; ?></label>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-lg-12 text-center pt-4">
                <button class="butn butn-1 send-suscription">Suscríbirme</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>
