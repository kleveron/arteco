<!-- Mapa -->
<?php if ( get_field( 'activar_mapa' ) == 1 ) : ?>
<div class="departamento-mapa section bg-color-11">
  <div class="container">
    <div class="row">
      <div class="col-lg-4">
        <?php if( get_field( 'map_pretitulo' ) ): ?>
          <div class="supratitle"> <?= get_field( 'map_pretitulo' ); ?> </div>
        <?php else:?>
          <div class="supratitle"> UBICACIÓN ESTRATÉGICA </div>
        <?php endif; ?>

        <?php if( get_field( 'map_titulo' ) ): ?>
          <h2 class="h3"> <?= get_field( 'map_titulo' ); ?> </h2>
        <?php else:?>
          <h2 class="h3"> Características del proyecto </h2>
        <?php endif; ?>

        <div class="pt-5">
          <div class="d-sm-flex align-items-center">
            <div class="p-1 pr-3">
              <img src="<?= get_template_directory_uri(); ?>/src/img/marker-black.png" alt="">
            </div>
            <?php if( isset(get_field( 'previo' )['ubicacion']) && get_field( 'previo' )['ubicacion'] != '' ): ?>
              <div class="position-relative">
                <strong> Ubicación </strong><?= get_field( 'previo' )['ubicacion']; ?>
              </div>
            <?php endif; ?>
          </div>
          <div class="pt-4">
            <div class="row">
              <?php if( get_field( 'map_pointers' ) ): ?>
                <?php  foreach ( get_field( 'map_pointers' ) as $key => $pointer ):?>
                  <?php if(isset($pointer['pointers_icon'] ) && $pointer['pointers_icon']  != '' && isset($pointer['pointers_text'] ) && $pointer['pointers_text']  != ''): ?>
                    <div class="col-auto col-lg-4 tex-center">
                      <div class="butn-ico <?= $key == 0 ? 'active': '' ; ?>" data-category="<?= sanitize_title($pointer['pointers_text']); ?>" data-toggle="tooltip" data-placement="top" title="<?= $pointer['pointers_text']; ?>">
                        <?= wp_get_attachment_image($pointer['pointers_icon'], 'full', true, array('class'=>'img-fluid w-auto h-auto p-1'))?>
                      </div>
                    </div>
                  <?php endif; ?>
                <?php endforeach;?>
              <?php endif; ?>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-7">

        <!-- Mapa -->
        <?php if( get_field( 'map_address' ) ): ?>
          <?php $ml = get_field( 'map_address' ); ?>
          <?php if(isset($ml['lat']) && $ml['lat'] && isset($ml['lng']) && $ml['lng']): ?>
            <div id="nmap" style="height: 379px" class="ratio ratio-16x9 jc-astoria-maps" data-lat="<?= $ml['lat']; ?>" data-lng="<?= $ml['lng']; ?>"></div>
          <?php endif; ?>    
        <?php endif; ?>

        <!-- //botones  -->
        <div class="mt-4">
          <?php if( get_field( 'link_de_google_maps' ) ): ?>
            <a class="butn butn-5 mr-4" href="<?= get_field( 'link_de_google_maps' ); ?>" target="_blank" rel="noopener noreferrer">
              <img class="d-inline-block align-middle" src="<?= get_template_directory_uri(); ?>/src/img/ico-maps.png">
              <span class="d-inline-block align-middle"> Google Maps </span>
            </a>
          <?php endif; ?>
          <?php if( get_field( 'link_de_waze' ) ): ?>
            <a class="butn butn-5 mr-4" href="<?= get_field( 'link_de_waze' ); ?>" target="_blank" rel="noopener noreferrer">
              <img class="d-inline-block align-middle" src="<?= get_template_directory_uri(); ?>/src/img/ico-waze.png">
              <span class="d-inline-block align-middle"> Waze </span>
            </a>
          <?php endif; ?>
        </div>

        <!-- Markers -->
        <div class="maps-markers">
          <?php if( get_field( 'map_pointers' ) ): ?>
            <?php  foreach ( get_field( 'map_pointers' ) as $key => $pointer ):?>
              <?php if(isset($pointer['pointers_icon'] ) && $pointer['pointers_icon']  != '' && isset($pointer['pointers_text'] ) && $pointer['pointers_text']  != ''): ?>
                <?php if(isset($pointer['pointers_list'] ) && $pointer['pointers_list']  != ''): ?>
                  <?php  foreach ( $pointer['pointers_list'] as $ey => $point ):?>
                    <?php if(isset($point['pointer_name']) && $point['pointer_name'] != '' && isset($point['pointer_desc']) && $point['pointer_desc'] != '' && isset($point['pointer_file']) && $point['pointer_file'] != '' && isset($point['pointer_lat_log']) && $point['pointer_lat_log'] != ''): ?>
                      <?php $latLong = $point['pointer_lat_log']; ?>
                      <div class="marker <?= sanitize_title($pointer['pointers_text']); ?> d-none" <?= $ey==0 ? 'data-default="1"' : ''; ?>data-lat="<?= $latLong['lat'];?>" data-lng="<?= $latLong['lng'];?>">
                        <div class="small-marker">
                          <div class="small-marker__img">
                            <img src="<?= get_template_directory_uri(); ?>/src/img/marker.svg" alt="pointer-arteco">
                          </div>
                          <div class="small-marker__title"><?= $point['pointer_name']; ?></div>
                        </div>
                        <div class="big-marker">
                          <div class="big-marker__img">
                            <?= wp_get_attachment_image($point['pointer_file'], 'full', true, array('class'=>'img-fluid w-auto h-auto', 'style'=>'max-height:100px;max-width:100%;'))?>
                          </div>
                          <div class="big-marker__title"><?= $point['pointer_name']; ?></div>
                          <div class="big-marker__desc"><?= $point['pointer_desc']; ?></div>
                        </div>
                      </div>
                    <?php endif; ?>
                  <?php endforeach;?>
                  <?php if(isset($ml['lat']) && $ml['lat'] && isset($ml['lng']) && $ml['lng']): ?>
                    <div class="marker <?= sanitize_title($pointer['pointers_text']); ?>" data-lat="<?= $ml['lat']; ?>" data-lng="<?= $ml['lng']; ?>">
                      <div class="logo-marker">
                        <div class="logo-marker__marker">
                          <img src="<?= get_template_directory_uri(); ?>/src/img/marker-black.svg" alt="">
                          <div class="logo-marker__logo">
                            <?php if( get_field( 'map_logo' ) ): ?>
                              <?= wp_get_attachment_image(get_field( 'map_logo' ), 'full', true, array('class'=>'img-fluid w-auto h-auto', 'style'=>'max-height:25px;max-width:25px;'))?>
                            <?php else: ?>
                              <img src="<?= get_template_directory_uri(); ?>/src/img/n-logo.png" alt="">
                            <?php endif; ?>
                          </div>
                        </div>
                      </div>
                    </div>
                  <?php endif; ?>
                <?php endif; ?>
              <?php endif; ?>
            <?php endforeach;?>
          <?php endif; ?>
        </div>

      </div>
    </div>

  </div>
</div>
<?php endif; ?>
