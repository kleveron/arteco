<?php  ?>


<!-- Banner -->

<div class="banner banner-departamentos">

	<?php get_template_part( 'template-parts/banner', 'top' ); ?>
	<?php if ( have_rows( 'hero' ) ) : ?>
		<?php while ( have_rows( 'hero' ) ) : the_row(); ?>



  <div class="banner__images">
    <div>
      <div>
				<?php if ( get_sub_field( 'fondo' ) ) : ?>
		      <img class="banner__img" src="<?php the_sub_field( 'fondo' ); ?>" alt="">
				<?php endif ?>
      </div>
    </div>
  </div>
  <div class="banner__title-wrapper">
    <div class="banner__title">
				<?php the_sub_field( 'titulo' ); ?>
    </div>
  </div>
<?php endwhile; ?>
<?php endif; ?>

</div>
