
<!-- Conoce -->


<?php if ( have_rows( 'conoce' ) ) : ?>
	<?php while ( have_rows( 'conoce' ) ) : the_row(); ?>



    <div class="conoce section background-bottom" style="background-image: url('<?php echo get_template_directory_uri(); ?>/src/img/home-noticia-background.png')">
      <div class="container">
        <div class="row">
          <div class="col-lg-6">
            <h3 class="h3 ln-h3">
             	<?php the_sub_field( 'titulo' ); ?>
            </h3>
            <div class="color-3">
              <?php the_sub_field( 'texto' ); ?>
               </div>
            <h4 class="h4 mt-5 mb-4">
            <?php  _e('Atributos del Proyecto','arteco');?>
            </h4>
            <div class="row">
              <?php if ( have_rows( 'atributos' ) ) :  while ( have_rows( 'atributos' ) ) : the_row(); ?>
              <div class="col-lg-4">
                <img src="<?php the_sub_field('logo'); ?>" alt="">
              </div>
               <?php endwhile;  endif; ?>
            </div>
          </div>
          <div class="col-lg-6">
            <div class="slider-ss__slick-2" data-id="1">
              <div>
                <div class="slider-ss__el slider-ss__el--2">
                  <div class="slider-ss__el-img">
                    <img src="<?php echo get_template_directory_uri(); ?>/src/img/conoce-img-1.png" alt="">
                  </div>
                  <div class="slider-ss__el-play">
                    <img src="<?php echo get_template_directory_uri(); ?>/src/img/play.png" alt="">
                  </div>
                    <div class="slider-ss__el-zoom zoom-btn" data-toggle="modal" data-src="<?php echo get_template_directory_uri(); ?>/src/img/slider-ss-1.png" data-target="#modalZoom">
                      <img src="<?php echo get_template_directory_uri(); ?>/src/img/two-arrows.png" alt="">
                    </div>
                    <div class="slider-ss__msg">
                    <div class="slider-ss__msg-title">
                      Acabados
                    </div>
                    <div class="slider-ss__msg-dsc">
                      Pisos y paredes de porcelanato
                    </div>
                  </div>
                </div>
              </div>
              <div>
                <div class="slider-ss__el slider-ss__el--2">
                  <div class="slider-ss__el-img">
                    <img src="<?php echo get_template_directory_uri(); ?>/src/img/conoce-img-1.png" alt="">
                  </div>
                  <div class="slider-ss__el-play">
                    <img src="<?php echo get_template_directory_uri(); ?>/src/img/play.png" alt="">
                  </div>
                  <div class="slider-ss__el-zoom zoom-btn" data-toggle="modal" data-src="<?php echo get_template_directory_uri(); ?>/src/img/slider-ss-1.png" data-target="#modalZoom">
                    <img src="<?php echo get_template_directory_uri(); ?>/src/img/two-arrows.png" alt="">
                  </div>
                </div>
              </div>
            </div>
            <div class="slider-ss__slick-dots slider-ss__slick-dots-1"></div>
          </div>
        </div>
      </div>
    </div>



  <?php endwhile; ?>
<?php endif; ?>
