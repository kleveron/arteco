<?php

$args = array(
    'post_type'              => array( 'proyecto_arteco' ),
    'order'                  => 'ASC',
    'tax_query' => array(
        array(
            'taxonomy' => 'proyecto_categoria',
            'field'    => 'slug',
            'terms'    =>  'entregados',
        ),
    ),
);

$proyectos = new WP_Query( $args ); ?>

<!-- Proyectos Entregados -->

<div class="home-proyectos-entregados section">
    <img class="home-proyectos-entregados__img" src="<?php echo get_template_directory_uri(); ?>/src/img/home-proyecto-entregado-background.png" alt="">
    <div class="container">
        <div class="text-center">
            <div class="supratitle">SELLO DE CALIDAD</div>
            <h2 class="h2">Proyectos entregados</h2>
        </div>
        <div class="text-center">

            <div class="proyectos__slick-2 pt-5">

<?php if ( $proyectos->have_posts() ) { while ( $proyectos->have_posts() ) {$proyectos->the_post();
$featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full');
$inactivo = get_field( 'activar' );

?>
    <?php if ( have_rows( 'previo' ) ) : ?>
        <?php while ( have_rows( 'previo' ) ) : the_row(); ?>
            <?php $distrito = get_sub_field('distrito'); ?>
              <div class="<?php echo $inactivo; ?>">
                <div class="proyectos__slick-el">
                <a href="<?php echo get_permalink(); ?>">
                    <div class="proyectos__slick-el-img-wrapper">
                        <div class="proyectos__slick-el-img-wrapper-inner">
                            <img class="proyectos__slick-el-img" src="<?php echo $featured_img_url ?>" alt="">
                        </div>
                        <h3 class="proyectos__slick-el-title">
                        <?php echo esc_html($distrito->name); ?>
                        </h3>
                    </div>
                  </a>
                    <div class="proyectos__slick-el-body">
                        <div class="proyectos__slick-el-subtitle"> <?php the_title();?> </div>
                        <div class="proyectos__slick-el-address">
                            <?php the_sub_field( 'ubicacion' ); ?>
                        </div>
                        <div class=" proyectos__slick-el-item-wrapper">
                            <div class="">
                                <div class="proyectos__slick-el-item">
                                    <img src="<?php echo get_template_directory_uri(); ?>/src/img/ico-metraje.png" alt="">
                                    <span>Desde <?php the_sub_field( 'area_desde' ); ?></span>
                                </div>
                            </div>
                            <div class="">
                                <div class="proyectos__slick-el-item">
                                    <img src="<?php echo get_template_directory_uri(); ?>/src/img/ico-habitaciones.png" alt="">
                                    <span>  <?php the_sub_field( 'dormitorios' ); ?> dorm.</span>
                                </div>
                            </div>
                        </div>
                        <div class="proyectos__slick-el-price">

                        </div>
                        <div class="text-center">
                            <a href="<?php echo get_permalink(); ?>" class="butn butn-2">Ver Proyecto</a>
                        </div>
                    </div>
                </div>
            </div>
        <?php endwhile; ?>
    <?php endif; ?>

<?php  } } else { _e('No se econtraron resultados','arteco');} wp_reset_postdata(); ?>
            </div>
        </div>
    </div>
</div>
