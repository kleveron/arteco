<!-- Comentarios -->
<?php if ( get_field( 'activar_cta' ) == 1 ) : ?>
<div class="section-t">
  <div class="container container--small">
    <h2 class="h2 mb-5">
      <?php  _e('Comentarios','arteco');?>
    </h2>
  </div>
  <div class="container container--small">

    <?php
       $comments = get_comments(array( 'post_id' => get_the_ID() ) );
        foreach($comments as $comment) :   ?>
        <!-- Comentario -->
        <div class="row align-items-center mb-5">
          <div class="col">
            <div class="d-flex align-items-center color-3 pt-0 pb-3">
              <div class="mr-2 comentario-autor-img">
                <?php echo get_avatar( get_the_author_meta( 'ID'), 72 ); ?>
              </div>
              <div class="pt-2">
                <div class="mb-2">
                  <strong>
                     <?php echo $comment->comment_author;  ?>
                  </strong>
                </div>
              </div>
            </div>
          </div>
          <div class="col">
            <div class="color-3 text-right">
               <?php echo get_comment_date('j \d\e F \d\e Y'); ?>
            </div>
          </div>
          <div class="col-lg-12">
            <?php echo $comment->comment_content; ?>
          </div>
        </div>

    <?php     endforeach;    ?>


  </div>
</div>



<div class="section">
  <div class="container container--small">
    <h2 class="h2 mb-5">
      <?php  _e('Dejanos tu comentarios','arteco');?>
   </h2>
    <div class="row">

        <div class="col-lg-6">
          <form action="<?php echo esc_url( home_url( ) ); ?>/wp-comments-post.php" method="post" id="commentform" class="comment-form" novalidate="">
          <div class="row">
            <div class="col-lg-12">
            <input id="author" name="author" size="30" placeholder="Nombre *" type="text" class="form-control placeholder-9">
             </div>
          </div>
          <div class="row">
            <div class="col-lg-12">
          <input id="email" name="email" ize="30" placeholder="Correo electrónico *" type="email" class="form-control placeholder-9">
        </div>
     </div>
     <div class="row">
       <div class="col-lg-12">
          <textarea name="comment" id="comment" placeholder="Escribe tu comentario *" class="form-control placeholder-9"  cols="30" rows="5"></textarea>
        </div>
      </div>
         <div class="mt-4">
         <input class="butn butn-3" name="submit" type="submit" id="submit" value="Enviar">
         <input type="hidden" name="comment_post_ID" value="<?php echo get_the_ID(); ?>" id="comment_post_ID">
         <input type="hidden" name="comment_parent" id="comment_parent" value="0">
        </div>
     </form>
      </div>
    </div>
  </div>
</div>
<?php endif; ?>
