<?php  ?>
<!-- Planos -->
<?php if ( get_field( 'activar_planos' ) == 1 ) : ?>
<div class="departamentos-interna-planos section">
    <div class="container py-4">
        <div class="text-center">
          <?php if ( have_rows( 'preplanos' ) ) : ?>
            <?php while ( have_rows( 'preplanos' ) ) : the_row(); ?>
            <div class="supratitle">
              	<?php the_sub_field( 'titulo_planos' ); ?>
            </div>
            <h2 class="h3 pb-4">
                <?php the_sub_field( 'subtitulo_planos' ); ?>
            </h2>
          <?php endwhile; ?>
        <?php endif; ?>
       </div>
       <div class="row" id="filtros-dep-internas">
            <div class="col-lg-6">
              <h2 class="h4 pb-4">
                <?php _e('Filtra el depa que deseas','arteco'); ?>
              </h2>
              <form action="" method="POST">
                <div class="row">
                  <div class="col-6">
                    <div class="ar-select mb-4">
                      <select class="form-control ar-select2" name="type" data-minimum-results-for-search="Infinity" placeholder="<?php _e('Tipo','arteco'); ?>">
                        <option value="" hidden><?php _e('Tipo','arteco'); ?></option>
                        <option value="all"> Todos </option>
                        <?php  $types =  ar_get_proyect($post->ID, 'tipo'); ?>
                        <?php if($types):?>
                          <?php  foreach ( $types as $ey => $type ):?>
                            <option value="<?= $type ?>"> Tipo <?= $type ?> </option>
                          <?php endforeach;?>
                        <?php endif;?>
                      </select>
                    </div>
                  </div>
                  <div class="col-6">
                    <div class="ar-select mb-4">
                      <select class="form-control ar-select2" name="view" data-minimum-results-for-search="Infinity" placeholder="<?php _e('Tipo de vista','arteco'); ?>">
                        <option value="" hidden><?php _e('Tipo de vista','arteco'); ?></option>
                        <option value="all"> Todos </option>
                        <?php  $views =  ar_get_proyect($post->ID, 'tipo_de_vista'); ?>
                        <?php foreach ( $views as $key => $view): ?>
                          <option value="<?= $view ?>"><?= $view; ?></option>
                        <?php endforeach;?>
                      </select>
                    </div>
                  </div>
                  <div class="col-6">
                    <div class="ar-select mb-4">
                      <select class="form-control ar-select2" name="bedrooms" data-minimum-results-for-search="Infinity" placeholder="<?php _e('Cant. Dorm ','arteco'); ?>">
                        <option value="" hidden><?php _e('Cant. Dorm ','arteco'); ?></option>
                        <option value="all"> Todos </option>
                        <?php $bedrooms =  ar_get_proyect($post->ID, 'dormitorios'); ?>
                        <?php foreach ( $bedrooms as $key => $bedroom): ?>
                          <option value="<?= $bedroom ?>"><?= $bedroom == 1 ? $bedroom.' Dormitorio' : $bedroom.' Dormitorios'?> </option>
                        <?php endforeach;?>
                      </select>
                    </div>
                  </div>
                  <div class="col-6">
                    <div class="ar-select mb-4">
                      <select class="form-control ar-select2" name="flat" data-minimum-results-for-search="Infinity" placeholder="<?php _e('Pisos ','arteco'); ?>">
                        <option value="" hidden><?php _e('Pisos ','arteco'); ?></option>
                        <option value="all"> Todos </option>
                        <?php $flats =  ar_get_proyect($post->ID, 'pisos'); ?>
                        <?php if( $flats ): ?>
                            <?php foreach ( $flats as $key => $flat): ?>
                              <option value="<?= $flat ?>"><?= $flat; ?> Piso </option>
                            <?php endforeach; ?>
                          <?php endif; ?>
                      </select>
                    </div>
                  </div>
                </div>
              </form>
            </div>
            <div class="col-lg-6">
                <div class="room-distribution">
                    <div class="row justify-content-center align-items-center">
                        <div class="col-lg-3 text-center d-none d-lg-block">
                            <img src="<?php echo get_template_directory_uri(); ?>/src/img/triple-arrow.png" alt="">
                        </div>
                        <div class="col text-center" id="image-preview">
                          <?php if( get_field( 'preview_plant' )  ): ?>
                            <?php $default = wp_get_attachment_image(get_field( 'preview_plant' ), 'full', false, array('class'=>'img-fluid' )) ?>
                          <?php else: ?>
                            <?php $default = '<img src="'.get_template_directory_uri().'/src/img/room-distribution.png" alt="">'; ?>
                          <?php endif; ?>
                          <?= $default; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div>
          <h2 class="h4 py-4"> Resultado Disponible </h2>
          <div class="filter-resultado" id="filtros-lista" icon="<?= get_template_directory_uri(); ?>/src/img/equis.png">
            <!-- <div class="filter-resultado-delete">
              <a href="#resultado-delete">Borrar filtros</a>
            </div> -->
          </div>

         <div class="departamentos-interna-slider">
         <div class="text-center">
           <div id="planos" class="departamentos-interna-slider__slick pt-5" >
               <?php if ( have_rows( 'planos' ) ) : ?>
               <?php while ( have_rows( 'planos' ) ) : the_row();
                $tipo = get_sub_field( 'tipo' );
                $tipo_de_vista = get_sub_field( 'tipo_de_vista' );
                $pisos = get_sub_field( 'pisos' );
                $imagen = get_sub_field( 'imagen' );
                $depa = get_sub_field( 'depa' );
                $dormitorios = get_sub_field( 'dormitorios' );
                $banos = get_sub_field( 'banos' );
                $precio = get_sub_field( 'precio' );
                //$medida = get_sub_field( 'medida' );
                $medida = get_sub_field( 'footage' );
                $enabled = get_sub_field( 'enabled' );
			   $plant = get_sub_field( 'plant' );
                if($enabled) :
               ?>
              <div class="planos-itm tipo-<?= $tipo;?>  dorm-<?= $dormitorios;?>  vista-<?= $tipo_de_vista;?>  pisos-<?= $pisos;?>"
              data-type="<?= $tipo;?>"
              data-view="<?= $tipo_de_vista;?>"
              data-bedrooms="<?= $dormitorios; ?>"
              data-flat="<?= $pisos; ?>"
			  data-plant="<?= $plant ? esc_html(wp_get_attachment_image($plant, 'full', false, array('class'=>'img-fluid' )) ) : esc_html($default); ?>" >
               <div class="proyectos__slick-el">
                 <div class="proyectos__slick-el-img-wrapper">
					<a href="#" data-toggle="modal" data-target="#modal-depto-<?php echo $depa;?>" class="">
                   		<img class="proyectos__slick-el-img" src="<?php echo $imagen; ?>" alt="">
					 </a>
                 </div>
                 <div class="proyectos__slick-el-body">
                   <div class="proyectos__slick-el-subtitle">
                     Depto. <?php echo $depa; ?>
                   </div>
                   <div class="proyectos__slick-el-item-wrapper">
                     <div class="proyectos__slick-el-item pb-2">
                       <img src="<?php echo get_template_directory_uri(); ?>/src/img/ico-metraje.png" alt="">
                       <span><?php echo $dormitorios." dorm / ".$banos." baños" ?>
                       </span>
                     </div>
                     <div class="row">
                       <div class="col">
                         <div class="proyectos__slick-el-item">
                           <img src="<?php echo get_template_directory_uri(); ?>/src/img/ico-habitaciones.png" alt="">
                           <span><?= $medida; ?> m2</span>
                         </div>
                       </div>
                       <div class="col">
                         <a href="#" data-toggle="modal" data-target="#modal-depto-<?php echo $depa;?>" class="proyectos__slick-el-item proyectos__slick-el-item--link">
                           <img src="<?php echo get_template_directory_uri(); ?>/src/img/ico-eye-gray.png" alt="">
                           <img src="<?php echo get_template_directory_uri(); ?>/src/img/ico-eye-orange.png" alt="">
                           <span> V. <?= ucfirst( substr($tipo_de_vista, 0, 3)); ?>.</span>
                         </a>
                       </div>
                     </div>
                   </div>
                   <div class="proyectos__slick-el-price">
                     <span class="proyectos__slick-el-price-number">
                       S/ <?php echo number_format("$precio",2,",","."); ?>
                      </span>
                   </div>
                 </div>
               </div>
              </div>
              <?php endif; ?>
              <?php endwhile; endif; ?>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>


<?php endif; ?>
