<!-- Saber más -->
<?php if ( get_field( 'activar_cta' ) == 1 ) : ?>
  <?php if ( have_rows( 'cta' ) ) : ?>
	<?php while ( have_rows( 'cta' ) ) : the_row(); ?>
 <div class="section bg-color-2 saber-mas">
    <div class="container container--small">
      <div class="row">
        <div class="col-lg-5">
          <?php if ( get_sub_field( 'imagen' ) ) : ?>
            <img class="img-fluid saber-mas__img" src="<?php the_sub_field( 'imagen' ); ?>" alt="">
          <?php endif ?>
        </div>
        <div class="col-lg-7">
          <h3 class="h3 color-w">
            <?php the_sub_field( 'titulo' ); ?><br>
            <span class="color-1"><?php the_sub_field( 'precio' ); ?></span>
          </h3>
          <div class="mt-3 mb-4 color-w">
            <?php the_sub_field( 'descripcion' ); ?>
         </div>
          <div>
            <?php if (get_sub_field('texto_de_boton')){  ?>
             <a href="<?php the_sub_field( 'link' ); ?>" class="butn butn-1">	<?php the_sub_field( 'texto_de_boton' ); ?></a>
              <?php } ?>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php endwhile; ?>
<?php endif; ?>
<?php endif; ?>
