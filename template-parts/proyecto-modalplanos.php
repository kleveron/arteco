<?php

?>
<!-- Modal -->

<?php if ( get_field( 'activar_planos' ) == 1 ) : ?>

  <?php if ( have_rows( 'planos' ) ) : ?>
  <?php while ( have_rows( 'planos' ) ) : the_row();
   $tipo = get_sub_field( 'tipo' );
   $tipo_de_vista = get_sub_field( 'tipo_de_vista' );
   $pisos = get_sub_field( 'pisos' );
   $imagen = get_sub_field( 'imagen' );
   $depa = get_sub_field( 'depa' );
   $dormitorios = get_sub_field( 'dormitorios' );
   $banos = get_sub_field( 'banos' );
   $precio = get_sub_field( 'precio' );
   //$medida = get_sub_field( 'medida' );
   $medida = get_sub_field( 'footage' );
   $areas_comunes = get_sub_field('areas_comunes');
   ?>
<div class="modal fade modal-card" id="modal-depto-<?php echo $depa; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-card-el">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <img src="<?php echo get_template_directory_uri(); ?>/src/img/equis.png" alt="">
                </button>
                <div class="modal-card-inner">
                    <div class="modal-card__img">
                        <img class="img-fluid" src="<?php echo $imagen; ?>" alt="">
                    </div>
                    <div class="modal-card__body">
                        <h2 class="h2">
                            Departamento <?php echo $depa; ?>
                        </h2>
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="modal-card__item">
                                    <img src="<?php echo get_template_directory_uri(); ?>/src/img/ico-habitaciones.png" alt="">
                                    <span><?php echo $dormitorios; ?> domitorio / <?php echo $banos; ?> baños</span>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="modal-card__item">
                                    <img src="<?php echo get_template_directory_uri(); ?>/src/img/ico-metraje.png" alt="">
                                    <span>Área total: <?php echo $medida; ?> m2</span>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="modal-card__item">
                                    <img src="<?php echo get_template_directory_uri(); ?>/src/img/ico-eye-gray.png" alt="">
                                    <span>Vista <?php echo $tipo_de_vista; ?></span>
                                </div>
                            </div>
                        </div>
                        <div class="modal-card__body-bottom">
                            <p><?php echo $areas_comunes; ?></p>
                            <div class="row align-items-center">
                                <div class="col">
                                    <span>Precio:</span> <strong> S/ <?php echo number_format("$precio",2,",","."); ?></strong>
                                </div>
                                <div class="col text-md-right">
                                    <button class="butn butn-1 ar-close-modal-plans" bedrooms="<?= $dormitorios; ?>" flat="<?= $pisos; ?>" view="<?= $tipo_de_vista; ?>" depto="<?= $depa; ?>" ><?php _e('Solicita información','arteco') ?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endwhile; endif; ?>
<?php endif; ?>
