<?php

if ( have_rows( 'previo' ) ) :
    while ( have_rows( 'previo' ) ) : the_row();
        $dormitorios = get_sub_field( 'dormitorios' );
        $ubicacion = get_sub_field( 'ubicacion' );
        $preciodesde = get_sub_field( 'precio_desde' );
        $preciohasta = get_sub_field( 'precio_hasta' );
    endwhile;
endif;

 if ( have_rows( '360' ) ) :
     while ( have_rows( '360' ) ) : the_row();
       $pretitulo360 = get_sub_field( 'pretitulo' );
       $titulo360 = get_sub_field( 'titulo' );
       $iframe360 = get_sub_field( 'iframe' );
    endwhile;
 endif; 
?>
<!-- 360 -->

<div class="departamentos-interna-360 section">
    <div class="container">


        <div class="departamento-interna-dsc">
            <div class="departamento-interna-dsc-el">
                <div class="departamento-interna-dsc-el-op">
                    <div class="departamento-interna-dsc-el-op-inner">
                        <img src="<?php echo get_template_directory_uri(); ?>/src/img/ico-market-black.png" alt="">
                        <div class="departamento-interna-dsc-el-op-dsc">
                            <div class="departamento-interna-dsc-el-op-supratitle">
                              <?php  _e('Ubicación','arteco'); ?>
                            </div>
                            <div class="departamento-interna-dsc-el-op-title color-1">
                                <?php echo $ubicacion?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="departamento-interna-dsc-el-op">
                    <div class="departamento-interna-dsc-el-op-inner">
                        <img src="<?php echo get_template_directory_uri(); ?>/src/img/ico-room-black.png" alt="">
                        <div class="departamento-interna-dsc-el-op-dsc">
                            <div class="departamento-interna-dsc-el-op-supratitle">
                            <?php   _e('Dormitorios','arteco'); ?>
                            </div>
                            <div class="departamento-interna-dsc-el-op-title">
                               <?php echo $dormitorios;?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="departamento-interna-dsc-el-op">
                    <div class="departamento-interna-dsc-el-op-inner">
                        <img src="<?php echo get_template_directory_uri(); ?>/src/img/ico-ticket-price-black.png" alt="">
                        <div class="departamento-interna-dsc-el-op-dsc">
                            <div class="departamento-interna-dsc-el-op-supratitle">
                                Precio
                            </div>
                            <div class="departamento-interna-dsc-el-op-title">
                                S/ <?php echo $preciodesde;?> - S/ <?php echo $preciohasta; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php if ( get_field( 'activar_360' ) == 1 ) : ?>
    <div class="container py-4">
        <div class="text-center">
            <div class="supratitle">
                <?php echo $pretitulo360;?>
            </div>
            <h2 class="h3 pb-4">
                <?php echo $titulo360;?>
            </h2>
        </div>
        <div class="text-center">
            <?php if($iframe360){   ?>
              <iframe src="<?php echo $iframe360;?>" width="1100" height="500"></iframe>
            <?php } else { ?>
            <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/src/img/img-360.png" alt="">
            <?php  } ?>
        </div>
    </div>
    <?php else : ?>

    <?php endif; ?>
</div>
