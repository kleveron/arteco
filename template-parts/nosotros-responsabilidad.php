
    <!-- Resposabilidad Social -->
<?php if ( get_field( 'activar_responsabilidad' ) == 1 ) : ?>
    <div class="section">
      <div class="container">
        <div class="supratitle text-center">
        <?php the_field( 'pretitulo_responsabilidad' ); ?>
        </div>
        <h2 class="h3 text-center">
        <?php the_field( 'titulo_responsabilidad' ); ?>
        </h2>
      </div>
      <div class="container">
        <div class="row pt-5 mt-5">
          <?php if ( have_rows( 'responsabilidad' ) ) : ?>
	           <?php while ( have_rows( 'responsabilidad' ) ) : the_row(); ?>
          <div class="col-lg-6">
            <?php the_sub_field( 'texto' ); ?>
           </div>
            <div class="col-lg-6">
              <?php if ( get_sub_field( 'foto' ) ) : ?>
                  <img class="img-fluid" src="<?php the_sub_field( 'foto' ); ?>" alt="">
              <?php endif ?>
           </div>
        <?php endwhile; ?>
      <?php endif; ?>
        </div>
      </div>
    </div>
<?php endif; ?>
