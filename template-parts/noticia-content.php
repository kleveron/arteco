<!-- Categoría -->

 <div class=" section" >
  <div class="container container--small">
    <div>
      <h2 class="h2 mb-5">
         <?php the_title(); ?>
      </h2>
    </div>
    <div>
    <?php if (has_post_thumbnail( $post->ID ) ): ?>
      <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
      <img src="<?php echo $image[0]; ?>" alt="" class="img-fluid">

    <?php endif; ?>
    </div>
    <div class="mt-4">
      <div class="row">
        <div class="col">
          <div class="d-flex color-3 pt-0 pb-3">
            <div class="mr-2">
              <?php echo get_avatar( get_the_author_meta('user_email') , 60, '', '', array('class'=>'rounded-circle') ); ?>
            </div>
            <div class="pt-2">
              <div class="mb-2">
                <strong>
                  <?php $nombre = get_the_author_meta( 'display_name', $post->post_author ); ?>
                  <?php echo $nombre; ?>
                </strong>
              </div>
              <div>
                <?php $descriccion = get_the_author_meta( 'user_description', $post->post_author ); ?>
                <?php echo $descriccion; ?>
              </div>
            </div>
          </div>
        </div>
        <div class="col">
          <div class="color-3 text-right">
                <?php echo get_the_date(); ?>
          </div>

        </div>
      </div>
    </div>
    <div class="post-content pt-4">
      <?php the_content(); ?>
        <?php if ( get_field( 'activar_carrusel' ) ) : ?>

          <div class="container container--extra-small">
            <div class="slider-ss__slick slider-ss__blog">
              <?php
              if( have_rows('carrusel') ):
                while( have_rows('carrusel') ) : the_row(); ?>
              <div>
                <div class="slider-ss__el">
                  <div class="slider-ss__el-img">
                    <?php if ( get_sub_field('imagen')): ?>
                          <img src="<?php the_sub_field('imagen'); ?>" alt="">
                    <?php endif; ?>
                  </div>
                    <?php if ( get_sub_field('video')): ?>
                    <div class="slider-ss__el-play video-btn" data-toggle="modal" data-src="<?php the_sub_field('video'); ?>" data-target="#modalVideo">
                    <img src="<?php echo get_template_directory_uri(); ?>/src/img/play.png" alt="">  </div>
                    <?php endif; ?>

                </div>
              </div>
                <?php    endwhile;   endif;  ?>

            </div>
          </div>
        <?php endif; ?>
      <?php if( get_field( 'activar_contenido_adicional' ) ): ?>
       <?php if ( have_rows( 'contenido_adicional' ) ) : ?>
          <?php while ( have_rows( 'contenido_adicional' ) ) : the_row(); ?>
            <h2 class="h2 mb-5 pt-4"> <?php the_sub_field( 'titulo' ); ?></h2>
            <p><?php the_sub_field( 'texto' ); ?></p>

            <div class="container container--extra-small">
              <div class="slider-ss__blog">
                <div class="slider-ss__el">
                  <div class="slider-ss__el-img">
                      <?php if ( get_sub_field( 'imagen_de_video' ) ) : ?>
                    <img src="<?php the_sub_field( 'imagen_de_video' ); ?>" alt="">
                      <?php endif ?>
                  </div>
                <?php if ( get_sub_field( 'video' ) ) : ?>
                  <div class="slider-ss__el-play video-btn" data-toggle="modal" data-src="<?php the_sub_field('video'); ?>" data-target="#modalVideo">
                    <img src="<?php echo get_template_directory_uri(); ?>/src/img/play.png" alt="">
                  </div>
                <?php endif ?>
                    <div class="slider-ss__el-zoom zoom-btn" data-toggle="modal" data-src="<?php the_sub_field( 'imagen_de_video' ); ?>" data-target="#modalZoom">
                    <img src="<?php echo get_template_directory_uri(); ?>/src/img/two-arrows.png" alt="">
                  </div>
                </div>
              </div>
            </div>
     	    <?php endwhile; ?>
        <?php endif; ?>
      <?php endif; ?>




    </div>
  </div>
</div>
