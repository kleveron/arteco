<!-- Artículos más leídos -->
<?php if ( get_field( 'activar_mas_leidos' ) == 1 ) :

  $args = array(
  	'post_type'    => 'post',
    'meta_key'  => 'wp_post_views_count', // set custom meta key
    'orderby'    => 'meta_value_num',
    'order'      => 'DESC',
    'post_status'  => 'publish',
    'posts_per_page' => 4
  );

$noticia_masleidos = new WP_Query( $args );



  ?>
<div class="section" >
  <div class="container">
    <div>
      <h2 class="h2"><?php _e('Artículos más leídos','arteco'); ?></h2>
    </div>
    <div class="text-center">
      <div class="row proyectos__no-slick">
     <?php  while ( $noticia_masleidos->have_posts() ) :
             $noticia_masleidos->the_post();
             $fecha = get_the_date( 'j \d\e F \d\e Y', $noticia_masleidos->ID );
             $featured_img_url = get_the_post_thumbnail_url($noticia_masleidos->ID, 'full');
             $categories = get_the_category();
             $autor = $noticia_masleidos->post_author;
             $autor_nombre =  get_the_author_meta('display_name',$autor);
             $autor_descripcion =  get_the_author_meta('description',$autor);
             $autor_foto = get_avatar_url($autor);

             ?>
        <div class="col-lg-6">
          <div class="custom-card text-left my-4">
            <div class="custom-card__img-wrapper">
              <img class="custom-card__img img-fluid" src="<?php echo get_template_directory_uri(); ?>/src/img/blog-noticia-leida-1.png" alt="">
              <h3 class="custom-card__title h3 ln-h3">
                  <?php echo the_title(); ?>
              </h3>
              <div class="custom-card__sticker"><?php if ( ! empty( $categories ) ) { echo esc_html( $categories[0]->name );  } ?></div>
            </div>
            <div class="color-3">
              <div class="px-3">
                <div class="row align-items-center">
                  <div class="col-lg-7">
                    <div class="color-3 pb-3 pt-3">
                       <?php echo $fecha; ?>
                    </div>
                    <div class="d-flex color-3 pt-0 pb-3">
                      <div class="mr-2">
                      <?php 
                        $author = get_the_author_meta('ID');
                        $picture = get_avatar_url($author, array('size' => 450));
                      ?>
                      <img class="autor_foto" src="<?= $picture; ?>" alt="">
                      </div>
                      <div class="pt-2">
                        <div class="mb-2">
                          <strong>
                           <?php echo $autor_nombre; ?>
                          </strong>
                        </div>
                        <div>
                         <?php echo $autor_descripcion; ?>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-5">
                    <div class="text-center">
                      <a href="<?php the_permalink(); ?>" class="butn butn-1"><?php _e('Ver más','arteco'); ?></a>
                    </div>
                  </div>
                </div>

              </div>
            </div>
          </div>
        </div>
     <?php  endwhile;     wp_reset_query();   ?>


      </div>
    </div>
  </div>
</div>
<?php endif; ?>
