  <!-- Reconocimientos -->
<?php if ( get_field( 'activar_reconocimientos' ) == 1 ) : ?>
  <div class="section">
    <div class="container">
      <div class="supratitle text-center">
       <?php the_field( 'pretitulo_reconocimientos' ); ?>
      </div>
      <h2 class="h3 text-center">
      <?php the_field( 'titulo_reconocimientos' ); ?>
      </h2>
    </div>
    <div class="container">
        <?php if( have_rows('reconocimientos') ): ?>
      <div class="row">
        <?php  while( have_rows('reconocimientos') ) : the_row();?>
            <?php  $logo = get_sub_field('logo');
                   $texto = get_sub_field('texto'); ?>
        <div class="col-lg-3 text-center my-5">
          <img src="<?php echo $logo; ?>" alt="">
          <div class="my-3">
             <?php echo $texto; ?>
          </div>
        </div>

        <?php  endwhile;  ?>
      </div>
         <?php endif; ?>
    </div>
  </div>
<?php endif; ?>
