<?php while ( have_posts() ) :  the_post();
 if ( get_field( 'activar_blog_widget' ) == 1 ) : ?>

<div class="home-noticias section background-bottom" style="background-image: url('<?php echo get_template_directory_uri(); ?>/src/img/home-noticia-background.png')">
    <div class="container">
      <?php if ( have_rows( 'blog_widget' ) ) : ?>
	       <?php while ( have_rows( 'blog_widget' ) ) : the_row(); ?>
		     <div class="text-center">
            <div class="supratitle">
              <?php the_sub_field( 'pretitulo' ); ?>
            </div>
            <h2 class="h2">
              	<?php the_sub_field( 'titulo' ); ?>
            </h2>
        </div>
          <?php endwhile; ?>
      <?php endif; ?>
        <div class="text-center">
            <div class="noticias__slick pt-5">
              <!-- Noticias -->
              <?php
              $args = array(
              	'post_type'   => 'post',
              );
              $noticias = new WP_Query( $args );

              if ( $noticias->have_posts() ) :
              ?>
              	<?php while( $noticias->have_posts() ) : $noticias->the_post() ?>

                  <div>
                      <div class="proyectos__slick-el">
                          <div class="proyectos__slick-el-inner">
                              <div class="proyectos__slick-el-img-wrapper">
                                <?php the_post_thumbnail( 'full' ); ?>
                             
                              </div>
                              <div class="proyectos__slick-el-body">
                                  <a href="<?php the_permalink(); ?>" class="proyectos__slick-el-subtitle">
                                      <?php the_title(); ?>
                                  </a>
                              </div>
                          </div>
                      </div>
                  </div>
             <?php endwhile ?>
              <?php else : ?>
               <p>No existen noticias publicadas</p>
              <?php endif ?>
              </div>
        </div>
    </div>
</div>

<?php endif;endwhile; ?>
