<!-- Valores -->
<?php if ( get_field( 'activar_valores' ) == 1 ) : ?>
<div class="section">
  <div class="container">
    <div class="supratitle text-center">
      <?php the_field( 'pretitulo_valores' ); ?>
    </div>
    <h2 class="h3 text-center">
      <?php the_field( 'titulo_valores' ); ?>
    </h2>
  </div>
  <div class="container valores-section">
    <?php if ( have_rows( 'valores' ) ) : ?>
	<?php while ( have_rows( 'valores' ) ) : the_row(); ?>
    <div class="row">
      <?php if ( have_rows( 'valor_1' ) ) : ?>
        <?php while ( have_rows( 'valor_1' ) ) : the_row(); ?>
       <div class="col-lg-3 text-center my-5">
         <?php if ( get_sub_field( 'icono' ) ) : ?>
 					<img src="<?php the_sub_field( 'icono' ); ?>" />
 				<?php endif ?>
        <div class="my-3">
          <strong> <?php the_sub_field( 'titulo' ); ?></strong>
        </div>
        <div> <?php the_sub_field( 'descripcion' ); ?> </div>
      </div>
        <?php endwhile; ?>
        <?php endif; ?>
       <?php if ( have_rows( 'valor_2' ) ) : ?>
          <?php while ( have_rows( 'valor_2' ) ) : the_row(); ?>
       <div class="col-lg-3 text-center my-5">
         <?php if ( get_sub_field( 'icono' ) ) : ?>
 					<img src="<?php the_sub_field( 'icono' ); ?>" />
 				<?php endif ?>
        <div class="my-3">
          <strong>    <?php the_sub_field( 'titulo' ); ?> </strong>
        </div>
        <div> <?php the_sub_field( 'descripcion' ); ?> </div>
      </div>
          <?php endwhile; ?>
          <?php endif; ?>
          <?php if ( have_rows( 'valor_3' ) ) : ?>
          <?php while ( have_rows( 'valor_3' ) ) : the_row(); ?>
     <div class="col-lg-3 text-center my-5">
       <?php if ( get_sub_field( 'icono' ) ) : ?>
         <img src="<?php the_sub_field( 'icono' ); ?>" />
       <?php endif ?>
        <div class="my-3">
          <strong>  <?php the_sub_field( 'titulo' ); ?></strong>
        </div>
        <div> <?php the_sub_field( 'descripcion' ); ?> </div>
      </div>
        <?php endwhile; ?>
        <?php endif; ?>
        <?php if ( have_rows( 'valor_4' ) ) : ?>
        <?php while ( have_rows( 'valor_4' ) ) : the_row(); ?>
       <div class="col-lg-3 text-center my-5">
         <?php if ( get_sub_field( 'icono' ) ) : ?>
 					<img src="<?php the_sub_field( 'icono' ); ?>" />
 				<?php endif ?>
        <div class="my-3">
          <strong><?php the_sub_field( 'titulo' ); ?></strong>
        </div>
        <div><?php the_sub_field( 'descripcion' ); ?> </div>
      </div>
    <?php endwhile; ?>
    <?php endif; ?>
    </div>
  <?php endwhile; ?>
  <?php endif; ?>
  </div>
</div>
<?php endif; ?>
