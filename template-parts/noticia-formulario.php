<!-- Formulario de contacto -->
<?php if ( get_field( 'activar_formulario' ) == 1 && get_field( 'post_proyect' ) ) : ?>
    <div class="contact-post-form  contacto" style="background-image: url(<?php echo get_template_directory_uri(); ?>/src/img/img-contact.png)">
      <div class="container">
        <div class="contacto-el">
          <?php if ( have_rows( 'formulario' ) ) : ?>
          	<?php while ( have_rows( 'formulario' ) ) : the_row(); ?>
              <div class="row">
                <div class="col-lg-5">
                  <h4 class="h3">
                    <?php the_sub_field( 'titulo' ); ?>
                  </h4>
                  <div class="contacto__img-2">
                        <?php if ( get_sub_field( 'imagen' ) ) : ?>
                        <img src="<?php the_sub_field( 'imagen' ); ?>" />
                        <?php endif ?>
                  </div>
                </div>
                <div class="col-lg-7">
                  <div class="contacto-el-form">
                    <form class="contacto-el-form" method="POST">
                      <?php $proyect = get_field( 'post_proyect' ); ?>
                      <input type="hidden" name="action" value="ar_ajax_contact">
                      <input type="hidden" name="page" value="<?= $post->ID; ?>">
                      <input type="hidden" name="proyect" value="<?= $proyect; ?>">
                      <input type="hidden" name="url" value="<?= ar_url_server( $_SERVER ); ?>">
                      <input type="hidden" name="type" value="proyect_post">
                      <div class="row">
                        <div class="col-lg-6">
                          <input placeholder="Nombre *" name="firstname" type="text" class="form-control" required>
                        </div>
                        <div class="col-lg-6">
                          <input placeholder="Apellido *" name="lastname"  type="text" class="form-control" required>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-lg-6">
                          <input placeholder="DNI *" name="dni" type="number" class="form-control ar-input-number" onkeypress="return event.keyCode === 8 || event.charCode >= 48 && event.charCode <= 57" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength = "8" required>
                        </div>
                        <div class="col-lg-6">
                          <input placeholder="Teléfono" name="phone" type="number" class="form-control ar-input-number" onkeypress="return event.keyCode === 8 || event.charCode >= 48 && event.charCode <= 57" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength = "9" required>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-lg-12">
                          <input placeholder="Correo electrónico *" name="email" type="email" class="form-control" required>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-lg-12">
                          <div class="ar-select mb-4">
                            <?php $hours =  ar_get_hour('9:00', '17:00' , 30 ); ?>
                            <select class="form-control ar-select2 ar-select2-proyects ar-select2-hour" name="hour" data-minimum-results-for-search="Infinity" placeholder="Selecciona una hora de contacto" required>
                              <option value="" hidden> Selecciona una hora de contacto </option>
                              <?php if( $hours ): ?>
                                <?php foreach ( $hours as $key => $hour): ?>
                                  <option value="<?= $hour ?>"><?= $hour ?></option>
                                <?php endforeach; ?>
                              <?php endif; ?>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-lg-12 mb-4">
                          <div class="ar-date-piker  position-relative">
                            <div class="position-relative ar-picker-wrap">
                              <input type="text" class="form-control ar-picker" name="date" placeholder="Agendar una cita para ver piloto (opcional)" autocomplete="off" readonly >
                              <button type="button" class="ar-picker-trigger"></button>
                            </div>
                            <div class="ar-picker-container"></div>
                          </div>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-lg-12">
                          <div class="d-block text-left my-2 custom-control custom-checkbox ">
                            <?php
                              $page = '';
                              $page2 = '';
                              if( have_rows('admin_terms', 'option') ):
                                while( have_rows('admin_terms', 'option') ): the_row();
                                  $page  = get_sub_field('admin_necessary_data_policies');
                                  $page2 = get_sub_field('admin_additional_data_policies');
                                endwhile;
                              endif;
                              $terms = 'He leido y acepto los términos y condiciones de la "Politicas de datos personales para fines necesarios".';
                              if($page):
                                $terms = 'He leido y acepto los términos y condiciones de la <a href="'.get_the_permalink($page->ID).'" target="_blank" rel="noopener noreferrer">"Politicas de datos personales para fines necesarios".</a>';
                              endif;
                            ?>
                            <input type="checkbox" class="custom-control-input" id="cbox-terminos" name="terms" value="on" required checked>
                            <label for="cbox-terminos" class="custom-control-label form-text"><?= $terms; ?></label>
                          </div>
                        </div>
                        <div class="col-lg-12">
                          <?php

                            $terms_ext = 'He leido y acepto los términos y condiciones de la "Politicas de datos personales para fines adicionales".';
                            if($page2):
                              $terms_ext = 'He leido y acepto los términos y condiciones de la <a href="'.get_the_permalink($page2->ID).'" target="_blank" rel="noopener noreferrer">"Politicas de datos personales para fines adicionales".</a>';
                            endif;
                          ?>
                          <div class="d-block text-left my-2 custom-control custom-checkbox ">
                            <input type="checkbox" class="custom-control-input" id="cbox-autorizo" name="terms_extra" value="on" >
                            <label for="cbox-autorizo" class="custom-control-label form-text"><?= $terms_ext; ?></label>
                          </div>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-lg-12 text-center pt-4">
                          <button class="butn butn-1 ar-send-contact-post">Enviar</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
          	<?php endwhile; ?>
          <?php endif; ?>
        </div>
      </div>
    </div>

<?php endif; ?>


<!-- Gracias -->

<div id="gracias_form" class="contacto" style="display:none;background-image: url(<?php echo get_template_directory_uri(); ?>/src/img/img-contact.png)">
  <div class="container">
    <div class="contacto-el">

      <?php if ( have_rows( 'formulario' ) ) : ?>
        <?php while ( have_rows( 'formulario' ) ) : the_row(); ?>
      <?php if ( have_rows( 'gracias' ) ) : ?>
         <?php while ( have_rows( 'gracias' ) ) : the_row(); ?>
           <?php the_sub_field( 'titulo' ); ?>
           <?php the_sub_field( 'descripcion' ); ?>
           <?php if ( get_sub_field( 'icono' ) ) : ?>
             <img src="<?php the_sub_field( 'icono' ); ?>" />
           <?php endif ?>
           <?php if ( get_sub_field( 'brochure' ) ) : ?>
             <a href="<?php the_sub_field( 'brochure' ); ?>" download>Download File</a>
           <?php endif; ?>
           <?php the_sub_field( 'post_descripcion' ); ?>
           <?php // Upgrade to ACF Theme Code Pro for repeater field support. ?>
           <?php // Visit http://www.hookturn.io for more information. ?>
         <?php endwhile; ?>
       <?php endif; ?>
      <div class="row">
        <div class="col-lg-6">
          <h4 class="h3">
            Felicidades <span class="color-1 name-thank"></span> ya casi terminamos.</span>
          </h4>
          <div class="contacto__img-2">
            <img src="<?php echo get_template_directory_uri(); ?>src/img/contacto-img-edificio.png" alt="">
          </div>
        </div>
        <div class="col-lg-6">
          <h4 class="h3">
            Gracias por dejar sus datos, ahora puedes descargar esto para ti
          </h4>
          <div class="contacto__dsc">
            Es un proceso que une herramientas de las finanzas y el coaching para enfocarlo en el aspecto económico personal, profundiza.
          </div>
          <div class="contacto__butn">
            <a href="#" class="contacto__butn-el">
              <img src="<?php echo get_template_directory_uri(); ?>src/img/ico-brochure.png" alt="">
              <span>Descargar brochure</span>
            </a>
          </div>
          <div class="text-center pt-4">
            También puedes seguirnos en nuestras redes <br> sociales
          </div>
          <div class="text-center contacto__socials">
            <a href="#"><img src="<?php echo get_template_directory_uri(); ?>src/img/ico-facebook-2.svg" alt=""></a>
            <a href="#"><img src="<?php echo get_template_directory_uri(); ?>src/img/ico-linkedin-2.svg" alt=""></a>
            <a href="#"><img src="<?php echo get_template_directory_uri(); ?>src/img/ico-instagram-2.svg" alt=""></a>
            <a href="#"><img src="<?php echo get_template_directory_uri(); ?>src/img/ico-youtube-2.svg" alt=""></a>
            <a href="#"><img src="<?php echo get_template_directory_uri(); ?>src/img/ico-twitter-2.svg" alt=""></a>
          </div>
        </div>
      </div>
    <?php endwhile; ?>
    <?php endif; ?>
    </div>
  </div>
</div>
