<?php

?>

<!-- Brochure -->
<?php if ( get_field( 'activar_brochure' ) == 1 ) : ?>
<div class="cuadro section">
    <div class="container">
        <div class="cuadro__marco">
          <?php if ( have_rows( 'brochure' ) ) : ?>
            <?php while ( have_rows( 'brochure' ) ) : the_row(); ?>
                  <div class="row">
                            <div class="col-lg-8">
                              <?php the_sub_field( 'texto' ); ?>
                            </div>
                  <div class="col-lg-4">
                     <a href="#form-contacto" class="butn butn-1"><?php echo _e('Descargar brochure','arteco');?></a>
                    <?php if ( get_sub_field( 'brochure' ) ) : ?>
                            <!-- <a href="<?php /*the_sub_field( 'brochure' ); */?>">Download File</a> -->
                    <?php endif; ?>

                  </div>
                  </div>
                <?php endwhile; ?>
      <?php endif; ?>


        </div>
    </div>
</div>
<?php else : ?>
	<?php // echo 'false'; ?>
<?php endif; ?>
