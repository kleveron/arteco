
  <!-- Quienes somos -->

  <?php if ( get_field( 'activar_quienes_somos' ) == 1 ) : ?>
  <div class="section-t">
    <div class="container">

        <?php if ( have_rows( 'quienes_somos' ) ) : ?>
        	<?php while ( have_rows( 'quienes_somos' ) ) : the_row(); ?>
     <div class="row">
          <div class="col-lg-6">
            <div class="supratitle">
              		<?php the_sub_field( 'pretitulo_quieres_somos' ); ?>
            </div>
            <h2 class="h3">
            	<?php the_sub_field( 'titulo_quieres_somos' ); ?>
            </h2>
            <div class="mt-1">
              <?php the_sub_field( 'descripcion_quieres_somos' ); ?>
            </div>
          </div>
          <div class="col-lg-6 pt-4">
            <div class="slider-ss__slick-2" data-id="q1">
              <?php
              if( have_rows('slider_quienes_somos') ):
                  while( have_rows('slider_quienes_somos') ) : the_row(); ?>
                  <?php
                      $imagen_slider = get_sub_field('imagen');
                      $titulo_slider = get_sub_field('titulo');
                      $subtitulo_slider = get_sub_field('subtitulo');
                      $video_slider = get_sub_field('video'); ?>
              <div>
                <div class="slider-ss__el slider-ss__el--2">
                  <div class="slider-ss__el-img">
                    <img src="<?php echo $imagen_slider; ?>" alt="">
                  </div>
                  <?php if ($video_slider){ ?>
                    <div class="slider-ss__el-play video-btn" data-toggle="modal" data-src="<?php echo $video_slider; ?>" data-target="#modalVideo">
                    <img src="<?php echo get_template_directory_uri(); ?>/src/img/play.png" alt="">
                    </div>
                  <?php } ?>
                <?php if ($titulo_slider!=null || $subtitulo_slider!=null ) { ?>
                 <div class="slider-ss__msg">
                    <div class="slider-ss__msg-title">
                      <?php echo $titulo_slider; ?>
                    </div>
                    <div class="slider-ss__msg-dsc">
                      <?php echo $subtitulo_slider; ?>
                    </div>
                  </div>
                  <?php } ?>
                </div>
              </div>
            <?php endwhile; ?>
          <?php endif; ?>

            </div>
            <div class="slider-ss__slick-dots slider-ss__slick-dots-q1"></div>
          </div>
        </div>
  <?php      endwhile;   endif;  ?>
  <?php if ( have_rows( 'mision' ) ) : ?>
  	<?php while ( have_rows( 'mision' ) ) : the_row(); ?>
  		   <div class="row pt-5 mt-5">
          <div class="col-lg-6">
            <h2 class="h3">
              <?php the_sub_field( 'titulo' ); ?>
            </h2>
            <div class="mt-1">
              <?php the_sub_field( 'descripcion' ); ?>
            </div>
          </div>
          <div class="col-lg-6 pt-4">
            <?php if (get_sub_field( 'imagen' ) ) : ?>
			           <img class="img-fluid" src="<?php the_sub_field( 'imagen' ); ?>" alt="">
		       <?php endif ?>
          </div>
        </div>
      <?php endwhile; ?>
    <?php endif; ?>
    <?php if ( have_rows( 'vision' ) ) : ?>
	     <?php while ( have_rows( 'vision' ) ) : the_row(); ?>
	     <div class="row pt-5 mt-5">
          <div class="col-lg-6 pt-4">
            <?php if ( get_sub_field( 'imagen' ) ) : ?>
               <img class="img-fluid" src="<?php the_sub_field( 'imagen' ); ?>" alt="">
            <?php endif ?>
          </div>
          <div class="col-lg-6">
            <h2 class="h3">
              <?php the_sub_field( 'titulo' ); ?>
            </h2>
            <div class="mt-1">
              <?php the_sub_field( 'descripcion' ); ?>
            </div>
          </div>
        </div>
      <?php endwhile; ?>
    <?php endif; ?>

        <div class="pt-5"></div>
        <div class="bg-color-11 py-5 mt-5">
    <?php if ( have_rows( 'mas' ) ) : ?>
      <?php while ( have_rows( 'mas' ) ) : the_row(); ?>
          <?php if ( have_rows( 'ideas' ) ) : ?>
  			  <?php while ( have_rows( 'ideas' ) ) : the_row(); ?>
          <div class="row p-5">
            <div class="col-lg-6">
              <h2 class="h3">
                	<?php the_sub_field( 'titulo' ); ?>
              </h2>
              <div class="mt-1">
              <?php the_sub_field( 'texto' ); ?>
              </div>
            </div>
            <div class="col-lg-6 pt-4">
              <?php if ( get_sub_field( 'imagen' ) ) : ?>
			              <img class="img-fluid" src="<?php the_sub_field( 'imagen' ); ?>" alt="">
				      <?php endif ?>
             </div>
          </div>
          <?php endwhile; ?>
          <?php endif; ?>
          <?php if ( have_rows( 'creacion_de_valor' ) ) : ?>
          <?php while ( have_rows( 'creacion_de_valor' ) ) : the_row(); ?>
          <div class="row p-5">
            <div class="col-lg-6 pt-4">
              <?php if ( get_sub_field( 'imagen' ) ) : ?>
					           <img class="img-fluid" src="<?php the_sub_field( 'imagen' ); ?>" alt="">
				       <?php endif ?>
            </div>
            <div class="col-lg-6">
              <h2 class="h3">
              <?php the_sub_field( 'titulo' ); ?>
              </h2>
              <div class="mt-1">
                <?php the_sub_field( 'texto' ); ?>
              </div>
            </div>
          </div>
          <?php endwhile; ?>
          <?php endif; ?>
          <?php if ( have_rows( 'mejores_ciudades' ) ) : ?>
			    <?php while ( have_rows( 'mejores_ciudades' ) ) : the_row(); ?>
          <div class="row p-5">
            <div class="col-lg-6">
              <h2 class="h3">
                <?php the_sub_field( 'titulo' ); ?>
              </h2>
              <div class="mt-1">
                <?php the_sub_field( 'texto' ); ?>
                 </div>
            </div>
            <div class="col-lg-6 pt-4">
              <?php if ( get_sub_field( 'imagen' ) ) : ?>
					       <img class="img-fluid" src="<?php the_sub_field( 'imagen' ); ?>" alt="">
				      <?php endif ?>
            </div>
          </div>
          <?php endwhile; ?>
          <?php endif; ?>
      <?php endwhile; ?>
    <?php endif; ?>
        </div>
      </div>
    </div>
<?php endif; ?>
