
  <!-- Alianza -->
<?php if ( get_field( 'activar_alianzas' ) == 1 ) : ?>
  <div class="section bg-color-2 color-w">
    <div class="container">
       <div class="row">
          <div class="col-lg-6">
             <?php if ( have_rows( 'alianzas' ) ) :  $i = 0;?>
             <?php while ( have_rows( 'alianzas' ) ) : the_row();
                  $i = $i + 1;
                  $postitulo = get_sub_field('postitulo');
                  $descripcion = get_sub_field('descripcion');
                ?>
          <div class="alianza-desc  <?php echo $i; if($i==1){ ?> active <?php } ?>">
            <h3 class="h2 ln-h3 color-1">
              <?php the_field( 'titulo_alianzas' ); ?>
            </h3>
            <div class="mt-4">
              <strong>
                <?php echo $postitulo; ?>
              </strong>
            </div>
            <div class="mt-4 d-block">
              <?php echo substr($descripcion,0,100); ?>
              <a class="color-1 show-mas" href="#">saber más</a>
            </div>
            <div class="mt-4 d-none">
              <?php echo $descripcion; ?>
             <a class="color-1 show-menos" href="#">saber menos</a>
            </div>
          </div>
        <?php endwhile;  endif; ?>
        </div>
        <div class="col-lg-6">
          <div class="row text-center">
            <?php if ( have_rows( 'alianzas' ) ) :  $p = 0;?>
           <?php while ( have_rows( 'alianzas' ) ) : the_row();
                    $p = $p + 1;
                    $logo = get_sub_field('logo');
           ?>
           <div class="col-lg-4">
             <div class="alianza  <?php echo $p; if($p==1){  ?> active <?php } ?>">
                <img src="<?php echo $logo; ?>" alt="">
              </div>
           </div>
           <?php endwhile;  endif; ?>
         </div>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>
