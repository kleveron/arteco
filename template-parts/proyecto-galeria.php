
<?php
  $cout = 1;



if( have_rows('contenido_interior') ):
    while( have_rows('contenido_interior') ) : the_row();

      $icono = get_sub_field('icono');
      $titulo = get_sub_field('icono');
    endwhile;
else :

endif;

?>
<!-- Galeria -->
<?php if ( get_field( 'activar_galeria' ) == 1 ) : ?>
  <?php if ( have_rows( 'galeria' ) ) : ?>
  	<?php while ( have_rows( 'galeria' ) ) : the_row(); ?>
      <?php $activo = get_sub_field( 'activo' ); ?>
      <?php $bg = get_field( 'bg_gallery' ) ?>
<div class="departamentos-interna-galeria section"  style="background-image: url('<?= $bg ? $bg : get_template_directory_uri().'/src/img/home-noticia-background.png'; ?>')">
    <div class="container py-4">
        <div class="text-center">
            <div class="supratitle">
                <?php echo _e('Galería','arteco'); ?>
            </div>
            <h2 class="h3 pb-4">
                <?php echo _e('Espacios ideales de tu próximo depa','arteco'); ?>
            </h2>
        </div>
        <div class="text-center pt-4 pb-5">
            <div class="galeria-btn">

                <div class="galeria-btn__el galeria-btn__el-js <?php if ($activo=='interior') { ?> active <?php } ?>" data-group="1">
                  <?php  _e('Interior','arteco');  ?>
                </div>

                <div class="galeria-btn__el galeria-btn__el-js <?php if ($activo=='areascomunes') { ?> active <?php } ?>" data-group="2">
                  <?php  _e('Áreas comunes','arteco');?>
                </div>

                <div class="galeria-btn__el galeria-btn__el-js <?php if ($activo=='exteriores') { ?> active <?php } ?>" data-group="3">
                    <?php  _e('Exteriores','arteco');?>
                </div>

                <div class="galeria-btn__el galeria-btn__el-js <?php if ($activo=='vistaporpiso') { ?> active <?php } ?>" data-group="4">
                    <?php  _e('Vistas por piso','arteco');?>
                </div>

            </div>
        </div>
  <?php if( have_rows('contenido_interior') ): $i = 0; ?>

      <div class="row pt-3 galeria-body__el-js <?php if ($activo!='interior') { ?> d-none  <?php  } ?> group1">
            <div class="col-lg-6 departamento-cacteristicas-el-js-wrapper">
                <div class="departamento-cacteristicas-1">
                    <div class="row">
                      <?php while ( have_rows( 'contenido_interior' ) ) : the_row();
                          $icono = get_sub_field('icono');
                          $titulo = get_sub_field('titulo');
                          $i = $i + 1;
                        //  $galeria = get_sub_field('galeria');
                       ?>
                        <div class="col-6">
                            <div class="departamento-cacteristicas-el departamento-cacteristicas-el-js  <?php if ($i==1){ echo 'active'; } ?> " data-group="1<?php echo $i;?>">
                                <?= wp_get_attachment_image( attachment_url_to_postid($icono), 'full', true, array('style'=> 'max-width:27px;max-height:27px;'))  ?>
                                <div class="departamento-cacteristicas-el-title"><?php echo   $titulo; ?></div>
                            </div>
                        </div>
                      <?php  endwhile; ?>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 departamento-cacteristicas-el-body-js-wrapper">
              <?php $cout = 0;
              while ( have_rows( 'contenido_interior' ) ) : the_row();
               $titulo = get_sub_field('titulo');
                $galeria = get_sub_field('galeria');
                $size = 'full';
                $cout = $cout + 1;?>
                <div class="slider-ss__slick departamento-cacteristicas-el-body-js <?php if ($cout!=1){ ?> d-none <?php } ?> group1<?php echo $cout; ?>">
                  <?php foreach( $galeria as $image ): ?>
                    <div>
                        <div class="slider-ss__el">
                          <div class="slider-ss__el-img">
                            <?= wp_get_attachment_image( $image['ID'], 'full', true )  ?>
                          </div>
                           <!--<div class="slider-ss__el-play video-btn" data-toggle="modal" data-src="https://player.vimeo.com/video/58385453?badge=0" data-target="#modalVideo">
                                <img src="<?php /*echo get_template_directory_uri(); */?>/src/img/play.png" alt="">
                            </div>-->
                            <div class="slider-ss__el-zoom zoom-btn" data-toggle="modal" data-src="<?php echo esc_url($image['sizes']['medium_large']); ?>" data-target="#modalZoom">
                                <img src="<?php echo get_template_directory_uri(); ?>/src/img/two-arrows.png" alt="">
                            </div>
                            <div class="slider-ss__msg">
                                <div class="slider-ss__msg-title">
                                  <?php if(esc_attr($image['title']) ): ?>
                                    <?= esc_attr($image['title']); ?>
                                  <?php else: ?>
                                    <?= esc_attr($image['alt']); ?>
                                  <?php endif; ?>
                                </div>
                                <div class="slider-ss__msg-dsc">
                                  <?php echo esc_html($image['description']); ?>
                                </div>
                            </div>
                          </div>
                    </div>
                   <?php endforeach; ?>
                 </div>
               <?php  endwhile; ?>
             </div>
        </div>
  <?php  endif;?>

<!-- Areas de comunes-->
  <?php if( have_rows('contenido_areascomunes') ): $i = 0;  ?>
    <div class="row pt-3 galeria-body__el-js <?php if ($activo!='areascomunes') { ?> d-none  <?php  } ?> group2">
        <div class="col-lg-6 departamento-cacteristicas-el-js-wrapper">
            <div class="departamento-cacteristicas-1">
                <div class="row">
                  <?php while ( have_rows( 'contenido_areascomunes' ) ) : the_row();
                      $icono = get_sub_field('icono');
                      $titulo = get_sub_field('titulo');
                      $i = $i + 1;
                    ?>
                     <div class="col-6">
                       <div class="departamento-cacteristicas-el departamento-cacteristicas-el-js  <?php if ($i==1){ echo 'active'; } ?> " data-group="2<?php echo $i;?>">
                           <img src="<?php echo $icono; ?>" alt="">
                           <div class="departamento-cacteristicas-el-title"><?php echo  $titulo; ?></div>
                       </div>
                    </div>
                    <?php  endwhile; ?>
                </div>
            </div>
        </div>
        <div class="col-lg-6 departamento-cacteristicas-el-body-js-wrapper">
          <?php $cout = 0;
            while ( have_rows( 'contenido_areascomunes' ) ) : the_row();
            $titulo = get_sub_field('titulo');
            $galeria = get_sub_field('galeria');
            $size = 'full';
            $cout = $cout + 1;?>
            <div class="slider-ss__slick departamento-cacteristicas-el-body-js <?php if ($cout!=1){ ?> d-none <?php } ?> group2<?php echo $cout; ?>">
              <?php foreach( $galeria as $image ): ?>
                <div>
                    <div class="slider-ss__el">
                      <div class="slider-ss__el-img">
                      <img src="<?php echo esc_url($image['sizes']['medium_large']); ?>" alt="" />
                      </div>
                      <div class="slider-ss__el-zoom zoom-btn" data-toggle="modal" data-src="<?php echo esc_url($image['sizes']['medium_large']); ?>" data-target="#modalZoom">
                          <img src="<?php echo get_template_directory_uri(); ?>/src/img/two-arrows.png" alt="">
                      </div>
                        <div class="slider-ss__msg">
                            <div class="slider-ss__msg-title">
                                <?php echo esc_attr($image['alt']); ?>
                            </div>
                            <div class="slider-ss__msg-dsc">
                                <?php echo esc_html($image['description']); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
               <?php  endwhile; ?>
           </div>
    </div>

  <?php  endif;?>
<!-- END Areas de comunes-->
<!-- Exteriores -->
  <?php if( have_rows('contenido_exteriores') ): $i = 0;  ?>
    <div class="row pt-3 galeria-body__el-js <?php if ($activo!='exteriores') { ?> d-none <?php  } ?> group3">
        <div class="col-lg-6 departamento-cacteristicas-el-js-wrapper">
            <div class="departamento-cacteristicas-1">
              <div class="row">
                <?php while ( have_rows( 'contenido_exteriores' ) ) : the_row();
                    $icono = get_sub_field('icono');
                    $titulo = get_sub_field('titulo');
                    $i = $i + 1;
                  ?>
                   <div class="col-6">
                     <div class="departamento-cacteristicas-el departamento-cacteristicas-el-js  <?php if ($i==1){ echo 'active'; } ?> " data-group="3<?php echo $i;?>">
                         <img src="<?php echo $icono; ?>" alt="">
                         <div class="departamento-cacteristicas-el-title"><?php echo  $titulo; ?></div>
                     </div>
                  </div>
                  <?php  endwhile; ?>
              </div>
            </div>
        </div>
        <div class="col-lg-6 departamento-cacteristicas-el-body-js-wrapper">
          <?php $cout = 0;
            while ( have_rows( 'contenido_exteriores' ) ) : the_row();
            $titulo = get_sub_field('titulo');
            $galeria = get_sub_field('galeria');
            $size = 'full';
            $cout = $cout + 1;?>
            <div class="slider-ss__slick departamento-cacteristicas-el-body-js <?php if ($cout!=1){ ?> d-none <?php } ?> group3<?php echo $cout; ?>">
              <?php foreach( $galeria as $image ): ?>
                <div>
                    <div class="slider-ss__el">
                      <div class="slider-ss__el-img">
                      <img src="<?php echo esc_url($image['sizes']['medium_large']); ?>" alt="" />
                      </div>
                      <div class="slider-ss__el-zoom zoom-btn" data-toggle="modal" data-src="<?php echo esc_url($image['sizes']['medium_large']); ?>" data-target="#modalZoom">
                          <img src="<?php echo get_template_directory_uri(); ?>/src/img/two-arrows.png" alt="">
                      </div>
                        <div class="slider-ss__msg">
                            <div class="slider-ss__msg-title">
                                <?php echo esc_attr($image['alt']); ?>
                            </div>
                            <div class="slider-ss__msg-dsc">
                                <?php echo esc_html($image['description']); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
               <?php  endwhile; ?>
           </div>
    </div>
  <?php  endif;?>
<!-- END exteriores -->
  <?php if( have_rows('contenido_vistaporpiso') ):  $i = 0;?>
        <div class="row pt-3 galeria-body__el-js <?php if ($activo!='vistaporpiso') { ?> d-none    <?php  } ?> group4">
            <div class="col-lg-6 departamento-cacteristicas-el-js-wrapper">
                <div class="departamento-cacteristicas-1">
                    <div class="row">
                      <?php while ( have_rows( 'contenido_vistaporpiso' ) ) : the_row();
                          $icono = get_sub_field('icono');
                          $titulo = get_sub_field('titulo');
                          $i = $i + 1;
                        ?>
                        <div class="col-6">
                          <div class="departamento-cacteristicas-el departamento-cacteristicas-el-js  <?php if ($i==1){ echo 'active'; } ?> " data-group="4<?php echo $i;?>">
                              <img src="<?php echo $icono; ?>" alt="">
                              <div class="departamento-cacteristicas-el-title"><?php echo  $titulo; ?></div>
                          </div>
                       </div>
                          <?php  endwhile; ?>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 departamento-cacteristicas-el-body-js-wrapper">
              <?php $cout = 0;
                while ( have_rows( 'contenido_vistaporpiso' ) ) : the_row();
                $titulo = get_sub_field('titulo');
                $size = 'full';
                $cout = $cout + 1;?>
                <div class="slider-ss__slick departamento-cacteristicas-el-body-js <?php if ($cout!=1){ ?> d-none <?php } ?> group4<?php echo $cout; ?>">
                  <?php
                        if( have_rows('galeria') ):
                              while( have_rows('galeria') ) : the_row();
                                      $titulo = get_sub_field('titulo');
                                      $subtitulo = get_sub_field('subtitulo');
                                      $previa = get_sub_field('previa');
                                      $link = get_sub_field('link');
                    ?>
                    <div>
                        <div class="slider-ss__el">
                            <div class="slider-ss__el-img">
                                <img src="<?php echo $previa;?>" alt="">
                            </div>
                            <?php if (get_sub_field('link')): ?>
                             <div class="slider-ss__el-play video-btn" data-toggle="modal" data-src="<?php echo $link; ?>" data-target="#modalVideo">
                                <img src="<?php echo get_template_directory_uri(); ?>/src/img/play.png" alt="">
                            </div>
                              <?php endif; ?>
                            <div class="slider-ss__el-zoom zoom-btn" data-toggle="modal" data-src="<?php echo $previa;?>" data-target="#modalZoom">
                                <img src="<?php echo get_template_directory_uri(); ?>/src/img/two-arrows.png" alt="">
                            </div>
                            <div class="slider-ss__msg">
                                <div class="slider-ss__msg-title">
                                    <?php echo $titulo;?>
                                </div>
                                <div class="slider-ss__msg-dsc">
                                    <?php echo $subtitulo;?>
                                </div>
                            </div>
                        </div>
                    </div>
                       <?php   endwhile;   endif;  ?>
                </div>
              <?php   endwhile; ?>
            </div>
        </div>
  <?php  endif;?>

    </div>
</div>
<?php endwhile; ?>
<?php endif; ?>

<?php else : ?>
	<?php // echo 'false'; ?>
<?php endif; ?>
