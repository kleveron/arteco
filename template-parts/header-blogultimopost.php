

<?php

$noticia_destacada = get_field('noticia_destacada');

if( $noticia_destacada ):
    $fecha = get_the_date( 'j \d\e F \d\e Y', $noticia_destacada->ID );
    $featured_img_url = get_the_post_thumbnail_url($noticia_destacada->ID, 'full');
    $titulo = $noticia_destacada->post_title;
    $descripcion_corta = $noticia_destacada->post_excerpt;
    $autor = $noticia_destacada->post_author;
    $autor_nombre =  get_the_author_meta('display_name',$autor);
    $autor_descripcion =  get_the_author_meta('description',$autor);
    $autor_foto = get_avatar_url($autor);
endif; ?>



  <!-- Último artículo -->

  <div class="section-b">
    <div class="container">
      <div class="row pt-5 mt-5">
        <div class="col-lg-6">
          <img class="img-fluid" src="<?php echo $featured_img_url; ?>" alt="">

        </div>
        <div class="col-lg-6 pt-4">
          <div class="color-3">
            <small>
              <?php echo $fecha; ?>
            </small>
          </div>
          <h2 class="h3 mt-2">
             <?php echo $titulo; ?>
          </h2>
          <div class="mt-1">
             <?php echo $descripcion_corta; ?>
          </div>
          <div>
            <div class="d-flex color-3 mt-5">
              <div class="mr-2">
                <img class="autor_foto" src="<?php echo esc_url($autor_foto); ?>" alt="">
              </div>
              <div class="pt-2">
                <div class="mb-2">
                  <strong>
                   <?php echo $autor_nombre; ?>
                  </strong>
                </div>
                <div>
                 <?php echo $autor_descripcion; ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
