<?php

// WP_Query arguments
$args = array(
    'post_type'              => array( 'proyecto_arteco' ),
    //'order'                  => 'ASC',
    /* 'tax_query' => array(
        array(
            'taxonomy' => 'proyecto_categoria',
            'field'    => 'slug',
            'terms'    =>  array('entrega-inmediata','planos'),
        ),
    ), */
);
$frontpage = get_option('page_on_front');

if( get_field( 'home_proyect_ejecution', $frontpage ) ):
    $args['post__in'] = get_field( 'home_proyect_ejecution',$frontpage );
    $args['orderby'] = 'post__in';
endif;
// The Query
$proyectos = new WP_Query( $args ); ?>


<!-- Proyectos -->

  <div class="home-proyectos section">
    <div class="container">
      <div class="text-center">
          <div class="supratitle">NOVEDADES</div>
        <h2 class="h2">Proyectos en ejecución</h2>

       <div class="text-center">
           <div class="proyectos__slick pt-5">

           <?php if ( $proyectos->have_posts() ) { while ( $proyectos->have_posts() ) {

               $proyectos->the_post();
               $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full');

               $terms = get_the_terms( $post->ID, 'proyecto_categoria' );
               if(!empty($terms)){
                   $term = array_shift($terms);
                   $catproyecto = $term->name;
               }

               $inactivo = get_field( 'activar' );

            ?>


            <!-- loop -->
            <?php if ( have_rows( 'previo' ) ) : ?>
                <?php while ( have_rows( 'previo' ) ) : the_row();
                    $distrito = get_sub_field('distrito');
                 ?>
                    <div class="<?php echo $inactivo; ?>">
                        <div class="proyectos__slick-el">
                          <a href="<?php echo get_permalink(); ?>">
                            <div class="proyectos__slick-el-img-wrapper">
                                <div class="proyectos__slick-el-img-wrapper-inner">
                                    <img class="proyectos__slick-el-img" src="<?php echo $featured_img_url ?>" alt="">
                                </div>
                                <h3 class="proyectos__slick-el-title">
                                    <?php echo esc_html($distrito->name); ?>
                                </h3>
                                <div class="proyectos__slick-el-sticker"><?php echo $catproyecto; ?></div>
                            </div>
                          </a>
                            <div class="proyectos__slick-el-body">
                                <a href="<?php echo get_permalink(); ?>" class="proyectos__slick-el-subtitle"><?php the_title();?></a>
                                <div class="proyectos__slick-el-address">
                                    <?php the_sub_field( 'ubicacion' ); ?>
                                </div>
                                <div class="proyectos__slick-el-item-wrapper">
                                    <div class="row">
                                        <div class="col">
                                            <div class="proyectos__slick-el-item">
                                                <img src="<?php echo get_template_directory_uri(); ?>/src/img/ico-metraje.png" alt="">
                                                <span>Desde   <?php the_sub_field( 'area_desde' ); ?></span>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="proyectos__slick-el-item">
                                                <img src="<?php echo get_template_directory_uri(); ?>/src/img/ico-habitaciones.png" alt="">
                                                <span> <?php the_sub_field( 'dormitorios' ); ?> dorm.</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="proyectos__slick-el-price">
                                    <span>Desde:</span><span class="proyectos__slick-el-price-number"> S/ <?php the_sub_field( 'precio_desde' ); ?></span>
                                </div>
                                <div class="text-center">
                                    <a href="<?php echo get_permalink(); ?>" class="butn butn-1">Ver más</a>
                                </div>
                            </div>
                        </div>
                    </div>

                <?php endwhile; ?>
            <?php endif; ?>


           <?php  } } else { _e('No se econtraron resultados','arteco');} wp_reset_postdata(); ?>
        </div>


      </div>
    </div>
  </div>
  </div>
