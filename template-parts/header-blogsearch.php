
  <!-- Qué estás buscando -->

  <div class="section-t">
    <div class="container text-center">
      <h1 class="h1 mb-5">
      <?php _e('¿Qué estas buscando?','arteco'); ?>
      </h1>
      <?php //get_search_form(); ?>
      <div class="input-search">
        <form id="searchform" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
        <input type="text" class="search-field form-control" name="s" placeholder="Escribe tu busqueda..." value="<?php echo get_search_query(); ?>">
        <input type="hidden" name="post_type" value="post" />
        <input type="submit" value=" " style="position: absolute;width: 76px;right: 0;top: 0px;border: 0;background: url(<?php echo get_site_url(); ?>/wp-content/themes/arteco/src/img/search.png);background-repeat: no-repeat;background-position: center center;">
        </form>
      </div>
    </div>
  </div>
