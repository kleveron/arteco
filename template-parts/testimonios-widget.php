<?php


$args = array(
    'post_type'              => array( 'testimonio_arteco' ),
    'order'                  => 'ASC',
);
$query = new WP_Query( $args );


?>
<!-- Clientes -->

<div class="home-clientes section">
    <div class="container">
        <div class="text-center">
            <div class="supratitle">
                TESTIMONIOS
            </div>
            <h2 class="h2">
                Nuestros clientes hablan
            </h2>
        </div>
        <div class="text-center">
            <div class="clientes__slick pt-5">

                <!-- loop-->

                <?php if ( $query->have_posts() ) { ?>
<?php while ( $query->have_posts() ) {  $query->the_post();  ?>

         <?php if ( have_rows( 'video_o_texto' ) ) : ?>
                            <?php while ( have_rows( 'video_o_texto' ) ) : the_row(); ?>
                                <?php if ( get_sub_field( 'activar_video' ) == 1 ) : ?>
                                    <!-- para video -->
                                    <div>
                                        <div class="proyectos__slick-el">
                                            <div class="proyectos__slick-el-inner">
                                                <div class="proyectos__slick-el-img-wrapper">
                                                    <?php if ( get_sub_field( 'imagen_previa' ) ) : ?>
                                                        <img class="proyectos__slick-el-img" src="<?php the_sub_field( 'imagen_previa' ); ?>" alt="">
                                                    <?php endif ?>
                                                   <div class="proyectos__slick-el-play video-btn" data-toggle="modal" data-src="<?php the_sub_field( 'video' ); ?>" data-target="#modalVideo">
                                                        <img src="<?php echo get_template_directory_uri(); ?>/src/img/play.png" alt="">
                                                    </div>
                                                    <div class="slider-ss__el-zoom zoom-btn" data-toggle="modal" data-src="../src/img/slider-ss-1.png" data-target="#modalZoom">
                                                        <img src="<?php echo get_template_directory_uri(); ?>/src/img/two-arrows.png" alt="">
                                                    </div>
                                                </div>
                                                <div class="proyectos__slick-el-body">
                                                    <div class="proyectos__slick-el-subtitle">
                                                        <?php the_field( 'cliente' ); ?>
                                                    </div>
                                                    <div class="proyectos__slick-el-desc">
                                                        <?php the_field( 'descripcion' ); ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                <?php else : ?>
                                    <!-- para texto -->

                                    <div>
                                        <div class="proyectos__slick-el">
                                            <div class="proyectos__slick-el-inner proyectos__slick-el-inner--2">
                                                <div class="proyectos__slick-el-comentario-wrapper">
                                                    <div class="proyectos__slick-el-comentario">
                                                        <?php the_sub_field( 'testimonio' ); ?>
                                                    </div>
                                                    <img class="proyectos__slick-el-comentario-img" src="<?php echo get_template_directory_uri(); ?>/src/img/comillas.png" alt="">
                                                </div>
                                                <div class="proyectos__slick-el-body">
                                                    <?php if ( get_sub_field( 'foto' ) ) : ?>
			                                                  <img class="proyectos__slick-el-body-img" src="<?php the_sub_field( 'foto' ); ?>" alt="">
		                                                <?php endif ?>
                                                   <div class="proyectos__slick-el-subtitle">
                                                        <?php the_field( 'cliente' ); ?>
                                                    </div>
                                                    <div class="proyectos__slick-el-desc">
                                                        <?php the_field( 'descripcion' ); ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                <?php endif; ?>


                            <?php endwhile; ?>
                        <?php endif; ?>



                    <?php } } ?>




            </div>
        </div>
    </div>
</div>
