<!-- Categorias -->

<?php
$cat_terms = get_terms(
                array('category'),
                array(
                        'hide_empty'    => false,
                        'orderby'       => 'name',
                        'order'         => 'ASC',
                        'number'        => 6
                    )
            );
 ?>


<div class="slider-categorias">
  <div class="container text-center">
    <div class="slider-categorias__slick">
      <?php if( $cat_terms ) : foreach( $cat_terms as $term ) : ?>
           <div class="slider-categorias__el">
              <a href="<?= get_category_link($term->term_id); ?>">
                <span>
                  <?php echo $term->name; ?>
                </span>
              </a>
            </div>
      <?php   endforeach;   endif; ?>


    </div>
  </div>
</div>
