<?php if ( get_field( 'activar_formulario' ) == 1 ) : ?>

<!-- Formulario -->

<div id="form-contacto" class="contacto" style="background-image: url(<?php echo get_template_directory_uri(); ?>/src/img/img-contact.png)">
    <div class="container">
        <div class="contacto-el">
            <div class="row">
                <div class="col-lg-5">
                    <h4 class="h3">
                        Contáctanos y disfruta los <br>
                        <span class="color-1">mejores beneficios para ti</span>
                    </h4>
                    <div class="contacto__dsc">
                        Conoce nuestro nuevo proyecto de uso residencial
                        y familiar, ubicado en la zona con mayor potencial.
                        Departamentos de 1 a 3 dormitorios, desde 40m2.
                    </div>
                    <div class="contacto__img-wrapper">
                        <div class="row justify-content-center align-items-center">
                          <?php if( get_field('contact_gallery') ): ?>
                            <?php  foreach ( get_field('contact_gallery') as $key => $img ):?>
                              <div class="col-4">
                                <?= wp_get_attachment_image($img, 'full', false, array('class'=> 'img-fluid', 'loading' =>  'lazy' )); ?>
                              </div>
                            <?php endforeach;?>
                          <?php endif; ?>
                        </div>
                    </div>
                    <?php if( get_field('contact_financed') ): ?>
                      <div class="contacto__financiamiento">
                        <div class="contacto__financiamiento-txt mb-3">Financiado por: </div>
                        <div class="row">
                          <?php  foreach ( get_field('contact_financed') as $key => $img ):?>
                            <div class="col-lg-4">
                              <?= wp_get_attachment_image($img, 'full', false, array('class'=> 'img-fluid', 'loading' =>  'lazy' )); ?>
                            </div>
                          <?php endforeach;?>
                        </div>
                      </div>
                    <?php endif; ?>
                </div>
                <div class="col-lg-7">
                  <?php $data = get_field('planos', $post->ID);  ?>
                  <form class="contacto-el-form" method="POST" data-json="<?= esc_html( json_encode($data) ); ?>">
                    <input type="hidden" name="action" value="ar_ajax_contact">
                    <input type="hidden" name="page" value="<?= $post->ID; ?>">
                    <input type="hidden" name="proyect" value="<?= $post->ID; ?>">
                    <input type="hidden" name="url" value="<?= ar_url_server( $_SERVER ); ?>">
                    <input type="hidden" name="type" value="proyect">
                    <div class="row">
                      <div class="col-lg-6">
                        <input placeholder="Nombre *" name="firstname" type="text" class="form-control" required>
                      </div>
                      <div class="col-lg-6">
                        <input placeholder="Apellido *" name="lastname"  type="text" class="form-control" required>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-lg-6">
                        <input placeholder="DNI *" name="dni" type="number" class="form-control ar-input-number" onkeypress="return event.keyCode === 8 || event.charCode >= 48 && event.charCode <= 57" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength = "8" required>
                      </div>
                      <div class="col-lg-6">
                        <input placeholder="Teléfono" name="phone" type="number" class="form-control ar-input-number" onkeypress="return event.keyCode === 8 || event.charCode >= 48 && event.charCode <= 57" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength = "9" required>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-lg-12">
                        <input placeholder="Correo electrónico *" name="email" type="email" class="form-control" required>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-6">
                        <div class="ar-select mb-4">
                          <?php $bedrooms =  ar_get_proyect($post->ID, 'dormitorios'); ?>
                          <select class="form-control ar-select2 ar-select2-proyects ar-select2-bedrooms" name="bedrooms" data-minimum-results-for-search="Infinity" placeholder="Cant. Dorm" required>
                            <option value="" hidden> Cant. Dorm </option>
                            <?php if( $bedrooms ): ?>
                              <?php foreach ( $bedrooms as $key => $bedroom): ?>
                                <option value="<?= $bedroom ?>"><?= $bedroom == 1 ? $bedroom.' Dormitorio' : $bedroom.' Dormitorios'?> </option>
                              <?php endforeach; ?>
                            <?php endif; ?>
                          </select>
                        </div>
                      </div>

                      <div class="col-6">
                        <div class="ar-select mb-4">
                          <?php $flats =  ar_get_proyect($post->ID, 'pisos'); ?>
                          <select class="form-control ar-select2 ar-select2-proyects ar-select2-flat" name="flat" data-minimum-results-for-search="Infinity" placeholder="Pisos" required>
                            <option value="" hidden> Pisos </option>
                            <?php if( $flats ): ?>
                              <?php foreach ( $flats as $key => $flat): ?>
                                <option value="<?= $flat ?>"><?= $flat; ?> Piso </option>
                              <?php endforeach; ?>
                            <?php endif; ?>
                          </select>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-6">
                        <div class="ar-select mb-4">
                          <?php  $views =  ar_get_proyect($post->ID, 'tipo_de_vista'); ?>
                          <select class="form-control ar-select2 ar-select2-proyects ar-select2-view" name="view" data-minimum-results-for-search="Infinity" placeholder="Tipo de visita" required>
                            <option value="" hidden> Tipo de visita </option>
                            <?php if( $views ): ?>
                              <?php foreach ( $views as $key => $view): ?>
                                <option value="<?= $view ?>"><?= $view; ?></option>
                              <?php endforeach; ?>
                            <?php endif; ?>
                          </select>
                        </div>
                      </div>

                      <div class="col-6">
                        <div class="ar-select mb-4">
                          <?php $departaments =  ar_get_proyect($post->ID, 'depa'); ?>
                          <select class="form-control ar-select2 ar-select2-proyects ar-select2-departament" name="departament" data-minimum-results-for-search="Infinity" placeholder="N° Depto." required>
                            <option value="" hidden> N° Depto. </option>
                            <?php if( $departaments ): ?>
                              <?php foreach ( $departaments as $key => $departament): ?>
                                <option value="<?= $departament ?>">Dpto. <?= $departament; ?></option>
                              <?php endforeach; ?>
                            <?php endif; ?>
                          </select>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-lg-12">
                        <div class="ar-select mb-4">
                          <?php $hours =  ar_get_hour('9:00', '17:00' , 30 ); ?>
                          <select class="form-control ar-select2 ar-select2-proyects ar-select2-hour" name="hour" data-minimum-results-for-search="Infinity" placeholder="Selecciona una hora de contacto" required>
                            <option value="" hidden> N° Depto. </option>
                            <?php if( $hours ): ?>
                              <?php foreach ( $hours as $key => $hour): ?>
                                <option value="<?= $hour ?>"><?= $hour ?></option>
                              <?php endforeach; ?>
                            <?php endif; ?>
                          </select>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-lg-12 mb-4">
                        <div class="ar-date-piker  position-relative">
                          <div class="position-relative ar-picker-wrap">
                            <input type="text" class="form-control ar-picker" name="date" placeholder="Agendar una cita para ver piloto (opcional)" autocomplete="off" readonly >
                            <button type="button" class="ar-picker-trigger"></button>
                          </div>
                          <div class="ar-picker-container"></div>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-lg-12">
                        <div class="d-block text-left my-2 custom-control custom-checkbox ">
                          <?php
                            $page = '';
                            $page2 = '';
                            if( have_rows('admin_terms', 'option') ):
                              while( have_rows('admin_terms', 'option') ): the_row();
                                $page  = get_sub_field('admin_necessary_data_policies');
                                $page2 = get_sub_field('admin_additional_data_policies');
                              endwhile;
                            endif;
                            $terms = 'He leido y acepto los términos y condiciones de la "Politicas de datos personales para fines necesarios".';
                            if($page):
                              $terms = 'He leido y acepto los términos y condiciones de la <a href="'.get_the_permalink($page->ID).'" target="_blank" rel="noopener noreferrer">"Politicas de datos personales para fines necesarios".</a>';
                            endif;
                          ?>
                          <input type="checkbox" class="custom-control-input" id="cbox-terminos" name="terms" value="on" required checked>
                          <label for="cbox-terminos" class="custom-control-label form-text"><?= $terms; ?></label>
                        </div>
                      </div>
                      <div class="col-lg-12">
                        <?php

                          $terms_ext = 'He leido y acepto los términos y condiciones de la "Politicas de datos personales para fines adicionales".';
                          if($page2):
                            $terms_ext = 'He leido y acepto los términos y condiciones de la <a href="'.get_the_permalink($page2->ID).'" target="_blank" rel="noopener noreferrer">"Politicas de datos personales para fines adicionales".</a>';
                          endif;
                        ?>
                        <div class="d-block text-left my-2 custom-control custom-checkbox ">
                          <input type="checkbox" class="custom-control-input" id="cbox-autorizo" name="terms_extra" value="on" >
                          <label for="cbox-autorizo" class="custom-control-label form-text"><?= $terms_ext; ?></label>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-lg-12 text-center pt-4">
                        <button class="butn butn-1 ar-send-contact">Enviar</button>
                      </div>
                    </div>
                  </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endif; ?>
