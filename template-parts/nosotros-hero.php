<!-- Banner -->
<?php if ( have_rows( 'banner' ) ) : ?>
	<?php while ( have_rows( 'banner' ) ) : the_row(); ?>
<div class="banner banner-nosotros">
    <?php get_template_part( 'template-parts/banner', 'top' ); ?>
  <div class="banner__images">
    <div>
      <div>
        <?php if ( get_sub_field( 'fondo' ) ) : ?>
        <img class="banner__img" src="<?php the_sub_field( 'fondo' ); ?>" alt="">
    		<?php endif ?>
      </div>
    </div>
  </div>
  <div class="banner__title-wrapper">
    <div class="container">
      <div class="banner__title">
        	<?php the_sub_field( 'titulo' ); ?>
        <!--Conoce el equipo  <span class="color-1"> Arteco</span>-->
      </div>
    </div>
  </div>
</div>
<?php endwhile; ?>
<?php endif; ?>
