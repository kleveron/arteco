<?php
//test

$args = array(
    'post_type'              => array( 'slider_arteco' ),
    'order'                  => 'ASC',
);
$query = new WP_Query( $args ); ?>


    <div class="banner">

      <?php get_template_part( 'template-parts/banner', 'top' ); ?>

<?php if ( $query->have_posts() ) { ?>



    <div class="banner__images">
        <div class="banner__slick">
    <?php while ( $query->have_posts() ) {  $query->the_post();  ?>
            <div>
                <?php if ( get_field( 'fondo' ) ) : ?>
                    <img class="banner__img" src="<?php the_field( 'fondo' ); ?>" alt="">
                <?php endif ?>
           </div>
    <?php } ?>

        </div>
    </div>
    <div class="banner__content">
        <div class="container">
            <div class="banner__list d-none d-lg-block">
                <ul>
                    <?php while ( $query->have_posts() ) {  $query->the_post();  ?>
                    <li class="banner__list-opt <?php if (get_field( 'numero' )==1){ echo 'active';} ; ?>">
                        <span class="banner__list-opt-dsc"> <?php the_field( 'estado' ); ?></span>
                        <span class="banner__list-opt-number"> <?php the_field( 'numero' ); ?></span>
                    </li>
                    <?php } ?>
               </ul>
            </div>
            <div class="banner__card-wrapper">
                    <?php while ( $query->have_posts() ) {  $query->the_post();  ?>
                <div class="banner__card <?php if (get_field( 'numero' )==1){ echo 'active';} ; ?>">
                        <?php if ( have_rows( 'contenido' ) ) : ?>
                            <?php while ( have_rows( 'contenido' ) ) : the_row(); ?>
                            <div class="banner__card-1">
                                <div class="banner__card-supratitle">
                                    <?php the_sub_field( 'pretitulo' ); ?>
                                 </div>
                                <div class="banner__card-title">
                                    <?php the_sub_field( 'titulo' ); ?>

                                </div>
                                <div class="banner__card-br"></div>
                                <div class="banner__card-bottom">
                                    <div class="banner__card-bottom-metraje">
                                      <?php if ( have_rows( 'primer_campo' ) ) : ?>
			                                     <?php while ( have_rows( 'primer_campo' ) ) : the_row(); ?>
                                             <img src="<?php the_sub_field( 'icono' ); ?>" alt="">
                                             <span><?php the_sub_field( 'texto' ); ?></span>
                                      <?php endwhile; ?>
		                                  <?php endif; ?>
                                    </div>
                                    <div class="banner__card-bottom-habitaciones">
                                      <?php if ( have_rows( 'segundo_campo' ) ) : ?>
                                          <?php while ( have_rows( 'segundo_campo' ) ) : the_row(); ?>
                                             <img src="<?php the_sub_field( 'icono' ); ?>" alt="">
                                             <span><?php the_sub_field( 'texto' ); ?></span>
                                      <?php endwhile; ?>
                                     <?php endif; ?>
                                    </div>
                                    <div class="banner__card-bottom-direccion">
                                      <?php if ( have_rows( 'tercer_campo' ) ) : ?>
                                          <?php while ( have_rows( 'tercer_campo' ) ) : the_row(); ?>
                                             <img src="<?php the_sub_field( 'icono' ); ?>" alt="">
                                             <span><?php the_sub_field( 'texto' ); ?></span>
                                      <?php endwhile; ?>
                                     <?php endif; ?>
                                    </div>
                                </div>
                                <div>
                                    <a href="<?php the_sub_field( 'mas_informacion' ); ?>" class="butn butn-1">Más Información</a>
                                </div>
                            </div>
                            <div class="banner__card-2">
                                    <div class="banner__card-counter">
                                        <?php the_field( 'numero' ); ?>/<small>5</small>
                                    </div>
                                </div>
                            <?php endwhile; ?>
                      <?php endif; ?>


                </div>
                    <?php } ?>

            </div>
        </div>
    </div>
    <div class="banner__dots"></div>


<?php }  ?>


    </div>


<?php


wp_reset_postdata();
