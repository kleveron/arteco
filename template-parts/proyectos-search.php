<!-- Proyectos -->

<div class="home-proyectos home-proyectos-departamentos section">
  <div class="container">
    <div class="proyectos__filters">
      <div class="row">
        <div class="col-lg-7">
          <div class="proyectos__filters-el">
            <div class="proyectos__filters-el-btn" data-filter=".planos">
              Planos
            </div>
            <div class="proyectos__filters-el-btn" data-filter=".entrega-inmediata">
              Entrega inmediata
            </div>
            <div class="proyectos__filters-el-btn" data-filter=".entregados">
              Entregados
            </div>
          </div>
        </div>
        <div class="col-lg-5">
          <div class="proyectos__filters-el-select">
            <div class="select-box">
              <div class="select-box__current" tabindex="1">
                  <div class="select-box__value"><input class="select-box__input" type="radio" id="distrito0" value="all" name="distrito" checked="checked" />
                      <p class="select-box__input-text">Filtrar por distrito</p>
                  </div>
                  <div class="select-box__value"><input class="select-box__input" type="radio" id="distrito1" value=".lince" name="distrito" />
                      <p class="select-box__input-text">Lince</p>
                  </div>
                  <div class="select-box__value"><input class="select-box__input" type="radio" id="distrito2" value=".jesus-maria" name="distrito" />
                      <p class="select-box__input-text">Jesús María</p>
                  </div>
                  <div class="select-box__value"><input class="select-box__input" type="radio" id="distrito3" value=".chorrillos" name="distrito" />
                      <p class="select-box__input-text">Chorrillos</p>
                  </div>
                  <div class="select-box__value"><input class="select-box__input" type="radio" id="distrito4" value=".trujillo" name="distrito" />
                      <p class="select-box__input-text">Trujillo</p>
                  </div>

                  <img class="select-box__icon" src="<?php echo get_template_directory_uri(); ?>/src/img/select-arrow.png" alt="Arrow Icon" aria-hidden="true" />
              </div>
              <ul class="select-box__list">
                  <li><label class="select-box__option" for="distrito0" aria-hidden="aria-hidden">Todos los distritos</label></li>
                  <li><label class="select-box__option" for="distrito1" aria-hidden="aria-hidden">Lince</label></li>
                  <li><label class="select-box__option" for="distrito2" aria-hidden="aria-hidden">Jesús María</label></li>
                  <li><label class="select-box__option" for="distrito3" aria-hidden="aria-hidden">Chorrillos</label></li>
                  <li><label class="select-box__option" for="distrito4" aria-hidden="aria-hidden">Trujillo</label></li>

              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
