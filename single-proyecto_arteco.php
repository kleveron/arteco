<?php

get_header('dark');


while ( have_posts() ) : the_post();



?>


<!-- Breadcrumb -->
<div class="custom-breadcrumb">
    <nav aria-label="breadcrumb">
        <div class="container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php bloginfo('url'); ?>/proyectos/">Proyectos</a></li>
                <li class="breadcrumb-item active" aria-current="page"><?php the_title();?></li>
            </ol>
        </div>
    </nav>
</div>

<?php get_template_part( 'template-parts/proyecto', 'banner' ); ?>
<?php if ( get_field( 'activar_proyecto_entregado' ) == 1) : ?>
<?php get_template_part( 'template-parts/proyecto', 'conoce' ); ?>
<?php endif;?>

<?php if ( get_field( 'activar_proyecto_entregado' ) == 0 ) : ?>
<?php get_template_part( 'template-parts/proyecto', '360' ); ?>
<?php endif;?>

<?php get_template_part( 'template-parts/proyecto', 'caracteristicas' ); ?>
<?php if ( get_field( 'activar_proyecto_entregado' ) == 1) : ?>
<?php get_template_part( 'template-parts/proyecto', 'caracteristicas_2' ); ?>
<?php endif;?>

<?php if ( get_field( 'activar_proyecto_entregado' ) == 0 ) : ?>
<?php get_template_part( 'template-parts/proyecto', 'brochure' ); ?>
<?php endif;?>

<?php get_template_part( 'template-parts/proyecto', 'mapa' ); ?>

<?php if ( get_field( 'activar_proyecto_entregado' ) == 0 ) : ?>
<?php get_template_part( 'template-parts/proyecto', 'galeria' ); ?>
<?php endif;?>

<?php if ( get_field( 'activar_proyecto_entregado' ) == 0) : ?>
<?php get_template_part( 'template-parts/proyecto', 'planos' ); ?>
<?php get_template_part( 'template-parts/proyecto', 'modalplanos' ); ?>
<?php endif;?>

<?php if ( get_field( 'activar_proyecto_entregado' ) == 0) : ?>
<?php get_template_part( 'template-parts/proyecto', 'formulario' ); ?>
<?php endif; ?>

<?php if ( get_field( 'activar_proyecto_entregado' ) == 1) : ?>
  <?php get_template_part( 'template-parts/testimonios', 'widget' ); ?>
<?php get_template_part( 'template-parts/proyectos-widget', 'ejecucion-carrusel' ); ?>
<?php endif;?>


<?php endwhile; ?>
<?php get_footer(); ?>
