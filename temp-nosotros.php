<?php /* Template Name: Template Nosotros*/ ?>
<?php get_header('dark'); ?>

 <?php get_template_part( 'template-parts/nosotros', 'hero' ); ?>
 <?php get_template_part( 'template-parts/nosotros', 'timeline' ); ?>
<?php get_template_part( 'template-parts/nosotros', 'quienessomos' ); ?>
<?php get_template_part( 'template-parts/nosotros', 'valores' ); ?>
<?php get_template_part( 'template-parts/nosotros', 'alianza' ); ?>
<?php get_template_part( 'template-parts/nosotros', 'bancos' ); ?>
<?php get_template_part( 'template-parts/nosotros', 'responsabilidad' ); ?>
<?php get_template_part( 'template-parts/nosotros', 'reconocimientos' ); ?>
<?php get_template_part( 'template-parts/nosotros', 'miembros' ); ?>



<!-- Modal Video -->
<div class="modal fade modal-video" id="modalVideo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
       <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <!-- 16:9 aspect ratio -->
        <div class="embed-responsive embed-responsive-16by9">
          <iframe class="embed-responsive-item" src="" id="video"  allowscriptaccess="always" allow="autoplay"></iframe>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Modal Zoom -->
<div class="modal fade modal-zoom" id="modalZoom" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
       <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <!-- 16:9 aspect ratio -->
        <div class="text-center">
		  <img class="img-fluid" src="" id="zoom" />
        </div>
      </div>
    </div>
  </div>
</div>


<?php get_footer(); ?>
